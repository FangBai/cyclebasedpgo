## CB-PGO vs VB-PGO C++ Software Package

  Maintainer: Fang Bai (fang.bai@yahoo.com; Fang.Bai@student.uts.edu.au)
  
  The repository includes the C++ software for minimum cycle basis, cycle-based pose graph optimization (CB-PGO), and vertex-based pose graph optimization (VB-PGO).

  The software is released with the publication, **sparse pose graph optimization in cycle space** [**(Arxiv)**](https://arxiv.org/pdf/2203.15597.pdf), by bai et al., T-RO.


---
### What's inside each folder ###

 - **[MinimumCycleBasis](./MinimumCycleBasis)**: Implementation of minimum cycle basis

 - **[EC-PGO](./EC-PGO)**: Implementation of cycle-based PGO (** CB-PGO ** in the paper), where EC standards for edge-cycle incidence
 
 - **[NE-PGO](./NE-PGO)**: Implementation of vertex-based PGO (** VB-PGO ** in the paper), where NE standards for node-edge incidence
 
 - **[ChordalInitialization](./ChordalInitialization)**: Implementation of chordal initialization. Both the rotation and translation are initialized to achieve better performance.

 - **[AddNoise](./AddNoise)**: Create noisy PGO dataset with groundtruth, using additive Gaussian noise
 
 - *[slam_plus_plus](./slam_plus_plus)*: External library, [SLAM++](https://sourceforge.net/p/slam-plus-plus/wiki/Home/)
 
 - *[mcb-leda6-docker](./mcb-leda6-docker)*: External library, Dimitrios Michail's MCB implementation based on LEDA.
 
 - *[scripts](./scripts)*: All the scripts needed to reproduce the result of the paper "Sparse Pose Graph Optimization in Cycle Space", Bai et al., T-RO.


---
### Installation requirement ###

 - **MinimumCycleBasis**: Implemented by C++ STL only. No other requirements.
 
 - **EC-PGO**, **NE-PGO**, **ChordalInitialization**: Dependent on [SLAM++](https://sourceforge.net/p/slam-plus-plus/wiki/Home/).


---
### How to run the code ###

 - Download the code

        git clone git@bitbucket.org:FangBai/slam_pgo_cycle_vs_vertex.git

 - Compile the code
       
        cd cyclebasedpgo

        mkdir build && cd build && cmake .. && make install -j8

    which compiles and installs the library and executables in `cyclebasedpgo/install`

 - Run cycle-based PGO by
 
        ./bin/EC-PGO -i input_dataset_file -v output_vertex_estimate -e output_edge_estimate  
	   	
 - Run vertex-based PGO by
 
        ./bin/NE-PGO -i input_dataset_file -v output_vertex_estimate


---
### How to use the code as a library on your research project ###


**Install location**: By default, the `make install` command install the library into `cyclebasedpgo/install`. However you can easily modification the install location by setting

```
cmake -DCMAKE_INSTALL_PREFIX=your_preferred_path .. && make install
```

To use the library, you have to let CMake know where the library has been installed. This is done by setting the `CMAKE_PREFIX_PATH` variable in your CMakeLists.txt.



**To use EC_PGO** (i.e., cycle based PGO, namely CB-PGO based on minimum cycle basis) solver, in you project .cpp file, include the header

```
#include <ECPGO_NonlinearSolver.h>
```

and in you CMakeLists.txt, add the following lines:

```
list (APPEND CMAKE_PREFIX_PATH local_install_path_to_the_repo/cyclebasedpgo/install )

add_executable(mcb_pgo mcb_pgo.cpp)
set_target_properties(mcb_pgo PROPERTIES
  LINK_SEARCH_START_STATIC ON
  LINK_SEARCH_END_STATIC ON
)
find_package(EC_PGO REQUIRED)
include_directories(${EC_PGO_INCLUDE_DIRS})
target_link_libraries(mcb_pgo ${EC_PGO_LIBRARIES})
```



**To use NE_PGO** (i.e., conventional vertex-based PGO, namely VB-PGO) solver, in you project .cpp file, include the header

```
#include <NEPGO_NonlinearSolver.h>
```

and in you CMakeLists.txt, add the following lines:

```
list (APPEND CMAKE_PREFIX_PATH local_install_path_to_the_repo/cyclebasedpgo/install )

add_executable(ne_pgo ne_pgo.cpp)
set_target_properties(ne_pgo PROPERTIES
  LINK_SEARCH_START_STATIC ON
  LINK_SEARCH_END_STATIC ON
)
find_package(NE_PGO REQUIRED)
include_directories(${NE_PGO_INCLUDE_DIRS})
target_link_libraries(ne_pgo ${NE_PGO_LIBRARIES})
```

Then you're ready to go!



**Some additional useful CMakeLists.txt settings:**
```
set(CMAKE_CXX_STANDARD 17)  # if you see errors regarding vector.emplace_back()
set(CMAKE_OSX_ARCHITECTURES "x86_64")  # if you're using Apple Arm64 Architecture, i.e., Mac M1, M2, M3 etc.
SET(CMAKE_BUILD_TYPE "Release")
```

`-fopenmp` flag needs to be activated in Linux/Ubuntu. 
On the other hand, on Mac, one shoud NOT use `-fopenmp` and `-march=native`, instead excuate `brew install llvm libomp`.
The corresponding CMakeLists.txt should look like this:
```
IF(UNIX AND NOT APPLE)
	SET (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Ofast -march=native -fopenmp")
ELSE(UNIX AND NOT APPLE)
	SET (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -O2")
ENDIF(UNIX AND NOT APPLE)
```

---
### Format of the PGO input file ###

The executables takes a data file in [**g2o format**](https://github.com/RainerKuemmerle/g2o/wiki/File-Format) as pose graph input, where each line of this text file defines an relative pose meaurement data. The line starts with an identifier such as Edge3/EDGE_SE3 for 3D data, or Edge2/EDGE_SE2 for 2D data, then followed by the measurement/edge's end node ids, the real measurement data, and the covriance of reflecting the uncertainty of the measurement data.
 
 - 2D PGO format:

        EDGE2 vID1 vID2 px py o Info11 Info 12 Info13 Info 22 Info 23 Info33
        EDGE_SE2 vID1 vID2 px py o Info11 Info 12 Info13 Info 22 Info 23 Info33  
       
 - 3D PGO format:
 
        EDGE3 vID1 vID2 px py pz o1 o2 o3 Info11 Info12 Info13 Info14 Info15 Info16 Info22 Info23 Info24 Info25 Info26 Info33 Info34 Info35 Info36 Info44 Info45 Info46 Info55 Info56 Info66
        EDGE_SE3 vID1 vID2 px py pz o1 o2 o3 Info11 Info12 Info13 Info14 Info15 Info16 Info22 Info23 Info24 Info25 Info26 Info33 Info34 Info35 Info36 Info44 Info45 Info46 Info55 Info56 Info66         



---
### Reference ###

```
@ARTICLE{BaietalTRO2021,
  author={Bai, Fang and Vidal-Calleja, Teresa and Grisetti, Giorgio},
  journal={IEEE Transactions on Robotics}, 
  title={Sparse Pose Graph Optimization in Cycle Space}, 
  year={2021},
  volume={37},
  number={5},
  pages={1381-1400},
  doi={10.1109/TRO.2021.3050328}
}
```

| [arXiv](https://arxiv.org/pdf/2203.15597.pdf) |

---
### License ###

  The software is released under the [GNU General Public License](./LICENSE).
