//
// Created by fang on 13/02/19.
//

#include <boost/heap/fibonacci_heap.hpp>

#include <iostream>

// public member functions
//bool empty(void) const;
//size_type size(void) const;
//size_type max_size(void) const;
//void clear(void);

//void pop(void);
//void update(handle_type, const_reference);
//void update_lazy(handle_type, const_reference);
//void update(handle_type);
//void update_lazy(handle_type);
//void increase(handle_type, const_reference);
//void increase(handle_type);

//void erase(handle_type const &);




struct node
{
    int id;

    node(int i)
            : id(i)
    { }
};

struct compare_node
{
    bool operator()(const node& n1, const node& n2) const
    {
        return n1.id > n2.id;
    }
};

int main()
{
    boost::heap::fibonacci_heap<node, boost::heap::compare<compare_node>> heap;

    typedef boost::heap::fibonacci_heap<node, boost::heap::compare<compare_node>>::handle_type ty_handle;

    heap.push(node(40));

    ty_handle h;
    h= heap.push(node(30));

    heap.push(node(20));
    heap.push(node(20));
    heap.push(node(20));
    heap.push(node(20));
    
    
    
    ty_handle h0;
    h0= heap.push(node(10));



    heap.increase(h, node(5));
    heap.increase(h0, node(1));


    node n = heap.top();
    //heap.pop();
    std::cout << "node: " << n.id << std::endl;

    std::cout << "node id = " << (*h0).id << std::endl;


    for(const node& n : heap) {
        std::cout << n.id << "\n";
    }
}