//
// Created by fang on 18/12/18.
//

#include <iostream>

#include "Timer.h"

#include "ShortestPaths.h"

int main()
{

    Timer timer;

    Graph G;


    //add nodes
    for (int i = 0; i < 9; ++i)
    {
        G.addNode(i);
    }

    // add edges
    int n_e_cnt = 0;

    G.addEdge(n_e_cnt, 0, 1, 4);  ++n_e_cnt;
    G.addEdge(n_e_cnt, 1, 2, 8);  ++n_e_cnt;
    G.addEdge(n_e_cnt, 2, 3, 7);  ++n_e_cnt;
    G.addEdge(n_e_cnt, 3, 4, 9);  ++n_e_cnt;
    G.addEdge(n_e_cnt, 4, 5, 10);  ++n_e_cnt;
    G.addEdge(n_e_cnt, 5, 6, 2);  ++n_e_cnt;
    G.addEdge(n_e_cnt, 6, 7, 1);  ++n_e_cnt;
    G.addEdge(n_e_cnt, 7, 0, 8);  ++n_e_cnt;

    G.addEdge(n_e_cnt, 7, 8, 7);  ++n_e_cnt;
    G.addEdge(n_e_cnt, 7, 1, 11);  ++n_e_cnt;

    G.addEdge(n_e_cnt, 8, 2, 2);  ++n_e_cnt;
    G.addEdge(n_e_cnt, 8, 6, 6);  ++n_e_cnt;

    G.addEdge(n_e_cnt, 2, 5, 4);  ++n_e_cnt;
    G.addEdge(n_e_cnt, 3, 5, 14);  ++n_e_cnt;



    double time_construct_graph = timer.toc(); timer.tic();

    // G.printGraph(std::cout);

    int status = G.reduceGraph();


    double time_reduce_graph = timer.toc(); timer.tic();

    if (! status)
        G.printGraph(std::cout);


    double time_print_graph = timer.toc(); timer.tic();




    /**@brief Compact Graph */



    CompactGraph CG (&G);  ///

    double time_build_compact_graph = timer.toc(); timer.tic();

    CG.printGraph(std::cout);  ///

    double time_print_compact_graph = timer.toc(); timer.tic();

    CG.computeAllPairsShortestPaths();  ///

    double time_all_paris_shortest_paths = timer.toc(); timer.tic();

    CG.printShortestPathTree(std::cout);  ///

    double time_print_spt_tree = timer.toc();




    std::cout << "\nTime for graph construction : " << time_construct_graph << " s\n";
    std::cout << "\nTime for graph compression : " << time_reduce_graph << " s\n";
    std::cout << "\nTime for graph printing (container iterating) : " << time_print_graph << " s\n";

    std::cout << "\nTime for building compact graph : " << time_build_compact_graph << " s\n";
    std::cout << "\nTime for printing compact graph : " << time_print_compact_graph << " s\n";
    std::cout << "\nTime for computing all-pairs-shortest-paths : " << time_all_paris_shortest_paths << " s\n";
    std::cout << "\nTime for printing SPT tree : " << time_print_spt_tree << " s\n";



    return 0;
}