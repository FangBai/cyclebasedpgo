//
// Created by fang on 25/12/18.
//


#include <unistd.h>

#include <string>

#include <sstream> //

#include <fstream>

#include "MinimumCycleBasis.h"





bool FuncReadPoseGraphFile (const char * p_s_file_name, Graph & G)
{

    std::ifstream ifs;

    ifs.open(p_s_file_name, std::ios::in);

    if (!ifs.is_open())
    {
        std::cerr << "\nFailed to Open File! Aborting..\n";
        return false;
    }

    // maximum existing ID for nodes
    int max_vidx = -1;
    int edge_id_cnt = -1;

    std::string line;
    std::string token;

    if( ifs.good() )
    {
        while (std::getline(ifs, line))
        {
            std::istringstream iss(line);

            if( std::getline(iss, token, ' ') )
                if( token.compare(0, 4, "Edge") == 0 || token.compare(0, 4, "EDGE") == 0 || token.compare("ODOMETRY") == 0 )
                {
                    // get node id1
                    std::getline(iss, token, ' ');
                    int v_id1 = atoi(token.c_str());

                    // get node id2
                    std::getline(iss, token, ' ');
                    int v_id2 = atoi(token.c_str());

                    // add nodes
                    while (v_id1 > max_vidx)
                    {
                        ++max_vidx;
                        G.addNode(max_vidx);
                    }
                    while (v_id2 > max_vidx)
                    {
                        ++max_vidx;
                        G.addNode(max_vidx);
                    }

                    // add edge
                    ++edge_id_cnt;
                    G.addEdge(edge_id_cnt, v_id1, v_id2);
                }
        }
        ifs.close();
    } else{
        std::cerr << "\nRead graph data failed! Aborting..\n";
        return false;
    }

    return true;

}








int main()
{

    const char * p_s_file_name = "/home/fang/Code/MinimumCycleBasis/data/M3500.txt";


    Graph G;

    FuncReadPoseGraphFile(p_s_file_name, G);


    G.reduceGraph();

    ShortestPaths CG(&G);


    printf("\nnum_nodes = %d,\t  num_edges = %d\n", CG.n_num_nodes(), CG.n_num_edges());

    AdaptiveSpanningTree tree(CG.n_num_nodes(), CG.n_num_edges());


    for (int i = 0; i < CG.n_num_edges(); ++i)
    {
        CompactGraph::TyCptEdge & e = CG.getEdge(i);
        if (i != 966 && i != 1117 && i!= 2224)
            tree.InsertEdge(i, e.m_n_source, e.m_n_target);
    }

    tree.Print(std::cout);


    return 0;
}