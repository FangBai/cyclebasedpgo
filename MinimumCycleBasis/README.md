# Minimum cycle basis package


Author: Fang Bai (fang.bai@yahoo.com)


## Build and install

The following command build and install the package into you home directory `~/LocalInstall` as:


```
mkdir build && cd build && cmake .. && make install
```


If you wish to install the package in another place, pass the path to cmake by setting `-DCMAKE_INSTALL_PREFIX=your_preferred_path`


```
mkdir build && cd build
cmake -DCMAKE_INSTALL_PREFIX=your_preferred_path .. && make install
```


After compilation, you can run and test the code with the example graph provided by


```
./mcb ../graph.txt
./fcb ../graph.txt
```



## How to use the code as a library

After installation,  to use the minimum cycle basis algorithm see [mcb.cpp](./src/mcb.cpp), you just need to include the header file:


```
#include "MinimumCycleBasis.h"
```


and to use the fundamental cycle basis algorithm see [fcb.cpp](./src/fcb.cpp)(if someone really needs it), include the header file:


```
#include "FundamentalCycleBasis.h"
```


Note that, **after installation**, in C++ you can also use `#include <MinimumCycleBasis.h>` and `#include <FundamentalCycleBasis.h>` up to your personal preference.


It is easy to configure the build with `CMakeLists.txt`.
To use `find_package()` in you cmake, simply add the install path to `CMakeLists.txt` by:


```
list (APPEND CMAKE_PREFIX_PATH $ENV{HOME}/LocalInstall) 
```


then cmake can locate the package by `find_package(MCB REQUIRED)`.

For example, to use **MCB**, one has to include the following two lines in your CMakeLists.txt


```
find_package(MCB REQUIRED)
include_directories(${MCB_INCLUDE_DIRS})
```


In my test, cmake works without 


```
target_link_libraries(your_binary_or_library_target ${MCB_LIBRARIES})
```


However, if it does not work on your machine, please add the above line as well...




## A sample CMakeLists.txt is provided as follows for your modification:

**CMakeLists.txt**


```
cmake_minimum_required(VERSION 3.2)

project(mcb LANGUAGES CXX)
set(CMAKE_CXX_STANDARD 11)

SET(CMAKE_BUILD_TYPE "Release")

list (APPEND CMAKE_PREFIX_PATH $ENV{HOME}/LocalInstall)
message(STATUS "CMAKE_PREFIX_PATH=${CMAKE_PREFIX_PATH}")

add_executable(mcb_exe mcb.cpp)
find_package(MCB REQUIRED)
message(STATUS "MCB_INCLUDE_DIRS=${MCB_INCLUDE_DIRS}")
message(STATUS "MCB_LIBRARIES=${MCB_LIBRARIES}")
include_directories(${MCB_INCLUDE_DIRS})
# target_link_libraries(mcb_exe ${MCB_LIBRARIES})


add_executable(fcb_exe fcb.cpp)
find_package(FCB)
if (FCB_FOUND)
    message(STATUS "FCB_INCLUDE_DIRS=${FCB_INCLUDE_DIRS}")
    message(STATUS "FCB_LIBRARIES=${FCB_LIBRARIES}")
    include_directories(${FCB_INCLUDE_DIRS})
    target_link_libraries(fcb_exe ${FCB_LIBRARIES})
endif (FCB_FOUND)

```

## [Paper describing the algorithm](https://arxiv.org/pdf/2203.15597.pdf).

```
@ARTICLE{BaietalTRO2021,
  author={Bai, Fang and Vidal-Calleja, Teresa and Grisetti, Giorgio},
  journal={IEEE Transactions on Robotics}, 
  title={{Sparse Pose Graph Optimization in Cycle Space}}, 
  year={2021},
  volume={37},
  number={5},
  pages={1381-1400},
  doi={10.1109/TRO.2021.3050328}
}
```

## License

The code is relased under the [Apache License](./LICENSE).
