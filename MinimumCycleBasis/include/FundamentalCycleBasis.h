//
// This file is part of the "CB-PGO vs VB-PGO C++ Software Package"
//
// Copyright and license (c) 2018-2020 Fang Bai <fang dot bai at yahoo dot com> @UTS:CAS; 
// All rights reserved.
// Maintained by Fang Bai <fang.bai@yahoo.com; fang.bai@student.uts.edu.au>
// The software is published under GNU General Public License. See GNU_GENERAL_PUBLIC_LICENSE.txt
//
// This is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the software package. If not, see <http://www.gnu.org/licenses/>.
//
// For academic use, please cite our publication:
// "Sparse Pose Graph Optimization in Cycle Space"
// Fang Bai, Teresa A. Vidal-Calleja, Giorgio Grisetti
// IEEE Transactions on Robotics
//


#ifndef FUNDAMENTALCYCLEBASIS_H
#define FUNDAMENTALCYCLEBASIS_H


#include <iostream>

#include <vector>

#include <list>

#include <unordered_map>   /// used for node ID mapping from Graph->CompactGraph

#include <set>   /// used for Dijkstra search frontier

#include <algorithm>

#include <limits.h>   /// INT_MAX

#include <assert.h>

#include "Timer.h"

#include "Graph.h"


class FundamentalCycleBasis

{

private:

    Graph m_graph;

    std::vector< std::vector< std::pair<int, bool> > > m_v_cycle_basis;

    bool m_b_verbose_print;

    double m_f_time_overall;

public:


    FundamentalCycleBasis()
            :m_b_verbose_print(false)
    {}


    FundamentalCycleBasis(int n_num_nodes, int n_num_edges)
            :m_graph(n_num_nodes, n_num_edges), m_b_verbose_print(false)
    {}


    inline bool addEdge (int t_n_id, int t_n_source, int t_n_target, int t_n_weight = 1)
    {
        return m_graph.addEdge(t_n_id, t_n_source, t_n_target, t_n_weight);
    }


    inline bool addNode(int t_n_id)
    {
        return m_graph.addNode(t_n_id);
    }


    inline const std::vector< std::vector< std::pair<int, bool> > > & getCycleBasis () const
    {
        return m_v_cycle_basis;
    }


    void setVerbosePrint (bool b_flag_enable_print)
    {
        m_b_verbose_print = b_flag_enable_print;
    }



    int run ()
    {
        Timer T;

        int num_edges = m_graph.n_num_edges();
        int num_nodes = m_graph.n_num_nodes();

        std::vector<int> v_n_odometry(num_nodes-1, -1);
        std::vector<int> v_n_chords;
        v_n_chords.reserve(num_edges-num_nodes+1);

        for (int idx = 0; idx < num_edges; ++idx)
        {
            const Graph::Edge & e = m_graph.getEdge(idx);
            if(std::abs(e.m_n_end_node_id[1] - e.m_n_end_node_id[0]) == 1)
            {
                int odom_id = std::min(e.m_n_end_node_id[0], e.m_n_end_node_id[1]);
                /**@brief /// In case there are parallel edges in odometry, take the very first one */
                if (v_n_odometry[odom_id] < 0)
                    v_n_odometry[odom_id] = idx;
                else
                    v_n_chords.push_back(idx);
            }
            else
            {
                v_n_chords.push_back(idx);
            }
        }

//        printf("\nv_n_odometry\n");
//        for (int i = 0; i < v_n_odometry.size(); ++i)
//            std::cout << v_n_odometry[i] << " ";
//        printf("\n");
//
//        printf("\nv_n_chords\n");
//        for (int i = 0; i < v_n_chords.size(); ++i)
//            std::cout << v_n_chords[i] << " ";
//        printf("\n");


        if ((int)v_n_chords.size() != num_edges-num_nodes+1 || (int)v_n_odometry.size() != num_nodes-1)
        {
            std::cerr << "\nError in FCB: v_n_chords.size() != num_edges-num_nodes+1 || v_n_odometry.size() != num_nodes-1\n";
            exit(1);
        }

        // add to std::vector< std::vector< std::pair<int, bool> > > m_v_cycle_basis;
        m_v_cycle_basis.resize(v_n_chords.size());
        // construct fundamental cycle basis
        for (size_t i = 0; i < v_n_chords.size(); ++i)
        {
            std::vector< std::pair<int, bool > > v_cycle;
            const Graph::Edge & e_chord = m_graph.getEdge( v_n_chords[i] );

            bool chord_dir = e_chord.m_n_end_node_id[0] >= e_chord.m_n_end_node_id[1];
            v_cycle.emplace_back( std::make_pair(e_chord.m_n_id, chord_dir) );

            int n_first_id = e_chord.m_n_end_node_id[0];
            int n_last_id = e_chord.m_n_end_node_id[1];

            // make sure n_first_id < n_last_id
            if (chord_dir)
                std::swap(n_first_id, n_last_id);

//            printf("\n n_first_id = %d,  n_last_id = %d \n", n_first_id, n_last_id);

            // traverse odometry edges
            for (int ii = n_first_id; ii < n_last_id; ++ii)
            {
                const Graph::Edge & e_odom = m_graph.getEdge( v_n_odometry[ii] );
                bool odom_dir = e_odom.m_n_end_node_id[0] < e_odom.m_n_end_node_id[1];
                v_cycle.emplace_back( std::make_pair(e_odom.m_n_id, odom_dir) );
            }

            m_v_cycle_basis[i].swap(v_cycle);
        }


        m_f_time_overall = T.toc();

        printf("\nTime FCB Overall: %f\n\n", m_f_time_overall);

	    return 0;
    }


    void print (std::ostream & os = std::cout) const
    {
        os << "\n\n---- Fundamental Cycle Basis ----\n\n";
        for(auto it = this->m_v_cycle_basis.begin(), end = this->m_v_cycle_basis.end(); it != end; ++it)
        {
            const std::vector<std::pair<int, bool>> & r_cycle = *it;
            for (int i = 0, n = r_cycle.size(); i < n; ++i)
                os << r_cycle[i].first << "(" << ( r_cycle[i].second ? "+1" : "-1" ) << ") ";
            os << "\n";
        }
    }


    friend std::ostream & operator << (std::ostream & os, const FundamentalCycleBasis & fcb)
    {

        os << "\n\n---- Fundamental Cycle Basis Statistics ----\n";
        os << "\nTime FCB Overall: " << fcb.m_f_time_overall << "\n\n";

        return os;
    }


};






#endif //FUNDAMENTALCYCLEBASIS_H
