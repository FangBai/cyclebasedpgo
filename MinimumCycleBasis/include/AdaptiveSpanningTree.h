//
// This file is part of the "CB-PGO vs VB-PGO C++ Software Package"
//
// Copyright and license (c) 2018-2020 Fang Bai <fang dot bai at yahoo dot com> @UTS:CAS; 
// All rights reserved.
// Maintained by Fang Bai <fang.bai@yahoo.com; fang.bai@student.uts.edu.au>
// The software is published under GNU General Public License. See GNU_GENERAL_PUBLIC_LICENSE.txt
//
// This is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the software package. If not, see <http://www.gnu.org/licenses/>.
//
// For academic use, please cite our publication:
// "Sparse Pose Graph Optimization in Cycle Space"
// Fang Bai, Teresa A. Vidal-Calleja, Giorgio Grisetti
// IEEE Transactions on Robotics
//


#ifndef MINIMUMCYCLEBASIS_ADAPTIVESPANNINGTREE_H
#define MINIMUMCYCLEBASIS_ADAPTIVESPANNINGTREE_H


/**@brief Fast set operations can be achieved if the elements are ordered.
 * Two data-structure for a set of elements.
 * (1) Sorted List: O(N) worst insertion. (iterating sorted list to find the place to insert)
 * (2) Binary Search Tree. O(logN) worst insertion.
 * So, BST seems to be a good dataset structure for set, if many look-up/insertion/deletion incurs. */



#include <vector>
//#include <set>  // STL set implemented by BST

#include <assert.h>

/**@brief check if spanning tree becomes cyclic after including an new edge.
 * @brief a key step when incrementally building a spanning tree. */
class AdaptiveSpanningTree {
public:

//    /**@brief a set of edges presented as index IDs. */
//    typedef std::set<int> BstIntSet;


    typedef struct ComponentNode
    {
        int component_id; // check component information with component id.

        ComponentNode *next; // or Node * next // the last node is (-1)/nullptr

        ComponentNode():component_id(-1) {} // default constructor

    } TyComponentNode;


    typedef struct ComponentInfo
    {
        int component_size; /// size of the component

        TyComponentNode *head; /// head, for looking up the beginning of component

        TyComponentNode *tail; /// tail, for linking to the next component

    } TyComponentInfo;


private:

    int m_n_num_nodes;

    int m_n_num_edges;

    /**@brief avoid loop up. */
    /**@brief operation on spanning tree, union, difference */

//    BstIntSet m_tree_set;  // spanning tree

    bool * m_b_tree_edges; // spanning tree, int *

    /**@brief the candidate data-structure must support: (1) fast component look-up with a single node ID.
     * @brief the candidate data-structure must support: (2) the union of two connected components. */

//    std::list<HashIntSet> m_connected_components; // connect components, given by node IDs, using unordered_set
//    /// slow on both look-up and union!!



    /**@brief integer buffer of connected-component. */
    TyComponentNode *m_v_connected_component_buffer; // size = number of nodes.
    // Operation: (1) lookup by pointer, O(1). #THIS PART IS GOOD#
    // Operation: (2) union by modification. but this result in iterating all elements for a specific value
    // Operation: because the CC of node ID is not given explicitly.


    std::vector<TyComponentInfo> m_v_connected_component_info; /// dynamic storage allocation.

//
//    // very slow delete/insert in the middle
//    std::vector<BstIntSet> m_connected_components;

//    list is good for delete/insert, but bad for component look-up
//    std::list<BstIntSet> m_connected_components;

    int m_n_connected_component_used_id;

    int m_n_spanning_tree_size;


public:


    AdaptiveSpanningTree(int t_n_num_nodes, int t_n_num_edges)
            : m_n_num_nodes(t_n_num_nodes),
              m_n_num_edges(t_n_num_edges),
              m_n_connected_component_used_id(0),
              m_n_spanning_tree_size(0)
    {
        /**@note parenthesis will call default initializer.*/
        m_b_tree_edges = new bool[m_n_num_edges](); // default initializer of boolean variable will set all to false
        m_v_connected_component_buffer = new TyComponentNode[m_n_num_nodes]();
    }


    /**@brief main method to check if spanning tree become cyclic after adding an edge. */
    /**@note a edge is given by <source-node-id, target-node-id>. */
    /**@brief general idea, check if source and target node belongs to the same connected-component */
    /**@note To check if the graph is acyclic, it does not matter how the nodes are connected, as long as they are connected */
    inline bool IsAcyclic(int n_source_id, int n_target_id)
    {
        TyComponentNode *ptr_source = m_v_connected_component_buffer + n_source_id;
        TyComponentNode *ptr_target = m_v_connected_component_buffer + n_target_id;

        int source_component_id = ptr_source->component_id;
        int target_component_id = ptr_target->component_id;

        if (source_component_id >= 0 && target_component_id >=0 && source_component_id == target_component_id)
            return false;

        return true;

    }


    /**@brief the main function to call to insert edge into graph without incurring a cycle.
     * @param n_source_id source id of the edge, can be any end-node of an undirected edge.
     * @param n_target_id target id of the edge, can be any end-node of an undirected edge.
     * @return Graph status after adding the edge.
     * true = acyclic and the edge is inserted.
     * false = cyclic and the edge is not inserted.
     */
    inline bool InsertEdge(int n_edge_id, int n_source_id, int n_target_id)
    {
        if(n_source_id == n_target_id) /// avoid inserting self-loop into spanning tree.
            return false;

        TyComponentNode *ptr_source = m_v_connected_component_buffer + n_source_id;
        TyComponentNode *ptr_target = m_v_connected_component_buffer + n_target_id;

        int source_component_id = ptr_source->component_id;
        int target_component_id = ptr_target->component_id;

        if(source_component_id < 0 && target_component_id < 0)
        {
            this->CreateNewConnectedComponent(ptr_source, ptr_target);
            m_b_tree_edges[n_edge_id] = true;
            ++m_n_spanning_tree_size;
            return true;
        }

        if(source_component_id >= 0 && target_component_id >= 0)
        {
            if(source_component_id == target_component_id) /// both end-nodes belong to same connected-component.
            {
                return false; /// this will cause a cycle in graph.
            }
            this->UnionConnectedComponent(ptr_source, ptr_target);
            m_b_tree_edges[n_edge_id] = true;
            ++m_n_spanning_tree_size;
            return true;
        }


        if(source_component_id >= 0)
        {
            this->AppendConnectedComponent(ptr_source, ptr_target);
            m_b_tree_edges[n_edge_id] = true;
            ++m_n_spanning_tree_size;
            return true;
        }
        else // target_component_id > 0
        {
            this->AppendConnectedComponent(ptr_target, ptr_source);
            m_b_tree_edges[n_edge_id] = true;
            ++m_n_spanning_tree_size;
            return true;
        }
    }



    inline bool IsTreeEdge (int n_id)
    {
        return m_b_tree_edges[n_id];
    }


    inline int Size ()
    {
        return m_n_spanning_tree_size;
    }


    void Print (std::ostream & os)
    {
        os << "\nSpanning Tree Size: " << this->Size();
        os <<"\nEdge Status:\n";
        for (int i = 0; i < m_n_num_edges; ++i)
        {
            os << m_b_tree_edges[i] << " ";
        }
        os << "\n\nNodes in Connected Components:\n";
        for (int i = 0; i < m_n_num_nodes; ++i)
        {
            if (m_v_connected_component_buffer[i].component_id >= 0)
                os << "[" << i << "](" << m_v_connected_component_buffer[i].component_id << ")\t";
        }
        os << "\n\nNodes Not in Connected Components:\n";
        for (int i = 0; i < m_n_num_nodes; ++i)
        {
            if (m_v_connected_component_buffer[i].component_id < 0)
                os << "[" << i << "](" << m_v_connected_component_buffer[i].component_id << ")\t";
        }
        os << "\n\nConnected Component:\n";
        for (size_t i = 0; i < m_v_connected_component_info.size(); ++i)
        {
            TyComponentNode * tmp = m_v_connected_component_info[i].head;
            if (m_v_connected_component_info[i].component_size)
            {
                printf("component %zd. size(%d):\t", i, m_v_connected_component_info[i].component_size);
                while (tmp != nullptr)
                {
                    int id = tmp - m_v_connected_component_buffer;
                    if(i == (size_t) tmp->component_id)
                    {
                        printf("[%d]\t", id);
                    }
                    tmp = tmp->next;
                }
                os << "\n";
            }
        }
        os << "\n\n";
    }


    ~AdaptiveSpanningTree()
    {
        delete []m_b_tree_edges;
        delete []m_v_connected_component_buffer;
    }





protected:

    /**@brief Union two existing (Component ID >= 0) connected components */
    inline void UnionConnectedComponent(TyComponentNode *ptr_source, TyComponentNode *ptr_target)
    {
        // make sure the component IDs are positive, namely, existing components.
        assert(ptr_source->component_id >= 0 && ptr_target->component_id >= 0);

        TyComponentInfo &ref_source_info = m_v_connected_component_info[ptr_source->component_id];
        TyComponentInfo &ref_target_info = m_v_connected_component_info[ptr_target->component_id];

        if (ref_source_info.component_size < ref_target_info.component_size) // merge source to target
        {
            /**@brief update component node */
            // link two connected components
            ref_target_info.tail->next = ref_source_info.head;

            // update component label in the component containing source node.
            int component_identifier = ptr_target->component_id;
            TyComponentNode* ptr_tmp = ref_source_info.head;
            while (ptr_tmp != nullptr)  // ref_source_info.tail->next = nullptr
            {
                ptr_tmp->component_id = component_identifier;
                ptr_tmp = ptr_tmp->next;
            }

            /**@brief update component information */
            // update component size
            ref_target_info.component_size += ref_source_info.component_size;
            // update target tail
            ref_target_info.tail = ref_source_info.tail;
            // disable size
            ref_source_info.component_size = 0;
        }
        else // merge target to source
        {
            /**@brief update component node */
            // link two connected components
            ref_source_info.tail->next = ref_target_info.head;

            // update component label in the component containing source node.
            int component_identifier = ptr_source->component_id;
            TyComponentNode * ptr_tmp = ref_target_info.head;


            while (ptr_tmp != nullptr)  // ref_source_info.tail->next = nullptr
            {
                ptr_tmp->component_id = component_identifier;
                ptr_tmp = ptr_tmp->next;
            }

            /**@brief update component information */
            // update component size
            ref_source_info.component_size += ref_target_info.component_size;
            // update source tail
            ref_source_info.tail = ref_target_info.tail;
            // disable size
            ref_target_info.component_size = 0;
        }
    }


    /**@brief Create new (Component ID < 0) connected components */
    inline void CreateNewConnectedComponent(TyComponentNode *ptr_source, TyComponentNode *ptr_target)
    {
        // make sure the component IDs are negative, namely, new components.
        assert(ptr_source->component_id < 0 && ptr_target->component_id < 0);

        /**@brief set component buffer */
        ptr_source->component_id = m_n_connected_component_used_id;
        ptr_target->component_id = m_n_connected_component_used_id;

        ptr_source->next = ptr_target;
        ptr_target->next = nullptr;

        /**@brief create new element in component info */
        m_v_connected_component_info.emplace_back( TyComponentInfo{ 2, ptr_source, ptr_target } );

        ++m_n_connected_component_used_id;
    }


    /**@brief source is one of the nodes in the connected component.
     * target is a new node to append to the connected component. */
    inline void AppendConnectedComponent(TyComponentNode *ptr_source, TyComponentNode *ptr_target)
    {
        // make sure the component id of source-node is positive, and new node does not belong to any connected-component.
        assert(ptr_source->component_id >= 0 && ptr_target->component_id < 0);

        TyComponentInfo & ref_source_info = m_v_connected_component_info[ ptr_source->component_id ];

        ref_source_info.tail->next = ptr_target;
        ref_source_info.tail = ptr_target;

        ++ref_source_info.component_size;

        ptr_target->component_id = ptr_source->component_id;
        ptr_target->next = nullptr;
    }


};






#endif //MINIMUMCYCLEBASIS_ADAPTIVESPANNINGTREE_H
