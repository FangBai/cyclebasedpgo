//
// This file is part of the "CB-PGO vs VB-PGO C++ Software Package"
//
// Copyright and license (c) 2018-2020 Fang Bai <fang dot bai at yahoo dot com> @UTS:CAS; 
// All rights reserved.
// Maintained by Fang Bai <fang.bai@yahoo.com; fang.bai@student.uts.edu.au>
// The software is published under GNU General Public License. See GNU_GENERAL_PUBLIC_LICENSE.txt
//
// This is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the software package. If not, see <http://www.gnu.org/licenses/>.
//
// For academic use, please cite our publication:
// "Sparse Pose Graph Optimization in Cycle Space"
// Fang Bai, Teresa A. Vidal-Calleja, Giorgio Grisetti
// IEEE Transactions on Robotics
//


#ifndef MINIMUMCYCLEBASIS_GRAPH_H
#define MINIMUMCYCLEBASIS_GRAPH_H

#include <iostream>
#include <vector>

/**@brief Node and Edge ID
 * PLAN 1: Each node/edge is associated with a ID = its storage index, which is unique
 * PLAN 2: Use an explicit ID, and std::unordered_map<int, ptr> to manage ID and storage
 * PLAN 2 is better when graph nodes/edges need to be deleted explicitly where map can be more flexible,
 * because it is independent of storage
 * HOWEVER, according to experiment, pure vector based implementation with ID = storage index can be twice faster!!
 */


#define RESERVED_ADJACENT_LIST_CAPACITY 5 // average node degree


class Graph {
public:

    class Node;

    class Edge;


    /**@brief Node Class */
    class Node {
    public:

        int const m_n_id;   /// node id, constant

        // initialized to be inactive (false), activated by graph compression algorithm if it is a non-degree 2 node
        bool m_b_active;   /// flag for active node, true = active, false = inactive

        std::vector<int> m_v_p_adjacent_lists;   /// a list of edge ids

    };


    /**@brief Edge Class */
    class Edge {
    public:

        int const m_n_id;   /// edge id, constant

        int const m_n_end_node_id[2];   /// [0]source and [1]target nodes, for this edge

        int const m_n_weight;   /// edge weight, positive integer

        // initialized to be active (true), deactivated by graph compression algorithm if it links to a degree 2 node
        bool m_b_active;   /// flag for active edge, true = active, false = inactive

        int m_n_ear_edge_id[2];   /// [0]ear_edge adjacent to source, [1]ear edge adjacent to target

    };


private:

    std::vector<Node> m_nodes_container;

    std::vector<Edge> m_edges_container;

    int m_n_num_edges;

    int m_n_num_nodes;

    int m_n_num_edges_original;

    int m_n_num_active_edges;

    int m_n_num_active_nodes;

    int m_n_dfs_root_node_id;


public:

    Graph() : m_n_num_edges(0), m_n_num_nodes(0),
              m_n_num_active_edges(0), m_n_num_active_nodes(0),
              m_n_dfs_root_node_id(-1)
    {}

    // constructor with reserved storage capacity for edges and nodes
    Graph(int n_num_nodes, int n_num_edges)
            : m_n_num_edges(0), m_n_num_nodes(0),
              m_n_num_active_edges(0), m_n_num_active_nodes(0),
              m_n_dfs_root_node_id(-1)
    {
        m_nodes_container.reserve(n_num_nodes);
        m_edges_container.reserve(n_num_edges);
    }


    inline int n_dfs_root_node_id() const
    {
        return m_n_dfs_root_node_id;
    }

    inline int n_num_nodes() const {
        return m_n_num_nodes;
    }

    inline int n_num_edges() const {
        return m_n_num_edges;
    }

    inline int n_num_active_nodes() const
    {
        return m_n_num_active_nodes;
    }

    inline int n_num_active_edges() const
    {
        return m_n_num_active_edges;
    }

    inline int n_num_edges_original() const {
        return m_n_num_edges_original;
    }

    Node & getNode (int t_n_id)
    {
        return m_nodes_container[t_n_id];
    }

    const Node & getNode (int t_n_id) const
    {
        return m_nodes_container[t_n_id];
    }

    Edge & getEdge (int t_n_id)
    {
        return m_edges_container[t_n_id];
    }

    const Edge & getEdge (int t_n_id) const
    {
        return m_edges_container[t_n_id];
    }

    bool addEdge(int t_n_id, int t_n_source, int t_n_target, int t_n_weight = 1)
    {
        if ( t_n_id == m_n_num_edges )
        {
            if ( t_n_source < m_n_num_nodes && t_n_target < m_n_num_nodes )
            {
                m_edges_container.emplace_back(Graph::Edge{t_n_id, {t_n_source, t_n_target}, t_n_weight, true, {-1, -1}});

                m_nodes_container[t_n_source].m_v_p_adjacent_lists.emplace_back(t_n_id);
                m_nodes_container[t_n_target].m_v_p_adjacent_lists.emplace_back(t_n_id);

                ++m_n_num_edges;
                return true;
            }
            else
            {
                return false;   /// source node or target node does not exist
            }
        }
        else
        {
            return false;  /// edge id must be assigned consecutively
        }
    }


    bool addNode(int t_n_id)
    {
        if( t_n_id == m_n_num_nodes)
        {
            m_nodes_container.emplace_back(Graph::Node{t_n_id, false});
            ++m_n_num_nodes;
            return true;
        }
        else
        {
            return false;  ///  edge id must be assigned consecutively
        }
    }


    inline int otherNode (int t_edge_id, int t_end_node_id)
    {
        Edge & e = m_edges_container[t_edge_id];
        return (e.m_n_end_node_id[0] == t_end_node_id) ?
                    e.m_n_end_node_id[1] : e.m_n_end_node_id[0];
    }

    inline int otherNode (const Edge & t_ref_edge, int t_end_node_id)
    {
        return (t_ref_edge.m_n_end_node_id[0] == t_end_node_id) ?
                    t_ref_edge.m_n_end_node_id[1] : t_ref_edge.m_n_end_node_id[0];
    }

    inline int otherNode (const Edge * t_p_edge, int t_end_node_id)
    {
        return (t_p_edge->m_n_end_node_id[0] == t_end_node_id) ?
                    t_p_edge->m_n_end_node_id[1] : t_p_edge->m_n_end_node_id[0];
    }


    /**@brief delete all degree 2 node in graph, replace with new edges
     * DFS search to find all degree 2 nodes, and compress the graph meanwhile
     * @return 0 Normal DFS search to reduce graph
     * @return 1 Exception flag, the graph is a circuit, independent itself, and no need to run MCB algorithm
     */
    int locateRootNodeDFS ()
    {
        assert((size_t) m_n_num_nodes == m_nodes_container.size());
        assert((size_t) m_n_num_edges == m_edges_container.size());

        m_n_num_edges_original = m_n_num_edges;

        m_n_num_active_nodes = 0;  /// initialize all nodes to be inactive
        m_n_num_active_edges = m_n_num_edges;  /// initialize all edges to be active

        if ( m_nodes_container.size() == 0)
            return 2;


        Node * p_node = &m_nodes_container[0];


        if(p_node->m_v_p_adjacent_lists.size() == 2)
        {
            int edge_id = p_node->m_v_p_adjacent_lists[0];  /// chose arbitrary direction
            int node_id = this->otherNode(edge_id, 0);

            p_node = &m_nodes_container[node_id];


            while (p_node->m_v_p_adjacent_lists.size() == 2)
            {
                edge_id = (p_node->m_v_p_adjacent_lists[0] != edge_id) ?
                           p_node->m_v_p_adjacent_lists[0] : p_node->m_v_p_adjacent_lists[1];

                node_id = this->otherNode(edge_id, node_id);

                p_node = &m_nodes_container[node_id];

                if(node_id == 0) /// the graph is essentially a big circuit (all nodes with degree 2)
                {
                    // activate first node.
                    m_nodes_container[node_id].m_b_active = true;
                    ++m_n_num_active_nodes;

                    // deactivate all edges.
                    for (int i = 0; i < m_n_num_edges; ++i)
                    {
                        m_edges_container[i].m_b_active = false;
                    }
                    m_n_num_active_edges = 0;

                    Node & r_node = m_nodes_container[node_id];

                    // add an ear edge
                    m_edges_container.emplace_back(
                            Graph::Edge{m_n_num_edges, {node_id, node_id}, m_n_num_edges, true,
                                        {r_node.m_v_p_adjacent_lists[0], r_node.m_v_p_adjacent_lists[1]} } );

                    r_node.m_v_p_adjacent_lists.emplace_back(m_n_num_edges);
                    r_node.m_v_p_adjacent_lists.emplace_back(m_n_num_edges);

                    ++m_n_num_active_edges;
                    ++m_n_num_edges;

                    m_n_dfs_root_node_id = node_id;

                    return 1; /// a flag indicating the graph needs no further compression.
                }
            }

            m_n_dfs_root_node_id = node_id;

            return 0;
        }
        else
        {
            m_n_dfs_root_node_id = p_node->m_n_id;

            return 0;
        }
    }


    int reduceGraph ()
    {
//        this->printGraph(std::cout);

        int root_node_id_status = this->locateRootNodeDFS();

//        printf("\n===> root_node_id_status = %d\n", root_node_id_status);
//        printf("\n===> m_n_dfs_root_node_id = %d\n", m_n_dfs_root_node_id);

        if ( root_node_id_status == 0 )
            this->DFS( &m_nodes_container[m_n_dfs_root_node_id] );   /// found a non-degree 2 node to start DFS
        else
            return root_node_id_status;

        return 0;
    }



    /**@brief A debug function, activates all nodes which are initially deactivated. */
    void activateAllNodes ()
    {
        assert((size_t) m_n_num_nodes == m_nodes_container.size());
        assert((size_t) m_n_num_edges == m_edges_container.size());

        this->locateRootNodeDFS();

        m_n_num_edges_original = m_n_num_edges;

        for (size_t i = 0, n = m_nodes_container.size(); i < n; ++i)
            m_nodes_container[i].m_b_active = true;

        m_n_num_active_nodes = m_nodes_container.size();
        m_n_num_active_edges = m_edges_container.size();
    }



    /**@brief Depth-First-Search (DFS) to compress graph
     * @note This function should ONLY be called from a non-degree 2 node */
    void DFS ( Node * node_ptr)
    {

        node_ptr->m_b_active = true;

        ++m_n_num_active_nodes;   /// increase active node number

            for (size_t ii = 0, nn = node_ptr->m_v_p_adjacent_lists.size(); ii < nn; ++ii)
            {
                int e_id = node_ptr->m_v_p_adjacent_lists[ii];

                Edge & ref_edge = m_edges_container[e_id];
                /// only traverse the active edges, and not new added "chain edges"
                if (ref_edge.m_b_active && ref_edge.m_n_id < m_n_num_edges_original)
                {
                    int node_id_next = this->otherNode(ref_edge, node_ptr->m_n_id);
                    Node * node_ptr_next = &m_nodes_container[node_id_next];

                    int n_cnt = 0;

                    /**@brief a degree-2 node connected by unsearched edge (active)*/
                    if (node_ptr_next->m_v_p_adjacent_lists.size() == 2)
                    {
                        int start_node_id = node_ptr->m_n_id;
                        int start_edge_id = ref_edge.m_n_id;

                        ref_edge.m_b_active = false;
                        --m_n_num_active_edges;   /// decrease active edge number
                        int accumulate_weight = ref_edge.m_n_weight;

                        ++n_cnt;

                        int next_edge_id = (node_ptr_next->m_v_p_adjacent_lists[0] != start_edge_id) ?
                                       node_ptr_next->m_v_p_adjacent_lists[0] : node_ptr_next->m_v_p_adjacent_lists[1];
                        Edge * next_edge_ptr = & m_edges_container[next_edge_id];

                        next_edge_ptr->m_b_active = false;
                        --m_n_num_active_edges;   /// decrease active edge number
                        accumulate_weight += next_edge_ptr->m_n_weight;

                        ++n_cnt;

                        int node_id_next = this->otherNode(next_edge_ptr, node_ptr_next->m_n_id);
                        node_ptr_next = & m_nodes_container[node_id_next];

                        while (node_ptr_next->m_v_p_adjacent_lists.size() == 2)
                        {
                            next_edge_id = (node_ptr_next->m_v_p_adjacent_lists[0] != next_edge_id) ?
                                           node_ptr_next->m_v_p_adjacent_lists[0] : node_ptr_next->m_v_p_adjacent_lists[1];
                            next_edge_ptr = & m_edges_container[next_edge_id];

                            next_edge_ptr->m_b_active = false;
                            --m_n_num_active_edges;   /// decrease active edge number
                            accumulate_weight += next_edge_ptr->m_n_weight;

                            ++n_cnt;

                            node_id_next = this->otherNode(next_edge_ptr, node_ptr_next->m_n_id);
                            node_ptr_next = & m_nodes_container[node_id_next];
                        }

                        // create a new link with weight accumulated
                        m_edges_container.emplace_back(
                                Graph::Edge{m_n_num_edges, {start_node_id, node_id_next}, accumulate_weight, true,
                                            {start_edge_id, next_edge_id} });

                        ++m_n_num_active_edges;   /// increase active edge number

                        node_ptr->m_v_p_adjacent_lists.emplace_back(m_n_num_edges);
                        node_ptr_next->m_v_p_adjacent_lists.emplace_back(m_n_num_edges);

                        ++m_n_num_edges;

//                        printf("prune %d %d weight %d length %d\n", start_node_id, node_id_next, accumulate_weight, n_cnt);
//                        printf("counted active edges: %d,   recorded active edgs: %d\n", this->checkActiveEdges(), m_n_num_active_edges);
//                        getchar();

                    }

                    if(!node_ptr_next->m_b_active)
                        this->DFS (node_ptr_next);
                }
            }

    }



    inline int checkActiveEdges ()
    {
        int n_edge_cnt = 0;
        for (size_t i = 0; i < m_edges_container.size(); ++i)
        {
            if (m_edges_container[i].m_b_active)
                ++n_edge_cnt;
        }
        return n_edge_cnt;
    }

    /**@brief recover the Degree 2 Edge/node chain, which was deactivated by calling reduceGraph ()
     * @param t_source : source node of the chain
     * @param t_target : target node of the chain
     * @param t_direction : traverse direction, true = source -> target, false = target -> source */
    void restoreDegree2Chain (const Edge & ref_edge, bool t_direction, std::vector<int> & edge_chain)
    {
        int working_edge_id = t_direction ? ref_edge.m_n_ear_edge_id[0] : ref_edge.m_n_ear_edge_id[1];
        int working_node_id = t_direction ? ref_edge.m_n_end_node_id[0] : ref_edge.m_n_end_node_id[1];

        Edge * working_edge = &m_edges_container[working_edge_id];
        Node * working_node = &m_nodes_container[working_node_id];

        int destination_node_id = this->otherNode(ref_edge, working_node_id);

        // switch working node to the first node in the chain.
        working_node_id = (working_edge->m_n_end_node_id[0] == working_node_id) ?
                            working_edge->m_n_end_node_id[1] : working_edge->m_n_end_node_id[0];

        edge_chain.emplace_back(working_edge_id);

        while (working_node_id != destination_node_id)
        {
            working_node = &m_nodes_container[working_node_id];

            working_edge_id = (working_node->m_v_p_adjacent_lists[0] == working_edge_id) ?
                               working_node->m_v_p_adjacent_lists[1] : working_node->m_v_p_adjacent_lists[0];

            working_node_id = this->otherNode(working_edge_id, working_node_id);

            edge_chain.emplace_back(working_edge_id);
        }
    }


    void printGraph(std::ostream & os)
    {
        os << "\nGraph Info.\n"
           << "\tNum. of Nodes: " << m_n_num_active_nodes
           << " active in total " << m_nodes_container.size() << "\n"
           << "\tNum. of Edges: " << m_n_num_active_edges
           << " active in total " << m_edges_container.size() << "\n";

        for (size_t n_id = 0; n_id < m_nodes_container.size(); ++n_id)
        {
            Node & n = m_nodes_container[n_id];
            if((size_t) n.m_n_id != n_id)
                os << "Node storage error!\n";
            os << "\nNode [" << n.m_n_id << "] " << (n.m_b_active ? "acitve" : "inactive") << "\n";
            os << "\tAdjacent Edges: \n";
            for (auto e_id : n.m_v_p_adjacent_lists)
            {
                Edge & e = m_edges_container[e_id];
                if(e.m_n_id != e_id)
                    os << "Edge storage error!\n";
                if (e.m_b_active)
                {
                    os << "\t\t[" << e.m_n_id << "]"
                       << " <" << e.m_n_end_node_id[0] << ", " << e.m_n_end_node_id[1] << ">"
                       << " : " << e.m_n_weight << "\n";
                }
            }
        }
        os << "\nEND#\n\n";
    }


    ~Graph() {}
};

#endif //MINIMUMCYCLEBASIS_GRAPH_H
