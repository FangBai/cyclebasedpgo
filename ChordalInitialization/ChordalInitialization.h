//
// This file is part of the "CB-PGO vs VB-PGO C++ Software Package"
//
// Copyright and license (c) 2018-2020 Fang Bai <fang dot bai at yahoo dot com> @UTS:CAS; 
// All rights reserved.
// Maintained by Fang Bai <fang.bai@yahoo.com; fang.bai@student.uts.edu.au>
// The software is published under GNU General Public License. See GNU_GENERAL_PUBLIC_LICENSE.txt
//
// This is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the software package. If not, see <http://www.gnu.org/licenses/>.
//
// For academic use, please cite our publication:
// "Sparse Pose Graph Optimization in Cycle Space"
// Fang Bai, Teresa A. Vidal-Calleja, Giorgio Grisetti
// IEEE Transactions on Robotics
//


#ifndef PGO_PROJECT_CHORDALINITIALIZATION_H
#define PGO_PROJECT_CHORDALINITIALIZATION_H

#include "eigen/Eigen/SVD" // JacobiSVD /// SVD is used to obtain rotation matrix

#include "LinearLeastSquares.h"   /// Linear least squares solver



template <const int n_dimension = 3>
class ChordalInitialization {

public:

    typedef Eigen::Matrix <double, n_dimension, n_dimension> _TyMatrix;
    typedef Eigen::Matrix <double, n_dimension, 1> _TyVector;

private:

    enum { m_n_dimension = n_dimension};

    LinearLeastSquares <n_dimension> m_ls_solver;

public:

    void addEdge ( int id1, int id2, const _TyMatrix & rlvR)
    {
        // repeat each R to n_dimension multiples
        // index id=k: in chrage of block rows:
        // n*k, n*k+1, ... n*(k+1)-1=n*k+n-1
        for (int i = 0; i < m_n_dimension; ++i)
        {
            // (n*id1+i, n*id2+i): A1 = R_{i,j}^T, A2 = -I, b = 0:
            m_ls_solver.addEdge(m_n_dimension*id1+i, m_n_dimension*id2+i,
                 rlvR.transpose(), -_TyMatrix::Identity(), _TyVector::Zero());
        }
    };


    bool solve ()
    {
        // Fix vertex 0
        // each vertex produces m_n_dimension multiples
        // we have to fix all derived vertices of vertex 0
        // namely vertex [0, 1, ... n_dimension-1]
        _TyMatrix m_init = _TyMatrix::Identity();
        for (int i = 0; i < m_n_dimension; ++i)
        {
            m_ls_solver.fixVertex(i, m_init.col(i));
        }
        // solve the linear least squares optimization problem.
        bool result = m_ls_solver.solve();
        if ( !result )
        {
            printf("\nfatal error: least squares optimization failed\n");
        }
        return result;
    }


    _TyMatrix getVertex (int id)
    {
        _TyMatrix m;
        // index id=k: in chrage of block rows:
        // n*k, n*k+1, ... n*(k+1)-1=n*k+n-1
        // construct affine matrix
        for (int i = 0; i < m_n_dimension; ++i)
        {
            m.col(i) = m_ls_solver.getVertex( m_n_dimension * id + i );
        }
        // construct rotation matrix
        return normalizeRotation(m).transpose();  /// what we compute is R^T, transpose back
    }


protected:

    /**@brief function that normalizes an affine matrix to a rotation matrix by Frobenius norm.
     *    min || M - R ||_F^2,  M is affine,  R is SO(n)
     *    Let: M = U * D * V^T be the singular value decomposition (SVD) of M
     *    The solution is:  R = U * diag (1, 1, det(U*V^T)) * V^T
     */
    _TyMatrix normalizeRotation (_TyMatrix & m)
    {
        Eigen::JacobiSVD<_TyMatrix> svd (m, Eigen::ComputeFullU | Eigen::ComputeFullV);

        double detU = svd.matrixU().determinant();
        double detV = svd.matrixV().determinant();

        if (detU * detV > 0)  // SO(n), det(R) = 1
        {
            // R = U * V^T
            return svd.matrixU() * svd.matrixV().transpose();
        }
        else
        {
            // R = U * diag(1, 1, -1) * V^T
            _TyMatrix U = svd.matrixU();
            U.col(U.cols() - 1) *= -1;
            return U * svd.matrixV().transpose();
        }
    }


};


#endif //PGO_PROJECT_CHORDALINITIALIZATION_H
