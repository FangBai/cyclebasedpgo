//
// This file is part of the "CB-PGO vs VB-PGO C++ Software Package"
//
// Copyright and license (c) 2018-2020 Fang Bai <fang dot bai at yahoo dot com> @UTS:CAS; 
// All rights reserved.
// Maintained by Fang Bai <fang.bai@yahoo.com; fang.bai@student.uts.edu.au>
// The software is published under GNU General Public License. See GNU_GENERAL_PUBLIC_LICENSE.txt
//
// This is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the software package. If not, see <http://www.gnu.org/licenses/>.
//
// For academic use, please cite our publication:
// "Sparse Pose Graph Optimization in Cycle Space"
// Fang Bai, Teresa A. Vidal-Calleja, Giorgio Grisetti
// IEEE Transactions on Robotics
//


#pragma once
#ifndef SLAM_PLUS_PLUS_LIEGROUP_H
#define SLAM_PLUS_PLUS_LIEGROUP_H

#include <iostream>

// This file requires the usage of Eigen
// Here I use the one shipped with slam_plus_plus
// To use the file out of the package, please assign the path to eigen on your system
// #include "your-native-path-to-eigen/Eigen/Core"

#include "eigen32/eigen/Eigen/Core"


// forward class declarations
class SO2;
class SO3;
class SE2;
class SE3;


// Specialization for 2D cases
template <const bool b_2D_Flag = true>
class LieGroup
{
private:
    LieGroup();

public:
    typedef SO2 _TySO;
    typedef SE2 _TySE;
};


// Specialization for 3D cases
template <>
class LieGroup<false>
{
private:
    LieGroup();

public:
    typedef SO3 _TySO;
    typedef SE3 _TySE;
};







// inject the 6D types into Eigen namespace
namespace Eigen {

#ifndef HAVE_EIGEN_6D_DOUBLE_VECTOR
    #define HAVE_EIGEN_6D_DOUBLE_VECTOR
typedef Eigen::Matrix<double, 6, 1> Vector6d; /**< @brief a 6D vector */
#endif // !HAVE_EIGEN_6D_DOUBLE_VECTOR
#ifndef HAVE_EIGEN_6D_DOUBLE_MATRIX
    #define HAVE_EIGEN_6D_DOUBLE_MATRIX
typedef Eigen::Matrix<double, 6, 6> Matrix6d; /**< @brief a 6x6 matrix */
#endif // !HAVE_EIGEN_6D_DOUBLE_MATRIX

} // ~Eigen





// function to check the sign of a value
template <typename T> int sgn(T val) {
    return (T(0) < val) - (val < T(0));
}

//*** implementaton of Lie Group operations ***//


/*
 * Implementation for SO(3) and SE(3)
 * Tested on January 19, 2018
 */


class SO3 {

private:

    SO3(){}

public:

    typedef Eigen::Matrix3d Matrix3d;
    typedef Eigen::Matrix4d Matrix4d;
    typedef Eigen::Vector3d Vector3d;
    typedef Eigen::Matrix6d Matrix6d;
    typedef Eigen::Vector6d Vector6d;


public:


    inline static Eigen::Matrix3d SkewSym(const Eigen::Vector3d & v)
    {
        Eigen::Matrix3d m;
        m <<   0,  -v(2), v(1),
              v(2),  0,  -v(0),
             -v(1), v(0),  0;
        return m;
    }


    inline static Eigen::Matrix3d Hat(const Eigen::Vector3d & v)
    {
        Eigen::Matrix3d m;
        m <<   0,  -v(2), v(1),
              v(2),  0,  -v(0),
             -v(1), v(0),  0;
        return m;
    }

    // Generators for Lie algebra of SO(3)
    inline static Eigen::Matrix3d E (int idx)
    {
        Eigen::Vector3d v;
        switch (idx)
        {
            case 0:
                v << 1, 0, 0;
                return SO3::Hat(v);
            case 1:
                v << 0, 1, 0;
                return SO3::Hat(v);
            case 2:
                v << 0, 0, 1;
                return SO3::Hat(v);
            default:
                printf("index out of bound! @ SO3::E(idx)\n");
                exit(1);
        }
    }


    inline static Eigen::Matrix3d Exp(const Eigen::Vector3d & Rvec)
    {
        double angle = Rvec.norm();
        double A = 1;
        double B = .5;
        if(angle > 1e-6){
            A = std::sin(angle)/angle;
            B = (1 - std::cos(angle))/(angle*angle);
        }
        else
        {
            /**@brief use Taylor expansion to increase accuracy */
            double angle2 = angle * angle;
            double angle4 = angle2 * angle2;
            A = 1 - angle2/6 + angle4/120;
            B = 0.5 - angle2/24 + angle4/720;
        }
        Eigen::Matrix3d vHat = SO3::SkewSym(Rvec);
        return ( Eigen::Matrix3d::Identity() + A * vHat + B * vHat * vHat );
    }


    inline static Eigen::Vector3d Log(const Eigen::Matrix3d & Rmatr)
    {
        double tmp = (Rmatr.trace()-1)/2;
        tmp = fmin(fmax(tmp,-1.0),1.0);
        double angle = std::acos(tmp);
        // do this for numerical round-off errors
        if(std::isnan(angle)){
            printf("numerical round-off errors happened at std::acos(), angle = NAN   @ SO3::Log()\n");
        }
        Eigen::Vector3d axis(Rmatr(2,1) - Rmatr(1,2),
                             Rmatr(0,2) - Rmatr(2,0),
                             Rmatr(1,0) - Rmatr(0,1));
        if (M_PI - angle < 1e-6){  // if the angle is close to pi
//            printf("angle is close to PI: angle = %f   @ SO3::Log() ", angle);
            axis(0) = - sgn(Rmatr(1,2)) * std::sqrt((1+Rmatr(0,0))/2);
            axis(1) = - sgn(Rmatr(0,2)) * std::sqrt((1+Rmatr(1,1))/2);
            axis(2) = - sgn(Rmatr(0,1)) * std::sqrt((1+Rmatr(2,2))/2);
            return (angle * axis);
        } else if (angle > 1e-6){
            double sina2 = 2 * std::sin(angle);
            return (axis * (angle/sina2));
        }
        else
        {
            /**@brief use Taylor expansion to increase accuracy */
            double angle2 = angle * angle;
            double sina2_div_angle =  2  - angle2/3 + angle2*angle2/60;
            return axis/sina2_div_angle;
        }
    }


    inline static Eigen::Matrix3d Jl(const Eigen::Vector3d & Rvec)
    {
        double angle = Rvec.norm();
        double B = 0.5;
        double C = 1/6;
        if(angle > 1e-6){
            B = (1 - std::cos(angle))/(angle*angle);
            C = (angle - std::sin(angle))/(angle*angle*angle);
        }
        else
        {
            /**@brief use Taylor expansion to increase accuracy */
            double angle2 = angle * angle;
            B = 0.5 - angle2/24 + angle2 * angle2/720;
            C = 1/6 - angle2/120;
        }
        Eigen::Matrix3d vHat = SO3::SkewSym(Rvec);
        return ( Eigen::Matrix3d::Identity() + B * vHat + C * vHat * vHat );
    }


    inline static Eigen::Matrix3d InvJl(const Eigen::Vector3d & Rvec)
    {
        double angle = Rvec.norm();
        double D = 1/12;
        if(angle > 1e-6){
            D = (1/angle - std::sin(angle)/(2*(1-std::cos(angle))))/angle;
        }
        else
        {
            /**@brief use Taylor expansion to increase accuracy */
            double angle2 = angle * angle;
            D = ( 1/12 - angle2/180 ) / ( 1 - angle2/12 + (angle2 * angle2)/360 );
        }
        Eigen::Matrix3d vHat = SO3::SkewSym(Rvec);
        return ( Eigen::Matrix3d::Identity() - 0.5 * vHat + D * vHat * vHat );
    }


    inline static Eigen::Matrix3d Jr(const Eigen::Vector3d & Rvec)
    {
        return SO3::Jl(-Rvec);
    }


    inline static Eigen::Matrix3d InvJr(const Eigen::Vector3d & Rvec)
    {
        return SO3::InvJl(-Rvec);
    }


    // Some trivial operations that is not usually necessary
    inline static Eigen::Matrix3d Inv(const Eigen::Matrix3d & Rmatr)
    {
        return Rmatr.transpose();  /* use transposeInPlace() for A = A'; */
    }


    inline static Eigen::Matrix3d Ad(const Eigen::Matrix3d & Rmatr)
    {
        return Rmatr;
    }


    inline static Eigen::Matrix3d InvAd(const Eigen::Matrix3d & Rmatr)
    {
        return Rmatr.transpose(); /* use transposeInPlace() for A = A'; */
    }


};


class SE3 {

private:

    SE3(){}

public:

    typedef Eigen::Matrix3d Matrix3d;
    typedef Eigen::Matrix4d Matrix4d;
    typedef Eigen::Vector3d Vector3d;
    typedef Eigen::Matrix6d Matrix6d;
    typedef Eigen::Vector6d Vector6d;


public:


    inline static Eigen::Matrix3d SkewSym(const Eigen::Vector3d & v)
    {
        Eigen::Matrix3d m;
        m <<   0,  -v(2), v(1),
              v(2),  0,  -v(0),
             -v(1), v(0),  0;
        return m;
    }

    // Lie algebra for SE3
    inline static Eigen::Matrix4d Hat(const Eigen::Vector6d & v)
    {
        Eigen::Matrix4d m = Eigen::Matrix4d::Zero();
        m.topLeftCorner<3,3>() = SE3::SkewSym( v.tail<3>() );
        m.topRightCorner<3,1>() = v.head<3>();
        return m;
    }

    // Lie algebra for Ad(SE(3))
    inline static Eigen::Matrix6d adHat(const Eigen::Vector6d & v)
    {
        Eigen::Matrix6d m;
        m.topLeftCorner<3,3>() = SE3::SkewSym( v.tail<3>() );
        m.topRightCorner<3,3>() = SE3::SkewSym( v.head<3>() );
        m.bottomLeftCorner<3,3>().setZero();
        m.bottomRightCorner<3,3>() = SE3::SkewSym( v.tail<3>() );
        return m;
    }

    // Generators for Lie algebra of SE(3)
    inline static Eigen::Matrix4d E (int idx)
    {
        Eigen::Vector6d v;
        switch (idx)
        {
            case 0:
                v << 1, 0, 0, 0, 0, 0;
                return SE3::Hat(v);
            case 1:
                v << 0, 1, 0, 0, 0, 0;
                return SE3::Hat(v);
            case 2:
                v << 0, 0, 1, 0, 0, 0;
                return SE3::Hat(v);
            case 3:
                v << 0, 0, 0, 1, 0, 0;
                return SE3::Hat(v);
            case 4:
                v << 0, 0, 0, 0, 1, 0;
                return SE3::Hat(v);
            case 5:
                v << 0, 0, 0, 0, 0, 1;
                return SE3::Hat(v);
            default:
                printf("index out of bound! @ SE3::E(idx)\n");
                exit(1);
        }
    }

    // Generators for Lie algebra of Ad(SE(3))
    inline static Eigen::Matrix6d adE (int idx)
    {
        Eigen::Vector6d v;
        switch (idx)
        {
            case 0:
                v << 1, 0, 0, 0, 0, 0;
                return SE3::adHat(v);
            case 1:
                v << 0, 1, 0, 0, 0, 0;
                return SE3::adHat(v);
            case 2:
                v << 0, 0, 1, 0, 0, 0;
                return SE3::adHat(v);
            case 3:
                v << 0, 0, 0, 1, 0, 0;
                return SE3::adHat(v);
            case 4:
                v << 0, 0, 0, 0, 1, 0;
                return SE3::adHat(v);
            case 5:
                v << 0, 0, 0, 0, 0, 1;
                return SE3::adHat(v);
            default:
                printf("index out of bound! @ SE3::adE(idx)\n");
                exit(1);
        }
    }

    // transform from vector form storage to matrix form storage
    inline static Eigen::Matrix4d V2M (const Eigen::Vector6d & v_rt)
    {
        Eigen::Matrix4d m_Rt = Eigen::Matrix4d::Identity();
        m_Rt.topLeftCorner<3,3>() = SO3::Exp(v_rt.tail<3>());
        m_Rt.topRightCorner<3,1>() = v_rt.head<3>();
        return m_Rt;
    }

    // transform from matrix form storage to vector form storage
    inline static Eigen::Vector6d M2V (const Eigen::Matrix4d & m_Rt)
    {
        Eigen::Vector6d v_rt;
        v_rt.head<3>() = m_Rt.topRightCorner<3,1>();
        v_rt.tail<3>() = SO3::Log(m_Rt.topLeftCorner<3,3>());
        return v_rt;
    }


    inline static Eigen::Matrix4d Exp (const Eigen::Vector6d & Tvec)
    {
        Eigen::Matrix4d m = Eigen::Matrix4d::Identity();
        m.topLeftCorner<3,3>() = SO3::Exp(Tvec.tail<3>());
        m.topRightCorner<3,1>() = SO3::Jl(Tvec.tail<3>()) * Tvec.head<3>();
        return m;
    }


    inline static Eigen::Vector6d Log (const Eigen::Matrix4d & Tmatr)
    {
        Eigen::Vector6d v;
        v.tail<3>() = SO3::Log(Tmatr.topLeftCorner<3,3>());
        v.head<3>() = SO3::InvJl(v.tail<3>()) * Tmatr.topRightCorner<3,1>();
        return v;
    }


    // Intermediate matrix for Jl, InvJl
    inline static Eigen::Matrix3d Ql (const Eigen::Vector6d & Tvec)
    {
        double angle = Tvec.tail<3>().norm();
        double C = 1/6;
        double E = -1/24;
        double F = -1/60;
        if(angle > 1e-6){
            double sina = std::sin(angle);
            double cosa = std::cos(angle);
            C = (angle - sina)/(angle*angle*angle);
            E = (1 - 0.5 * angle*angle - cosa)/(angle*angle*angle*angle);
            F = E - (6*C-1)/(2*angle*angle);
        }
        else
        {
            /**@brief use Taylor expansion to increase accuracy */
            double angle2 = angle * angle;
            C = 1/6 - angle2/120;
            E = -1/24 + angle2/720;
            F = -1/60 + angle2/720;
        }
        Eigen::Matrix3d uHat = SO3::SkewSym(Tvec.head<3>());
        Eigen::Matrix3d wHat = SO3::SkewSym(Tvec.tail<3>());
        Eigen::Matrix3d m;
        m = 0.5 * uHat
                + C * ( wHat*uHat + uHat*wHat + wHat*uHat*wHat )
                - E * ( wHat*wHat*uHat + uHat*wHat*wHat - 3 * wHat*uHat*wHat)
                - 0.5 * F * ( wHat*uHat*wHat*wHat + wHat*wHat*uHat*wHat );
        return m;
    }


    inline static Eigen::Matrix6d Jl (const Eigen::Vector6d & Tvec)
    {
        Eigen::Matrix6d m;
        Eigen::Matrix3d t_Jlw = SO3::Jl(Tvec.tail<3>());
        m.topLeftCorner<3,3>() = t_Jlw;
        m.topRightCorner<3,3>() = SE3::Ql(Tvec);
        m.bottomLeftCorner<3,3>().setZero();
        m.bottomRightCorner<3,3>() = t_Jlw;
        return m;
    }


    inline static Eigen::Matrix6d InvJl (const Eigen::Vector6d & Tvec)
    {
        Eigen::Matrix6d m;
        Eigen::Matrix3d t_InvJlw = SO3::InvJl(Tvec.tail<3>());
        m.topLeftCorner<3,3>() = t_InvJlw;
        m.topRightCorner<3,3>() = - t_InvJlw * SE3::Ql(Tvec) * t_InvJlw;
        m.bottomLeftCorner<3,3>().setZero();
        m.bottomRightCorner<3,3>() = t_InvJlw;
        return m;
    }


    inline static Eigen::Matrix6d Jr (const Eigen::Vector6d & Tvec)
    {
        return SE3::Jl(-Tvec);
    }


    inline static Eigen::Matrix6d InvJr (const Eigen::Vector6d & Tvec)
    {
        return SE3::InvJl(-Tvec);
    }


    inline static Eigen::Matrix4d Inv(const Eigen::Matrix4d & Tmatr)
    {
        Eigen::Matrix3d invR = Tmatr.block<3, 3>(0, 0).transpose();
        Eigen::Vector3d t = Tmatr.block<3, 1>(0, 3);
        Eigen::Matrix4d m = Eigen::Matrix4d::Identity();
        m.topLeftCorner<3,3>() = invR;
        m.topRightCorner<3,1>() = -invR * t;
        return m;
    }


    inline static Eigen::Matrix6d Ad(const Eigen::Matrix4d & Tmatr)
    {
        Eigen::Matrix3d R = Tmatr.block<3, 3>(0, 0);
        Eigen::Vector3d t = Tmatr.block<3, 1>(0, 3);
        Eigen::Matrix6d m;
        m.topLeftCorner<3,3>() = R;
        m.topRightCorner<3,3>() = SO3::SkewSym(t) * R;
        m.bottomLeftCorner<3,3>().setZero();
        m.bottomRightCorner<3,3>() = R;
        return m;
    }


    inline static Eigen::Matrix6d InvAd(const Eigen::Matrix4d & Tmatr)
    {
        Eigen::Matrix3d invR = Tmatr.block<3, 3>(0, 0).transpose();
        Eigen::Vector3d t = Tmatr.block<3, 1>(0, 3);
        Eigen::Matrix6d m;
        m.topLeftCorner<3,3>() = invR;
        m.topRightCorner<3,3>() = -invR * SO3::SkewSym(t);
        m.bottomLeftCorner<3,3>().setZero();
        m.bottomRightCorner<3,3>() = invR;
        return m;
    }

    

};


/*
 * Implementation for SO(2) and SE(2)
 * Tested on January 7, 2018
 */


class SO2 {

private:
    SO2(){}

public:

    typedef Eigen::Matrix2d Matrix2d;
    typedef Eigen::Matrix3d Matrix3d;
    typedef Eigen::Vector2d Vector2d;
    typedef Eigen::Vector3d Vector3d;


public:


    inline static Eigen::Matrix2d SkewSym(double angle)
    {
        Eigen::Matrix2d m;
        m << 0,    -angle,
             angle, 0;
        return m;
    }


    inline static Eigen::Matrix2d Hat(double angle)
    {
        Eigen::Matrix2d m;
        m << 0,    -angle,
             angle, 0;
        return m;
    }


    inline static Eigen::Matrix2d Exp(const Eigen::Matrix<double, 1, 1> & rvec)
    {
        double angle = rvec(0);
        double sina = std::sin(angle);
        double cosa = std::cos(angle);
        Eigen::Matrix2d m;
        m << cosa, -sina,
             sina,  cosa;
        return m;
    }


    inline static Eigen::Matrix<double, 1, 1> Log(const Eigen::Matrix2d & Rmatr)
    {
        double sina = Rmatr(1,0);
        double cosa = Rmatr(0,0);
        Eigen::Matrix<double, 1, 1> rvec;
        rvec(0) = std::atan2(sina, cosa);
        return rvec;
    }


    // Some trivial operations that is not usually necessary
    inline static Eigen::Matrix2d Inv(const Eigen::Matrix2d & Rmatr)
    {
        return Rmatr.transpose();  /* use transposeInPlace() for A = A'; */
    }


    inline static Eigen::Matrix2d Ad(const Eigen::Matrix2d & Rmatr)
    {
        return Rmatr;
    }


    inline static Eigen::Matrix2d InvAd(const Eigen::Matrix2d & Rmatr)
    {
        return Rmatr.transpose(); /* use transposeInPlace() for A = A'; */
    }


};


class SE2 {

private:

    SE2(){}

public:

    typedef Eigen::Matrix2d Matrix2d;
    typedef Eigen::Matrix3d Matrix3d;
    typedef Eigen::Vector2d Vector2d;
    typedef Eigen::Vector3d Vector3d;


public:


    // transform from Vector form storage to Matrix form storage
    inline static Eigen::Matrix3d V2M(const Eigen::Vector3d & v_rt)
    {
        double angle = v_rt(2);
        double sina = std::sin(angle);
        double cosa = std::cos(angle);
        Eigen::Matrix3d m;
        m << cosa, -sina, v_rt(0),
             sina,  cosa, v_rt(1),
             0,     0,    1;
        return m;
    }


    // transform from Matrix form storage to Vector form storage
    inline static Eigen::Vector3d M2V(const Eigen::Matrix3d & m_Rt)
    {
        double sina = m_Rt(1,0);
        double cosa = m_Rt(0,0);
        double angle = std::atan2(sina, cosa);
        Eigen::Vector3d v_rt( m_Rt(0,2),
                              m_Rt(1,2),
                              angle);
        return v_rt;
    }



    inline static Eigen::Matrix2d SkewSym(double angle)
    {
        Eigen::Matrix2d m;
        m << 0,    -angle,
             angle, 0;
        return m;
    }



    inline static Eigen::Matrix3d Exp(const Eigen::Vector3d & Tvec)
    {
        double angle = Tvec(2);
        double sina = std::sin(angle);
        double cosa = std::cos(angle);
        double A = 1;
        double B = 0;
        if(std::abs(angle)>1e-6){
            A = sina/angle;
            B = (1-cosa)/angle;
        }
        else
        {
            /**@brief use Taylor expansion to increase accuracy */
            double angle2 = angle * angle;
            double angle3 = angle2 * angle;
            A = 1 - angle2/6 + (angle2 * angle2)/120;
            B = angle/2 - angle3/24 + ( angle2 * angle3 )/720;
        }
        Eigen::Matrix3d m;
        m << cosa, -sina, A*Tvec(0)-B*Tvec(1),
             sina,  cosa, B*Tvec(0)+A*Tvec(1),
             0,     0,    1;
        return m;
    }



    inline static Eigen::Vector3d Log(const Eigen::Matrix3d & Tmatr)
    {
        double sina = Tmatr(1,0);
        double cosa = Tmatr(0,0);
        double angle = std::atan2(sina, cosa);
        double iA = 1;
        double iB = angle/2;
        if(std::abs(angle)>1e-6){
            iA = angle*sina/(2-2*cosa);
        }
        else
        {
            /**@brief use Taylor expansion to increase accuracy */
            double angle2 = angle * angle;
            double angle4 = angle2 * angle2;
            iA = (1 - angle2/6 + angle4/120) / (1 - angle2/12 + angle4/360);
        }
        Eigen::Vector3d v( iA*Tmatr(0,2) + iB*Tmatr(1,2),
                          -iB*Tmatr(0,2) + iA*Tmatr(1,2),
                           angle);
        return v;
    }



    inline static Eigen::Matrix3d Jl(const Eigen::Vector3d & Tvec)
    {
        double x = Tvec(0);
        double y = Tvec(1);
        double angle = Tvec(2);
        double A = 1;
        double B = 0;
        double H1 = 0;
        double H2 = 0;
        if(std::abs(angle)>1e-6){
            double sina = std::sin(angle);
            double cosa = std::cos(angle);
            A = sina/angle;
            B = (1-cosa)/angle;
            double aa = angle*angle;
            H1 = (angle-sina)/aa;
            H2 = (cosa + aa/2 - 1)/aa;
        }
        else
        {
            /**@brief use Taylor expansion to increase accuracy */
            double sina = std::sin(angle);
            double cosa = std::cos(angle);
            double angle2 = angle * angle;
            double angle3 = angle2 * angle;
            A = 1 - angle2/6 + (angle2 * angle2)/120;
            B = angle/2 - angle3/24 + ( angle2 * angle3 )/720;
            H1 = angle/6 - angle3/120;
            H2 = angle2/24 - ( angle2 *angle2 ) / 720;
        }
        double q1 =  y/2 + H1*x - H2*y;
        double q2 = -x/2 + H2*x + H1*y;
        Eigen::Matrix3d m;
        m << A, -B, q1,
             B,  A, q2,
             0,  0,  1;
        return m;
    }



    inline static Eigen::Matrix3d InvJl(const Eigen::Vector3d & Tvec)
    {
        double x = Tvec(0);
        double y = Tvec(1);
        double angle = Tvec(2);
        double iA = 1;
        double iB = 0;
        double H1 = 0;
        double H2 = 0;
        if(std::abs(angle)>1e-6){
            double sina = std::sin(angle);
            double cosa = std::cos(angle);
            iA = angle*sina/(2-2*cosa);
            iB = angle/2;
            double aa = angle*angle;
            H1 = (angle-sina)/aa;
            H2 = (cosa + aa/2 - 1)/aa;
        }
        else
        {
            /**@brief use Taylor expansion to increase accuracy */
            double sina = std::sin(angle);
            double cosa = std::cos(angle);
            double angle2 = angle * angle;
            double angle3 = angle2 * angle;
            double angle4 = angle2 * angle2;
            iA = (1 - angle2/6 + angle4/120) / (1 - angle2/12 + angle4/360);
            iB = angle/2;
            H1 = angle/6 - angle3/120;
            H2 = angle2/24 - angle4 / 720;
        }
        double q1 =  y/2 + H1*x - H2*y;
        double q2 = -x/2 + H2*x + H1*y;
        double iq1 =  iA * q1 + iB * q2;
        double iq2 = -iB * q1 + iA * q2;
        Eigen::Matrix3d m;
        m << iA, iB, -iq1,
            -iB, iA, -iq2,
              0,  0,  1;
        return m;
    }



    inline static Eigen::Matrix3d Jr(const Eigen::Vector3d & Tvec)
    {
        return SE2::Jl(-Tvec);
    }



    inline static Eigen::Matrix3d InvJr(const Eigen::Vector3d & Tvec)
    {
        return SE2::InvJl(-Tvec);
    }



    inline static Eigen::Matrix3d Inv(const Eigen::Matrix3d & Tmatr)
    {
        Eigen::Matrix3d m;
        m << Tmatr(0,0), Tmatr(1,0), -Tmatr(0,0)*Tmatr(0,2)-Tmatr(1,0)*Tmatr(1,2),
             Tmatr(0,1), Tmatr(1,1), -Tmatr(0,1)*Tmatr(0,2)-Tmatr(1,1)*Tmatr(1,2),
             0,          0,           1;
        return m;
    }



    static Eigen::Matrix3d Ad(const Eigen::Matrix3d & Tmatr)
    {
        Eigen::Matrix3d m;
        m << Tmatr(0,0), Tmatr(0,1),  Tmatr(1,2),
             Tmatr(1,0), Tmatr(1,1), -Tmatr(0,2),
             0,          0,           1;
        return m;
    }



    static Eigen::Matrix3d InvAd(const Eigen::Matrix3d & Tmatr)
    {
        Eigen::Matrix3d m;
        m << Tmatr(0,0), Tmatr(1,0), -Tmatr(0,0)*Tmatr(1,2)+Tmatr(1,0)*Tmatr(0,2),
             Tmatr(0,1), Tmatr(1,1), -Tmatr(0,1)*Tmatr(1,2)+Tmatr(1,1)*Tmatr(0,2),
             0,          0,           1;
        // an equivalent expression
        /*
        m << Tmatr(0,0), Tmatr(1,0), -Tmatr(0,1)*Tmatr(0,2)-Tmatr(1,1)*Tmatr(1,2),
             Tmatr(0,1), Tmatr(1,1),  Tmatr(0,0)*Tmatr(0,2)+Tmatr(1,0)*Tmatr(1,2),
             0,          0,           1;
        */
        return m;
    }

    
};



#endif //SLAM_PLUS_PLUS_LIEGROUP_H
