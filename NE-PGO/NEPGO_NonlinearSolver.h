//
// This file is part of the "CB-PGO vs VB-PGO C++ Software Package"
//
// Copyright and license (c) 2018-2020 Fang Bai <fang dot bai at yahoo dot com> @UTS:CAS; 
// All rights reserved.
// Maintained by Fang Bai <fang.bai@yahoo.com; fang.bai@student.uts.edu.au>
// The software is published under GNU General Public License. See GNU_GENERAL_PUBLIC_LICENSE.txt
//
// This is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the software package. If not, see <http://www.gnu.org/licenses/>.
//
// For academic use, please cite our publication:
// "Sparse Pose Graph Optimization in Cycle Space"
// Fang Bai, Teresa A. Vidal-Calleja, Giorgio Grisetti
// IEEE Transactions on Robotics
//


#ifndef SLAM_PLUS_PLUS_NEPGO_NONLINEARSOLVER_H
#define SLAM_PLUS_PLUS_NEPGO_NONLINEARSOLVER_H


#include <iostream>
#include <fstream>

#include <stdio.h> // printf
#include <algorithm>
#include <numeric>
#include <queue>
#ifdef _OPENMP
#include <omp.h>
#endif // _OPENMP
#include <vector>
// #include <list>



#include "slam/LinearSolver_UberBlock.h" // linear solver
#include "slam/ConfigSolvers.h" // nonlinear graph solvers
#include "slam/Marginals.h" // covariance calculation
#include "slam/Distances.h" // distance calculation
#include "slam_app/ParsePrimitives.h"
#include "slam/Parser.h"
#include "eigen/Eigen/SVD" // JacobiSVD
#include "slam/MemUsage.h"


#include "LieGroup.h" // Lie Group implementation
#include "LieGroupSE2_Types.h" // SE(2) types
#include "LieGroupSE3_Types.h" // SE(3) types


#include "ChordalInitialization.h" // chordal initialization


#include "ComparePoses.h"




namespace msc_func
{
    template<class Container>
    void SplitString(const std::string &str, Container &container) {
        std::istringstream iss(str);
        std::copy(std::istream_iterator<std::string>(iss),
                  std::istream_iterator<std::string>(),
                  std::back_inserter(container));
    }
}



template <const bool b_2D_SLAM = true>
class CNEPGO_NonlinearSolver{

public:

    typedef typename CChooseType<CVertexPose2D, CVertexPose3D, b_2D_SLAM>::_TyResult _TyVertexPose; /**< @brief pose vertex type */
    typedef typename CChooseType<CEdgePose2D, CEdgePose3D, b_2D_SLAM>::_TyResult _TyEdgePose; /**< @brief pose-pose edge type */

    typedef typename MakeTypelist(_TyVertexPose) _TyVertexTypelist; /**< @brief list of vertex types */
    typedef typename MakeTypelist(_TyEdgePose) _TyEdgeTypelist; /**< @brief list of edge types */

    typedef CFlatSystem<_TyVertexPose, _TyVertexTypelist, _TyEdgePose, _TyEdgeTypelist> CSystemType; /**< @brief optimized system type */
    typedef CLinearSolver_UberBlock<typename CSystemType::_TyHessianMatrixBlockList> CLinearSolverType; /**< @brief linear solver type */

    typedef CNonlinearSolver_Lambda<CSystemType, CLinearSolverType> CNonlinearSolverType; /**< @brief nonlinear solver type */



    enum {
        n_dimension = (b_2D_SLAM)? 3 : 6, /**< @brief vertex dimension */
    };

    typedef Eigen::Matrix<double, n_dimension, n_dimension> _TyJacobianMatrix; /**< @copydoc _TyEdgeData::_TyMatrix */
    typedef Eigen::Matrix<double, n_dimension, 1> _TyLieAlgebraVector; /**< @copydoc _TyEdgeData::_TyVector */

    typedef typename LieGroup<b_2D_SLAM>::_TySE SE;  // Lie Group SE Operations

    typedef typename CSystemType::_TyVertexMultiPool _TyVertexMultiPool;
    typedef typename CSystemType::_TyEdgeMultiPool _TyEdgeMultiPool;

    enum { n_rot_matrix_dimension = (b_2D_SLAM)? 2 : 3,
           n_rot_vector_dimension = (b_2D_SLAM)? 1 : 3};

    typedef typename LieGroup<b_2D_SLAM>::_TySO SO;  // Lie Group SO Operations



protected:

    size_t m_n_max_nonlinear_iteration_num;

    double m_f_nonlinear_error_thresh;

    CSystemType m_system;

    CNonlinearSolverType m_solver;

    std::vector<double> m_v_f_objective_function;

    std::vector<double> m_v_f_perturbation;

    double m_f_time_optimization;

    int m_n_used_iteration;


    bool m_b_dump_vertex_per_iter;

    std::string m_str_output_dir;

public:

    CNEPGO_NonlinearSolver(size_t n_max_nonlinear_iteration_num = 20, double f_nonlinear_error_thresh = .001)
            :m_n_max_nonlinear_iteration_num(n_max_nonlinear_iteration_num),
             m_f_nonlinear_error_thresh(f_nonlinear_error_thresh),
             m_solver(m_system, TIncrementalSolveSetting(), TMarginalsComputationPolicy(),
                      true /*verbose*/),
             m_b_dump_vertex_per_iter(false)
    {

    }


    int Optimize(size_t n_max_iterations, double f_error_threshold)
    {
        if (m_b_dump_vertex_per_iter)
        {
            m_solver.SetDumpVertexPerIter(m_b_dump_vertex_per_iter, m_str_output_dir);
        }

        CTimer time;
        CTimerSampler tic(time);
        m_solver.Optimize(n_max_iterations, f_error_threshold);
        tic.Accum_DiffSample(m_f_time_optimization);
        m_solver.Dump();

        m_v_f_objective_function = m_solver.Get_v_f_objective_function();

        m_v_f_perturbation = m_solver.Get_v_f_perturbation();

        m_n_used_iteration = m_v_f_objective_function.size() - 1;

        std::cout << "\n\n";

        PrintSummaryInfo(std::cout);

        return m_n_used_iteration;
    }

    int Optimize(size_t n_max_iterations)
    {
        return this->Optimize(n_max_iterations, m_f_nonlinear_error_thresh);
    }


    int Optimize(double f_error_threshold)
    {
        return this->Optimize(m_n_max_nonlinear_iteration_num, f_error_threshold);
    }


    int Optimize()
    {
        return this->Optimize(m_n_max_nonlinear_iteration_num, m_f_nonlinear_error_thresh);
    }


    bool ChordalInit ()
    {
        ChordalInitialization<n_rot_matrix_dimension> chordInit;
        // add edge from graph system
        _TyEdgeMultiPool & r_edge_pool = m_system.r_Edge_Pool();
        for (size_t i = 0, n = r_edge_pool.n_Size(); i < n; ++i)
        {
            _TyEdgePose & edge = r_edge_pool[i];
            const _TyLieAlgebraVector & val_edge  = edge.v_Measurement();
            Eigen::Matrix<double, n_rot_vector_dimension, 1> vr= val_edge.template tail<n_rot_vector_dimension>();
            chordInit.addEdge(edge.n_Vertex_Id(0), edge.n_Vertex_Id(1), SO::Exp(vr));
        }
        // solve chordal optimization
        if (!chordInit.solve())
            return false;
        // set vertex orientation values
        _TyVertexMultiPool &r_vertex_pool = m_system.r_Vertex_Pool();
        for (size_t i = 0, n = r_vertex_pool.n_Size(); i < n; ++i)
        {
            _TyVertexPose &vertex = r_vertex_pool[i];
            vertex.r_v_State(). template tail<n_rot_vector_dimension>() = SO::Log(chordInit.getVertex(i));
        }
        // return true;
        return initTranslation(); // initialize the translation part as well.
    }


    /**@brief initialize translational estimate by given rotation */
    bool initTranslation ()
    {
        LinearLeastSquares<n_rot_matrix_dimension> linearLeastSquares;
        Eigen::Matrix<double, n_rot_matrix_dimension, n_rot_matrix_dimension> I_n;
        I_n.setIdentity();
        _TyEdgeMultiPool & r_edge_pool = m_system.r_Edge_Pool();
        for (size_t i = 0, n = r_edge_pool.n_Size(); i < n; ++i)
        {
            _TyEdgePose & edge = r_edge_pool[i];
            int id1 = edge.n_Vertex_Id(0);
            int id2 = edge.n_Vertex_Id(1);
            const _TyLieAlgebraVector & val_edge  = edge.v_Measurement();
            Eigen::Matrix<double, n_rot_matrix_dimension, 1> vt= val_edge.template head <n_rot_matrix_dimension>();
            _TyVertexMultiPool & r_vertex_pool = m_system.r_Vertex_Pool();
            vt = SO::Exp (r_vertex_pool[id1].v_State().template tail<n_rot_vector_dimension>() ) * vt;
            linearLeastSquares.addEdge(id1, id2, -I_n, I_n, vt);
        }
        linearLeastSquares.fixVertex(0, Eigen::Matrix<double, n_rot_matrix_dimension, 1>::Zero());
        if(!linearLeastSquares.solve())
            return false;
        _TyVertexMultiPool &r_vertex_pool = m_system.r_Vertex_Pool();
        for (size_t i = 0, n = r_vertex_pool.n_Size(); i < n; ++i)
        {
            _TyVertexPose &vertex = r_vertex_pool[i];
            vertex.r_v_State(). template head<n_rot_matrix_dimension>() = linearLeastSquares.getVertex(i);
        }
        return true;
    }


    // objective functions
    inline std::vector<double> Get_ObjFunc()
    {
        return m_v_f_objective_function;
    }

    // perturbation used for each iteration.
    inline std::vector<double> Get_Perturbation ()
    {
        return m_v_f_perturbation;
    }

    void SetDumpVertexPerIter (bool b_flag_set = false, std::string t_str_dir = std::string())
    {
        m_b_dump_vertex_per_iter = b_flag_set;
        m_str_output_dir = t_str_dir;
        if ( t_str_dir.size() > 0 && *(m_str_output_dir.end()-1) != '/' )
        {
            m_str_output_dir.push_back('/');
        }
    }

    void PrintSummaryInfo (std::ostream & os = std::cout)
    {
        os << "\nFinish NE-PGO in " << m_n_used_iteration << " iterations.\n";

        os << "\nFinal_ObjFunc ( " << *(m_v_f_objective_function.end() - 1) << " )\n\n";

        os << "\n\n##### --- summary info --- #####\n";


        os << "\nused_iterations ( " << m_n_used_iteration << " )\n\n";

        os << "sys_info ( NumEdges: " << this->m_system.n_Edge_Num()
           << "  NumVertices: " << this->m_system.n_Vertex_Num()
           << " )" << std::endl << std::endl;

        os << "Lambda_info ( Size: " << m_solver.r_Lambda().n_BlockRow_Num() << " by " << m_solver.r_Lambda().n_BlockColumn_Num()
           << "  NumNonZero: " << m_solver.r_Lambda().n_Symmetric_NonZero_Num()/(n_dimension*n_dimension)
           << "  NonZeroRatio: " << m_solver.r_Lambda().f_Symmetric_NonZero_Ratio()
           << " )" << std::endl << std::endl;

        os << "time_used ( Overall: " << m_f_time_optimization
           << " )" << std::endl << std::endl;

        os << "time_used_solve ( cholesky_solve: "
           << m_solver.Get_f_time_cholesky() << " sec"
           << " )\n\n";

        os << "time_used_solve_per_iteration ( cholesky_solve: "
           << m_solver.Get_f_time_cholesky() /m_n_used_iteration << " sec"
           << " )\n\n";


        os << "perturbation (";
        for (size_t i = 0; i < m_v_f_perturbation.size(); ++i)
            os << " " << m_v_f_perturbation[i];
        os << " )\n\n";

        os << "obj_func (";
        for (size_t i = 0; i < m_v_f_objective_function.size(); ++i)
            os << " " << m_v_f_objective_function[i];
        os << " )" << std::endl << std::endl;


        if (m_b_dump_vertex_per_iter)
        {
            os << "odometry_error (";
            std::string str_opt_file = m_str_output_dir +
                                       "v" + std::to_string(m_n_used_iteration) + ".txt";
            for ( int i = 0; i <= m_n_used_iteration; ++i)
            {
                std::string str_tmp_file = m_str_output_dir + "v" + std::to_string(i) + ".txt";
                double val = msc::CompFile<b_2D_SLAM>(str_opt_file.c_str(), str_tmp_file.c_str());
                os << " " << val;
            }
            os << " )\n\n";
        }
    }


    void PlotSystemMatrix(const char *p_s_filename, int n_scalar = 5)
    {
        m_solver.Dump_SystemMatrix(p_s_filename, n_scalar);
    }


    virtual void SaveSolution(const char *p_s_filename)
    {
        FILE * p_fw;
        if( !(p_fw = fopen(p_s_filename, "w")) )
            return;
        _TyVertexMultiPool & r_vertex_pool = m_system.r_Vertex_Pool();
        for (size_t i = 0, n = m_system.r_Vertex_Pool().n_Size(); i < n; ++i) {
            _TyVertexPose &vertex = r_vertex_pool.template r_At<_TyVertexPose>(i);
            _TyLieAlgebraVector v = vertex.v_State();
            if(b_2D_SLAM){
                fprintf(p_fw,
                             "VERTEX2 %zu %.8f %.8f %.8f\n",
                             i, v[0], v[1], v[2]
                );
            }else{
                fprintf(p_fw,
                             "VERTEX3 %zu %.8f %.8f %.8f %.8f %.8f %.8f\n",
                             i, v[0], v[1], v[2], v[3], v[4], v[5]
                );
            }
        }
        fclose(p_fw);
        printf("Estimates of vertices have already been saved into the file: \"%s\" \n", p_s_filename);
    }


    void PlotTrajectory(const char *p_s_filename = NULL)
    {
        if(p_s_filename == NULL)
            p_s_filename = "default.tga";
        if(b_2D_SLAM){
            m_system.Plot2D(p_s_filename, plot_quality::plot_Printing); // plot in print quality
        }else{
            m_system.Plot3D(p_s_filename, plot_quality::plot_Printing); // plot in print quality
        }
        printf("The trajectory has been plotted in the file: \"%s\" \n", p_s_filename);
    }


    /** @brief load edges from a given file
      * @attention interface function to be called in main */
    bool Load_Edges (const char * p_s_file_name)
    {

        std::ifstream ifs(p_s_file_name);

        std::string line;

        if( ifs.peek() == std::ifstream::traits_type::eof() )
        {
            std::cerr << "Empty measurement data (edge) file!" << std::endl;
            return false;
        }

        if( ifs.is_open() )
        {
            while (std::getline(ifs, line))
            {
                std::vector<std::string> tokens;

                msc_func::SplitString(line, tokens);

                if( !tokens.empty() )
                    if( tokens[0].compare(0, 4, "Edge") == 0 || tokens[0].compare(0, 4, "EDGE") == 0)
                    {

                        size_t n_vertex_id1 = std::atoi(tokens[1].c_str());
                        size_t n_vertex_id2 = std::atoi(tokens[2].c_str());

                        _TyLieAlgebraVector v_measurement;
                        _TyJacobianMatrix m_information;

                        if(b_2D_SLAM)
                        {
                            v_measurement << std::atof(tokens[3].c_str()), std::atof(tokens[4].c_str()), std::atof(
                                    tokens[5].c_str());

                            double m11 = std::atof(tokens[6].c_str());
                            double m12 = std::atof(tokens[7].c_str());
                            double m13 = std::atof(tokens[8].c_str());
                            double m22 = std::atof(tokens[9].c_str());
                            double m23 = std::atof(tokens[10].c_str());
                            double m33 = std::atof(tokens[11].c_str());

                            m_information << m11, m12, m13,
                                    m12, m22, m23,
                                    m13, m23, m33;
                        }
                        if(!b_2D_SLAM)
                        {


                            if (tokens[0].compare(0, 13, "EDGE_SE3:QUAT") == 0)
                            {



                                double qw = std::atof(tokens[9].c_str());

                                Eigen::Vector3d rv (std::atof(tokens[6].c_str()), std::atof(tokens[7].c_str()), std::atof(tokens[8].c_str()));

                                if (rv.norm() < 1e-15)
                                    rv.setZero();
                                else
                                    rv =  ( 2 * std::acos(qw) / rv.norm() ) * rv;

                                v_measurement << std::atof(tokens[3].c_str()), std::atof(tokens[4].c_str()), std::atof(tokens[5].c_str()),
                                        rv[0], rv[1], rv[2];


                                double m11 = std::atof(tokens[10].c_str());
                                double m12 = std::atof(tokens[11].c_str());
                                double m13 = std::atof(tokens[12].c_str());
                                double m14 = std::atof(tokens[13].c_str());
                                double m15 = std::atof(tokens[14].c_str());
                                double m16 = std::atof(tokens[15].c_str());

                                double m22 = std::atof(tokens[16].c_str());
                                double m23 = std::atof(tokens[17].c_str());
                                double m24 = std::atof(tokens[18].c_str());
                                double m25 = std::atof(tokens[19].c_str());
                                double m26 = std::atof(tokens[20].c_str());

                                double m33 = std::atof(tokens[21].c_str());
                                double m34 = std::atof(tokens[22].c_str());
                                double m35 = std::atof(tokens[23].c_str());
                                double m36 = std::atof(tokens[24].c_str());

                                double m44 = std::atof(tokens[25].c_str());
                                double m45 = std::atof(tokens[26].c_str());
                                double m46 = std::atof(tokens[27].c_str());

                                double m55 = std::atof(tokens[28].c_str());
                                double m56 = std::atof(tokens[29].c_str());

                                double m66 = std::atof(tokens[30].c_str());

                                m_information << m11, m12, m13, m14, m15, m16,
                                        m12, m22, m23, m24, m25, m26,
                                        m13, m23, m33, m34, m35, m36,
                                        m14, m24, m34, m44, m45, m46,
                                        m15, m25, m35, m45, m55, m56,
                                        m16, m26, m36, m46, m56, m66;


                            }
                            else
                            {

                                v_measurement << std::atof(tokens[3].c_str()), std::atof(tokens[4].c_str()), std::atof(tokens[5].c_str()),
                                        std::atof(tokens[6].c_str()), std::atof(tokens[7].c_str()), std::atof(tokens[8].c_str());

                                double m11 = std::atof(tokens[9].c_str());
                                double m12 = std::atof(tokens[10].c_str());
                                double m13 = std::atof(tokens[11].c_str());
                                double m14 = std::atof(tokens[12].c_str());
                                double m15 = std::atof(tokens[13].c_str());
                                double m16 = std::atof(tokens[14].c_str());

                                double m22 = std::atof(tokens[15].c_str());
                                double m23 = std::atof(tokens[16].c_str());
                                double m24 = std::atof(tokens[17].c_str());
                                double m25 = std::atof(tokens[18].c_str());
                                double m26 = std::atof(tokens[19].c_str());

                                double m33 = std::atof(tokens[20].c_str());
                                double m34 = std::atof(tokens[21].c_str());
                                double m35 = std::atof(tokens[22].c_str());
                                double m36 = std::atof(tokens[23].c_str());

                                double m44 = std::atof(tokens[24].c_str());
                                double m45 = std::atof(tokens[25].c_str());
                                double m46 = std::atof(tokens[26].c_str());

                                double m55 = std::atof(tokens[27].c_str());
                                double m56 = std::atof(tokens[28].c_str());

                                double m66 = std::atof(tokens[29].c_str());

                                m_information << m11, m12, m13, m14, m15, m16,
                                        m12, m22, m23, m24, m25, m26,
                                        m13, m23, m33, m34, m35, m36,
                                        m14, m24, m34, m44, m45, m46,
                                        m15, m25, m35, m45, m55, m56,
                                        m16, m26, m36, m46, m56, m66;

                            }


                        }



                        m_system.r_Add_Edge(
                                _TyEdgePose(n_vertex_id1, n_vertex_id2, v_measurement, m_information, m_system)
                        );

                    }
            }
        } else{
            std::cerr << "Read edge data failed! Aborting.." << std::endl;
            return false;
        }

        ifs.close();

        return true;
    }




    bool SetVertexEstimate (const char * p_s_file_name)
    {
        std::ifstream ifs(p_s_file_name);

        std::string line;

        if( ifs.peek() == std::ifstream::traits_type::eof() )
        {
            std::cerr << "Empty measurement data (edge) file!" << std::endl;
            return false;
        }

        // node id counter
        int n_node_id_cnt = -1;

        _TyVertexMultiPool & r_vertex_pool = m_system.r_Vertex_Pool();

        if( ifs.is_open() )
        {
            while (std::getline(ifs, line))
            {
                std::vector<std::string> tokens;

                msc_func::SplitString(line, tokens);

                if( !tokens.empty() )
                    if( tokens[0].compare(0, 6, "Vertex") == 0 || tokens[0].compare(0, 6, "VERTEX") == 0)
                    {

                        size_t n_vertex_id = std::atoi(tokens[1].c_str());

                        _TyLieAlgebraVector v_measurement;

                        if(b_2D_SLAM)
                        {
                            v_measurement << std::atof(tokens[2].c_str()), std::atof(tokens[3].c_str()), std::atof(
                                    tokens[4].c_str());
                        }
                        if(!b_2D_SLAM)
                        {
                            if (tokens[0].compare(0, 15, "VERTEX_SE3:QUAT") == 0)
                            {
                                double qw = std::atof(tokens[8].c_str());

                                Eigen::Vector3d rv (std::atof(tokens[5].c_str()), std::atof(tokens[6].c_str()), std::atof(tokens[7].c_str()));

                                if (rv.norm() < 1e-15)
                                    rv.setZero();
                                else
                                    rv =  ( 2 * std::acos(qw) / rv.norm() ) * rv;

                                v_measurement << std::atof(tokens[2].c_str()), std::atof(tokens[3].c_str()), std::atof(tokens[4].c_str()),
                                        rv[0], rv[1], rv[2];
                            }
                            else
                            {
                                v_measurement << std::atof(tokens[2].c_str()), std::atof(tokens[3].c_str()), std::atof(tokens[4].c_str()),
                                        std::atof(tokens[5].c_str()), std::atof(tokens[6].c_str()), std::atof(tokens[7].c_str());
                            }
                        }


                        // add vertex estimate in here.

                        _TyVertexPose & vertex = r_vertex_pool.template r_At<_TyVertexPose>(++n_node_id_cnt);

                        // set m_v_state
                        vertex.r_v_State() = v_measurement;

                    }
            }
        } else{
            std::cerr << "Read edge data failed! Aborting.." << std::endl;
            return false;
        }

        ifs.close();

        return true;
    }



};










#endif //SLAM_PLUS_PLUS_NEPGO_NONLINEARSOLVER_H
