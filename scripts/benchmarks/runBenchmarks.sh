#!/bin/bash

function MonteCarloRun ()
{

    # data file
    f_data=$1

    # number of MonteCarlo trials
    n_max_trial=$2

    # number of max iterations
    n_max_iter=$3


    file_name="${f_data##*/}"
    file_name="${file_name%.*}"
    
    dir=${file_name}

    rm $dir -rf
    mkdir $dir -p

    mkdir ${dir}/ne -p
    mkdir ${dir}/ec_mcb -p
    mkdir ${dir}/ec_fcb -p
    mkdir ${dir}/ne_chord -p
    mkdir ${dir}/ec_mcb_chord -p
    mkdir ${dir}/ec_fcb_chord -p
    


    for cnt in $(seq 1 ${n_max_trial})
    do

    
    # ---- nepgo ----#
	eval ../../bin/NE_PGO -i ${f_data} -s ${dir}/ne/s${cnt}.txt -n ${n_max_iter}



    # ---- ecpgo_mcb ----#
	eval ../../bin/EC_PGO -i ${f_data} -s ${dir}/ec_mcb/s${cnt}.txt -n ${n_max_iter}



    # ---- ecpgo_fcb ----#
if [[ $file_name != "city10k" ]] && [[ $file_name != "torus10000" ]]
then

	eval ../../bin/EC_PGO_FCB -i ${f_data} -s ${dir}/ec_fcb/s${cnt}.txt -n ${n_max_iter}

fi

   
    # ---- nepgo chordInit ----#
	eval ../../bin/NE_PGO -i ${f_data} -s ${dir}/ne_chord/s${cnt}.txt -n ${n_max_iter} -c



    # ---- ecpgo_mcb chordInit ----#
	eval ../../bin/EC_PGO -i ${f_data} -s ${dir}/ec_mcb_chord/s${cnt}.txt -n ${n_max_iter} -c



    # ---- ecpgo_fcb chordInit ----#
if [[ $file_name != "city10k" ]] && [[ $file_name != "torus10000" ]]
then

	eval ../../bin/EC_PGO_FCB -i ${f_data} -s ${dir}/ec_fcb_chord/s${cnt}.txt -n ${n_max_iter} -c

fi


    done


    
    # process data by average the statistics

    python ../python_scripts/avg_data.py ${dir}/ne/ ${file_name}_NEPGO.txt
    python ../python_scripts/avg_data.py ${dir}/ec_mcb/ ${file_name}_ECPGO_MCB.txt
    python ../python_scripts/avg_data.py ${dir}/ec_fcb/ ${file_name}_ECPGO_FCB.txt
    python ../python_scripts/avg_data.py ${dir}/ne_chord/ ${file_name}_NEPGO_Chord.txt
    python ../python_scripts/avg_data.py ${dir}/ec_mcb_chord ${file_name}_ECPGO_MCB_Chord.txt
    python ../python_scripts/avg_data.py ${dir}/ec_fcb_chord/ ${file_name}_ECPGO_FCB_Chord.txt



    # clean the directory
    rm $dir -rf


# ----- get odometry based objFunc for EC_PGO instance ----#

mkdir $dir -p

    # ECPGO_MCB
    eval ../../bin/EC_PGO -i ${f_data} -s ${file_name}_ECPGO_MCB_OdomFunc.txt -d $dir -n ${n_max_iter}
    python ./copyStatistics.py ${file_name}_ECPGO_MCB.txt ${file_name}_ECPGO_MCB_OdomFunc.txt
    rm ${file_name}_ECPGO_MCB_OdomFunc.txt

    
    # ECPGO_MCB_Chord
    eval ../../bin/EC_PGO -i ${f_data} -s ${file_name}_ECPGO_MCB_Chord_OdomFunc.txt -c -d $dir -n ${n_max_iter}
    python ./copyStatistics.py ${file_name}_ECPGO_MCB_Chord.txt ${file_name}_ECPGO_MCB_Chord_OdomFunc.txt
    rm ${file_name}_ECPGO_MCB_Chord_OdomFunc.txt
    

if [[ $file_name != "city10k" ]] && [[ $file_name != "torus10000" ]]
then

    # ECPGO_FCB
    eval ../../bin/EC_PGO_FCB -i ${f_data} -s ${file_name}_ECPGO_FCB_OdomFunc.txt -d $dir -n ${n_max_iter}
    python ./copyStatistics.py ${file_name}_ECPGO_FCB.txt ${file_name}_ECPGO_FCB_OdomFunc.txt
    rm ${file_name}_ECPGO_FCB_OdomFunc.txt
    
    # ECPGO_FCB_Chord
    eval ../../bin/EC_PGO_FCB -i ${f_data} -s ${file_name}_ECPGO_FCB_Chord_OdomFunc.txt -c -d $dir -n ${n_max_iter}
    python ./copyStatistics.py ${file_name}_ECPGO_FCB_Chord.txt ${file_name}_ECPGO_FCB_Chord_OdomFunc.txt
    rm ${file_name}_ECPGO_FCB_Chord_OdomFunc.txt
    
fi

rm $dir -rf


}



# run all files in Monte Carlo

MC_NUM=100

MonteCarloRun ../../data/MITb.txt ${MC_NUM} 10
MonteCarloRun ../../data/INTEL.txt ${MC_NUM} 10
MonteCarloRun ../../data/kitti.txt ${MC_NUM} 10
MonteCarloRun ../../data/intel.txt ${MC_NUM} 10
MonteCarloRun ../../data/M3500.txt ${MC_NUM} 10
MonteCarloRun ../../data/sphere2500.txt ${MC_NUM} 10
MonteCarloRun ../../data/city10k.txt ${MC_NUM} 10
MonteCarloRun ../../data/torus10000.txt ${MC_NUM} 10

sphere_noisy=sphere2500_0.1.txt
MonteCarloRun ../../data/${sphere_noisy} ${MC_NUM} 50


result_root_dir=../../result/benchmarks
#rm ${result_root_dir} -rf
mkdir ${result_root_dir} -p
mv *.txt ${result_root_dir}




