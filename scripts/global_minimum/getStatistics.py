import glob
import sys, os


def isfloat (value):
    try:
        float (value)
        return True
    except ValueError:
        return False
    



def checkValid (f_val):
    if ( abs(f_val - 1) < 1e-2 ):
        return 1.0
    else:
        return 0.0




def mean (lst):
    if (len(lst) > 0):
        return sum(lst) / len(lst)
    else:
        return 0

    


def getObjFunc (filename):

    f_objFunc = 0
       
    with open(filename) as fin:
        
        for line in fin:

            lstr = line.split(' ')

            if (lstr[0] == 'Final_ObjFunc'):
                for itt in lstr:
                    if ( isfloat (itt) ):
                        f_objFunc = float (itt)
                    
    return f_objFunc





def main():

    noisyFile = sys.argv[1]

    MaxMcNum = int (sys.argv[2])

    ne_lst = list()
    ec_mcb_lst = list()
    ec_fcb_lst = list()    
    ne_chord_lst = list()
    ec_mcb_chord_lst = list()
    ec_fcb_chord_lst = list()    

    
    for mc_cnt in range (1, MaxMcNum+1):

        mcdir = noisyFile + '/' + 'mc_' + str(mc_cnt) + '/';

        objGlob = getObjFunc ( mcdir + 'glob/s_global_minimum.txt' )

        objNE = getObjFunc ( mcdir + 'NE/summary.txt' )
        objEC_MCB = getObjFunc ( mcdir + 'EC_MCB/summary.txt' )
        objEC_FCB = getObjFunc ( mcdir + 'EC_FCB/summary.txt' )
        objNE_Chord = getObjFunc ( mcdir + 'NE_Chord/summary.txt' )
        objEC_MCB_Chord = getObjFunc ( mcdir + 'EC_MCB_Chord/summary.txt' )
        objEC_FCB_Chord = getObjFunc ( mcdir + 'EC_FCB_Chord/summary.txt' )

        validNE = checkValid (objNE / objGlob)
        validEC_MCB = checkValid (objEC_MCB / objGlob)
        validEC_FCB = checkValid (objEC_FCB / objGlob)
        validNE_Chord = checkValid (objNE_Chord / objGlob)
        validEC_MCB_Chord = checkValid (objEC_MCB_Chord / objGlob)
        validEC_FCB_Chord = checkValid (objEC_FCB_Chord / objGlob)
        
        #print 'NE-PGO:', validNE, ' EC-PGO:', validEC

        ne_lst.append (validNE)
        ec_mcb_lst.append (validEC_MCB)
        ec_fcb_lst.append (validEC_FCB)
        ne_chord_lst.append (validNE_Chord)
        ec_mcb_chord_lst.append (validEC_MCB_Chord)
        ec_fcb_chord_lst.append (validEC_FCB_Chord)
        
    print noisyFile,' NE:', mean(ne_lst), 'EC_MCB:', mean(ec_mcb_lst), 'EC_FCB:', mean(ec_fcb_lst), 'NE_Chord:', mean(ne_chord_lst), 'EC_MCB_Chord:', mean(ec_mcb_chord_lst), 'EC_FCB_Chord:', mean(ec_fcb_chord_lst)
        

    
if __name__ == '__main__':
    main()
