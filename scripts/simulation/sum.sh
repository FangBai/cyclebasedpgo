#/bin/bash

NumMC=100

#SPTY_RATIO=( 0.01 0.05 0.1 0.15 0.2 0.25 )

#NUM_POSES=( 500 1000 4000 7000 10000 30000 50000 70000 100000 )


SPTY_RATIO=( 0.01 0.05 0.1 0.15 0.2 0.25 )

NUM_POSES=( 1000 4000 10000 30000 )

bin_dir=../../bin

MC_TRIALS=( $(seq 1 ${NumMC} ) )

out_root_dir=../../result/simulation



for sptyRatio in "${SPTY_RATIO[@]}"
do

    OutputFile="results_${sptyRatio}.txt"

    echo " " >> "results.txt"
    echo "Sparsity: ${sptyRatio}" >> "results.txt"
    echo "--------------------- " >> "results.txt"
    python sumStatistics.py $OutputFile >> "results.txt"
    echo " " >> "results.txt"

    mv ${OutputFile} ${out_root_dir}/

done


mv results.txt ${out_root_dir}/


