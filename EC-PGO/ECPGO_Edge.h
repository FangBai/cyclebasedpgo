//
// This file is part of the "CB-PGO vs VB-PGO C++ Software Package"
//
// Copyright and license (c) 2018-2020 Fang Bai <fang dot bai at yahoo dot com> @UTS:CAS; 
// All rights reserved.
// Maintained by Fang Bai <fang.bai@yahoo.com; fang.bai@student.uts.edu.au>
// The software is published under GNU General Public License. See GNU_GENERAL_PUBLIC_LICENSE.txt
//
// This is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the software package. If not, see <http://www.gnu.org/licenses/>.
//
// For academic use, please cite our publication:
// "Sparse Pose Graph Optimization in Cycle Space"
// Fang Bai, Teresa A. Vidal-Calleja, Giorgio Grisetti
// IEEE Transactions on Robotics
//


#ifndef SLAM_PLUS_PLUS_ECPGO_EDGE_H
#define SLAM_PLUS_PLUS_ECPGO_EDGE_H

#include "LieGroup.h"

template < class VertexType >
class CECPGO_Edge {

public:

    typedef VertexType _TyVertex;
    typedef _TyVertex* _PtVertexType; /**<@brief pointer to vertex type */

    enum { n_dimension = _TyVertex::n_dimension }; /**<@brief lie algebra dimension */

    typedef typename _TyVertex::_TyLieGroupMatrix _TyLieGroupMatrix; /**< @brief Lie Group matrix type */
    typedef typename _TyVertex::_TyJacobianMatrix _TyJacobianMatrix; /**< @brief Jacobian matrix type */
    typedef typename _TyVertex::_TyLieAlgebraVector _TyLieAlgebraVector; /**< @brief Lie algebra vector space type */

    typedef typename _TyVertex::_TySE _TySE; /**<@brief lie group implementations */


    typedef CECPGO_SparseVector<_TyJacobianMatrix> _TySparseVector;
    typedef typename _TySparseVector::EntryType _TyEntry;

    typedef Eigen::Map<_TyJacobianMatrix> _TyMapEigenMatrixFromBuffer;

private:

    /** @brief binary edge, two vertices attached */
    size_t m_v_n_vertex_id[2]; /**< @brief reference vertex ID */

    _PtVertexType m_v_ptr_vertex[2]; /**<@brief pointers to vertices, faster access than IDs, not necessary to search the pool */

private:

    _TyLieGroupMatrix m_m_measurement; /**< @brief measurement matrix */

    _TyLieGroupMatrix m_m_estimation; /**< @brief estimation matrix */

    _TyJacobianMatrix m_m_information; /**< @brief information matrix. Info = L*L^T */

    /** Info = L*L^T */
    /** Cov = inv(Info) = L^{-T} * L^{-1} = L^{-T} * (L^{-T})^T  */
    /** Sqrt(Cov) = L^{-T}  */
    _TyJacobianMatrix m_m_sqrt_covariance; /**< @brief square root of the covariance matrix. L^{-T} */

    _TyLieAlgebraVector m_v_edge_error; /**< @brief eta: error of the edge. used for linearization (also RightHandSide), objFunc */

    _TyJacobianMatrix m_m_Jacobian; /**< @brief Jacobian matrix for linearized edge. J*L */

    _TyLieAlgebraVector m_v_rhs; /**< @brief right-hand-side.  -J*eta  */

    _TyLieAlgebraVector m_v_perturb; /**< @brief righ-hand_side perturabtion */
private:

    /**@brief Set this to the index of first elements in linear system
     * in accordance with SLAM++'s default implementation.*/
    size_t m_n_order; /**<@brief order of edge in linear system. Ax=b. Reordering is done automatically by Linear Solver. Not need to do it here. */

    bool m_b_flag_edge_error_need_update; /**<@brief flag to update m_v_edge_error, avoid unnecessary update */

    _TySparseVector m_spvec_jacobian_cycle_index; /** sparse vector <size_t, double*> of jacobian indexed by cycles */

    size_t m_n_edge_id;

public:

    CECPGO_Edge(){}

    /** @brief constructor from : v_measurement, information */
    explicit CECPGO_Edge(const _TyLieAlgebraVector & v_measurement, const _TyJacobianMatrix & m_information)
            :
            m_m_measurement (_TySE::V2M(v_measurement)), m_m_information (m_information),
            m_m_sqrt_covariance (m_information.inverse().llt().matrixL()),
            m_m_estimation (m_m_measurement),
            /**< @attention use measurement as initial value */
            m_b_flag_edge_error_need_update(true),
            m_spvec_jacobian_cycle_index(0)
    {
        m_v_n_vertex_id[0] = 0; m_v_n_vertex_id[1] = 0;
        m_v_ptr_vertex[0] = 0; m_v_ptr_vertex[1] = 0;
    }

    /** @brief constructor from : vertex1_id, vertexId2_id, v_measurement, information */
    explicit CECPGO_Edge(size_t n_vertex_id1, size_t n_vertex_id2,
              const _TyLieAlgebraVector & v_measurement, const _TyJacobianMatrix & m_information)
            :
            m_m_measurement (_TySE::V2M(v_measurement)), m_m_information (m_information),
            m_m_sqrt_covariance (m_information.inverse().llt().matrixL()),
            m_m_estimation (m_m_measurement),
            /**< @attention use measurement as initial value */
            m_b_flag_edge_error_need_update(true),
            m_spvec_jacobian_cycle_index(0)
    {
        m_v_n_vertex_id[0] = n_vertex_id1; m_v_n_vertex_id[1] = n_vertex_id2;
        m_v_ptr_vertex[0] = 0; m_v_ptr_vertex[1] = 0;
    }

    /** @brief constructor from : vertex1_id, vertexId2_id, vertex1_ptr, vertex2_ptr, v_measurement, information */
    CECPGO_Edge(size_t n_vertex_id1, size_t n_vertex_id2,
              _PtVertexType p_vertex_id1, _PtVertexType p_vertex_id2,
              const _TyLieAlgebraVector & v_measurement, const _TyJacobianMatrix & m_information)
            :
            m_m_measurement (_TySE::V2M(v_measurement)), m_m_information (m_information),
            m_m_sqrt_covariance (m_information.inverse().llt().matrixL()),
            m_m_estimation (m_m_measurement),
            /**< @attention use measurement as initial value */
            m_b_flag_edge_error_need_update(true),
            m_spvec_jacobian_cycle_index(0)
    {
        m_v_n_vertex_id[0] = n_vertex_id1; m_v_n_vertex_id[1] = n_vertex_id2;
        m_v_ptr_vertex[0] = p_vertex_id1; m_v_ptr_vertex[1] = p_vertex_id2;
    }


    /** append entries to m_spvec_jacobian_cycle_index */
    inline void Append_Spvec_Jacobian_Cylce_Index (const _TyEntry & entry)
    {
        m_spvec_jacobian_cycle_index.append(entry);
    }


    /** @brief set edge estiamte */
    inline void Set_EdgeEstimate(const _TyLieGroupMatrix & m_estimation){
        m_m_estimation = m_estimation;
        m_b_flag_edge_error_need_update = true;
    }


    /** @brief get edge measurement matrix */
    inline const _TyLieGroupMatrix & r_Get_Measurement () const {
        return m_m_measurement;
    }


    /** @brief get edge information matrix */
    inline const _TyJacobianMatrix & r_Get_Information () const {
        return m_m_information;
    }


    /** @brief get edge estimation */
    inline const _TyLieGroupMatrix & r_Get_EdgeEstimate() const {
        return m_m_estimation;
    }


    /** @brief get edge estimation */
    inline _TyLieGroupMatrix & r_Get_EdgeEstimate() {
        return m_m_estimation;
    }


    /** @brief update the error between estimate and measurement.
     * used in Linearization and ObjFunc, also served as RightHandSide */
    inline void Calculate_EdgeError () {
        if(m_b_flag_edge_error_need_update)
            m_v_edge_error = _TySE::Log( _TySE::Inv(m_m_measurement) * m_m_estimation );
        m_b_flag_edge_error_need_update = false;
    }


    /** @brief calculate weighted jacobian, and right-hand-side. i.e.,  J*eta, J*L */
    inline void Linearize_Edge () {
        /**@brief calculate eta. eta may be updated from linearization, or objFunc calculation. */
        this->Calculate_EdgeError();
        /**@brief calculate unweighted jacobain. i.e., J  */
        /*  check here if want to use Jr or InvJr */
        m_m_Jacobian = _TySE::Jr( m_v_edge_error );  // CHECK HERE //
        /* Only need Jr in final calculation. So we use Jr directly. */
        /**@brief calculate Right-Hand-Side,  rhs = -J*eta  */
        m_v_rhs = -m_m_Jacobian * m_v_edge_error;
        /**@brief calculate weighted jacobian. i.e., J*L   */
        m_m_Jacobian = m_m_Jacobian * m_m_sqrt_covariance;
    }

    /** @brief update edge estimate */
    /**@param [in] v_update = (A*J*L)^{T} * [ (A*J*L) * (A*J*L)^{T} ]^{-1} * [-A*J*eta + b]  */
    /**@brief perturbation = -J*eta - J*L * v_update */
    inline void Update_EdgeEstimate (const Eigen::VectorXd & v_update){
        _TyLieAlgebraVector v = _TyLieAlgebraVector::Zero();
        for (size_t i = 0, n = m_spvec_jacobian_cycle_index.size(); i < n; ++i)
        {
            _TyEntry & entry = m_spvec_jacobian_cycle_index[i];
            _TyMapEigenMatrixFromBuffer matr(entry.second);
            v += matr * v_update.segment<n_dimension>(entry.first);
        }
        m_v_perturb = this->m_v_rhs - this->m_m_Jacobian * v;  ///< calculate perturbation vector
        m_m_estimation = m_m_estimation * _TySE::Exp(m_v_perturb);
        m_b_flag_edge_error_need_update = true;
    }


    /** @brief get RightHandSide -J*eta  */
    inline const _TyLieAlgebraVector & r_Get_EdgeRhs(){
        return m_v_rhs;
    }

    /** @brief get weighted Jacobian J*L */
    inline const _TyJacobianMatrix & r_Get_EdgeJacobian(){
        return m_m_Jacobian;
    }

    /** @brief get edge value in vector form. [ translation + axis-angle ] */
    inline _TyLieAlgebraVector v_State() const {
        return _TySE::M2V( m_m_estimation );
    }

    /** @brief calculate objFunc of this edge, */
    inline double f_PerturbationSquaredNorm () {
        return this->m_v_perturb.squaredNorm();
    }

    /** @brief calculate objFunc of this edge, */
    inline double f_ObjFuncPerEdge () {
        ///@brief update edge error eta. eta may be updated from linearization, or objFunc calculation. */
        this->Calculate_EdgeError();
        return m_v_edge_error.transpose() * (m_m_information * m_v_edge_error);
    }

    /** @brief return IDs of reference vertices */
    /** @arg [ 0, or 1 ] */
    inline size_t n_Vertex_Id (size_t n_vertex) const
    {
        _ASSERTE( n_vertex < 2 );
        return m_v_n_vertex_id[n_vertex];
    }

    /** @brief return Pointers to reference vertices */
    /** @arg [ 0, or 1 ] */
    inline _PtVertexType p_Vertex_Ptr(size_t n_vertex) const
    {
        _ASSERTE( n_vertex < 2 );
        return m_v_ptr_vertex[n_vertex];
    }

    /**@brief set vertex pointer of an edge */
    inline void Set_VertexPtr (size_t n_vertex, _PtVertexType v_ptr)
    {
        _ASSERTE( n_vertex < 2 );
        m_v_ptr_vertex[n_vertex] = v_ptr;
    }

    /**<@brief return pointer to the edge object, this pointer */
    /** @return this */
    inline CECPGO_Edge * Ptr () {
        return this;
    }

    /** @brief set the order of edge in final Jacobian construction */
    inline void Set_Order (size_t n_order) {
        m_n_order = n_order;
    }

    /** @brief get the order of edge in final Jacobian construction */
    inline size_t n_Order () const {
        return m_n_order;
    }


    /** @brief deduce non-zero entries in the final sytem matrix */
    template <class TDeductionPlan>
    void Deduce_SystemMatrixBlocks(TDeductionPlan & r_p)
    {
        size_t n = m_spvec_jacobian_cycle_index.size();
        for (size_t irow = 0; irow < n; ++irow)
        {
            for (size_t jcol = irow; jcol < n; ++jcol)
            {
                size_t row = m_spvec_jacobian_cycle_index[irow].first;
                size_t col = m_spvec_jacobian_cycle_index[jcol].first;

                double* pj_row = m_spvec_jacobian_cycle_index[irow].second;
                double* pj_col = m_spvec_jacobian_cycle_index[jcol].second;


#ifdef __SLAM_PLUS_PLUS_ECPGO_COLUMN_MAJOR_ALLOCATE_SYSTEM_MATRIX

                r_p.AddProductEntry(std::pair<size_t, size_t>(col, row),
                                    std::pair<double*, double*>(pj_col, pj_row));

#else

                r_p.AddProductEntry(std::pair<size_t, size_t>(row, col),
                                    std::pair<double*, double*>(pj_row, pj_col));

#endif
            }
        }
    }


    ~CECPGO_Edge(){};

};

#endif //SLAM_PLUS_PLUS_ECPGO_EDGE_H
