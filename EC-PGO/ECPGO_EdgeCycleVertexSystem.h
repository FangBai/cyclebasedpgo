//
// This file is part of the "CB-PGO vs VB-PGO C++ Software Package"
//
// Copyright and license (c) 2018-2020 Fang Bai <fang dot bai at yahoo dot com> @UTS:CAS; 
// All rights reserved.
// Maintained by Fang Bai <fang.bai@yahoo.com; fang.bai@student.uts.edu.au>
// The software is published under GNU General Public License. See GNU_GENERAL_PUBLIC_LICENSE.txt
//
// This is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the software package. If not, see <http://www.gnu.org/licenses/>.
//
// For academic use, please cite our publication:
// "Sparse Pose Graph Optimization in Cycle Space"
// Fang Bai, Teresa A. Vidal-Calleja, Giorgio Grisetti
// IEEE Transactions on Robotics
//


#ifndef SLAM_PLUS_PLUS_ECPGO_EDGECYCLEVERTEXSYSTEM_H
#define SLAM_PLUS_PLUS_ECPGO_EDGECYCLEVERTEXSYSTEM_H

#include "LieGroup.h"

/**
 * @brief File: "slam/Segregated.h"
 * @brief segregated storage templates in SLAM++
 * @brief implemention of fap_detail::fap_base.
 * @brief wrap to - foward_allocated_pool -
 */

#include "slam/Segregated.h"

/**
 * @brief File: "slam/FlatSystem.h"
 * @brief implementation of CMultiPool, based on  - foward_allocated_pool -
 * @brief methods: r_At, r_Add_Element, For_Each, For_Each_Parallel
 */
#include "slam/FlatSystem.h"

/**
 * @brief "slam/BlockMatrix.h"
 * @brief Implementation of BloackMatrix
 */
#include "slam/BlockMatrix.h"





namespace msc_func
{
    /**@fn function to compare two edges according to IDs */
    template <class EdgePtr>
    bool SortByIDs (const EdgePtr ptr_edge1, const EdgePtr ptr_edge2)
    {
        bool con1 = ( ptr_edge1->n_Vertex_Id(0) <= ptr_edge2->n_Vertex_Id(0) );
        bool con2 = ( ptr_edge1->n_Vertex_Id(0) <= ptr_edge2->n_Vertex_Id(1) );
        bool con3 = ( ptr_edge1->n_Vertex_Id(1) <= ptr_edge2->n_Vertex_Id(0) );
        bool con4 = ( ptr_edge1->n_Vertex_Id(1) <= ptr_edge2->n_Vertex_Id(1) );
        return (con1 && con2 && con3 && con4);
    }

    /**@fn sort odometry edges in ascending order according to vertex IDs */
    template <class EdgePtr>
    void SortOdometryEdges(std::vector<EdgePtr> & EdgePtrVector)
    {
        std::sort(EdgePtrVector.begin(), EdgePtrVector.end(), SortByIDs<EdgePtr>);
    }
}


/** @brief a collection of function objects for parallelism */
namespace COps {

    /** @brief function object to linearize edge */
    class CLinearize_Edge {
    public:
        inline CLinearize_Edge () {}

        template <class TEdge>
        inline void operator ()(TEdge & r_edge) const {
            r_edge.Linearize_Edge ();
        }
    };

    /** @brief function object to update edge estimate */
    class CUpdate_EdgeEstimate {
    protected:
        const Eigen::VectorXd & m_r_dx;

    public:
        inline CUpdate_EdgeEstimate(const Eigen::VectorXd &r_dx)
                :m_r_dx(r_dx)
        {}

        template <class TEdge>
        inline void operator ()(TEdge & r_edge) const
        {
            r_edge.Update_EdgeEstimate (m_r_dx);
        }
    };


    /** @brief function object to calculate cycle jacobian and rhs */
    class CCalculate_CycleJacobian_And_Rhs {
    protected:
        Eigen::VectorXd & m_r_v_rhs;
    public:
        inline CCalculate_CycleJacobian_And_Rhs (Eigen::VectorXd & r_v_rhs)
                :m_r_v_rhs(r_v_rhs)
        {}

        template <class TCycle>
        inline void operator ()(TCycle & r_cycle)
        {
            r_cycle.Calculate_CycleJacobian_And_Rhs (m_r_v_rhs);
        }
    };

    /** @brief function object to calculate objFunc */
    class CSum_ObjFunc {
    protected:
        double m_f_sum;

    public:
        inline CSum_ObjFunc ()
                :m_f_sum(0)
        {}

        template <class TEdge>
        inline void operator()(TEdge & r_edge)
        {
            m_f_sum += r_edge.f_ObjFuncPerEdge();
        }

        inline operator double () const
        {
            return m_f_sum;
        }
    };

    /** @brief function object to calculate cycle error norm */
    class CSum_CycleErrorSquaredNorm {
    protected:
        double m_f_sum;

    public:
        inline CSum_CycleErrorSquaredNorm ()
                :m_f_sum(0)
        {}

        template <class TCycle>
        inline void operator()(TCycle & r_cycle)
        {
            m_f_sum += r_cycle.f_Get_CycleErrorSquaredNorm();
        }

        inline operator double () const
        {
            return m_f_sum;
        }
    };

    /** function object for deduce system matrix from edges */
    template <class TEdge, class TDeductionPlan>
    class CDeduce_SystemMatrixBlocks {
    protected:
        TDeductionPlan & m_r_p;

    public:
        inline CDeduce_SystemMatrixBlocks (TDeductionPlan & t_r_p)
            :m_r_p(t_r_p)
        {}

        inline void operator ()(TEdge & r_edge)
        {
            r_edge.Deduce_SystemMatrixBlocks (m_r_p);
        }
    };

}


/**
 * @brief Implementation of System to be optimized
 * A template class, templated on < Edge, Cycle, Vertex >
 * Storage allocation using implementation of CMultiPool
 * @attention APIs with parallelism for: linearize system, update edge estimate, calculated objFunc
 * @attention Variable ordering and organizing Jacobians into System Matrix will be implemented in NonlinearSolver */
template <class EdgeType, class CycleType, class VertexType,
        const size_t n_pool_page_size = 1024>
class CECPGO_EdgeCycleVertexSystem {
public:

    typedef EdgeType _TyEdge; /**<@brief edge type */
    typedef CycleType _TyCycle; /**<@brief cycle type */
    typedef VertexType _TyVertex; /**<@brief vertex type */

    typedef typename _TyEdge::_PtVertexType _PtVertexType; /**<@brief pointer to vertex type */
    typedef typename _TyCycle::_PtEdgeType _PtEdgeType; /**<@brief pointer to edge type */

    enum { n_dimension = _TyEdge::n_dimension }; /**<@brief lie algebra dimension */

    typedef typename _TyEdge::_TyLieGroupMatrix _TyLieGroupMatrix; /**< @brief Lie Group matrix type */
    typedef typename _TyEdge::_TyJacobianMatrix _TyJacobianMatrix; /**< @brief Jacobian matrix type */
    typedef typename _TyEdge::_TyLieAlgebraVector _TyLieAlgebraVector; /**< @brief Lie algebra vector space type */

    typedef typename _TyEdge::_TySE _TySE; /**<@brief lie group implementations */


    typedef Eigen::Map<_TyJacobianMatrix, CUberBlockMatrix::map_Alignment> _TyMapEigenMatrixFromUberBlock;


    /** @brief MakeTypeList for CMultiPool<BaseType, TypeList>, do not forget typename */
    typedef typename MakeTypelist(_TyEdge) _TyEdgeList; /**<@brief TypeList with EdgeType */
    typedef typename MakeTypelist(_TyCycle) _TyCycleList; /**<@brief TypeList with CycleType */
    typedef typename MakeTypelist(_TyVertex) _TyVertexList; /**<@brief TypeList with VertexType */

    typedef typename MakeTypelist(_TyJacobianMatrix) _TyJacobianMatrixList;

    /** @brief CMultiPool types for edge, cycle, vertex */
    typedef multipool::CMultiPool<_TyEdge, _TyEdgeList, n_pool_page_size> _TyEdgePool; /**<@brief edge pool type */
    typedef multipool::CMultiPool<_TyCycle, _TyCycleList, n_pool_page_size> _TyCyclePool; /**<@brief cycle pool type */
    typedef multipool::CMultiPool<_TyVertex, _TyVertexList, n_pool_page_size> _TyVertexPool; /**<@brief vertex pool type */


    typedef typename _TyCycle::_TySparseVector _TySparseVector;
    typedef typename _TyCycle::_TyEntry _TyEntry;


    /**@brief Deduce non-zero blocks of system matrix */
    typedef CECPGO_DeductionPlan<_TyEdge> _TyDeductionPlan;


private:

    _TyDeductionPlan m_deduction_plan;

    _TyEdgePool m_edge_pool; /**< @brief edge storage in multipool::CMultiPool */
    _TyCyclePool m_cycle_pool; /**< @brief cycle storage in multipool::CMultiPool */
    _TyVertexPool m_vertex_pool; /**< @brief vertex storage in multipool::CMultiPool */

    CUberBlockMatrix m_m_SystemMatrix; /**<@brief system matrix storage */


private:

    size_t m_n_edge_num; /**< @brief number of edges in edge pool */
    size_t m_n_cycle_num; /**< @brief number of cycles in cycle pool */
    size_t m_n_vertex_num; /**< @brief number of vertices in vertex pool @attention 0 if vertex not needed.*/

    size_t m_n_edge_element_num;
    size_t m_n_cycle_element_num;

    bool m_b_flag_vertex_ready; /**< @brief true=vertex_ready, false=vertex_Not_ready */

    std::vector< std::vector<bool> > m_b_jacobian_sparsity_pattern; /**<@brief sparsity pattern of jacobian matrix */

    std::vector< std::vector<bool> > m_b_system_sparsity_pattern; /**<@brief sparsity pattern of system matrix */

    std::vector< std::vector<double *> > m_ptr_system_matrix; /**<@brief pointers to blocks in system matrix */

private:

    std::vector<_PtEdgeType> m_v_ptr_odometry_edges;

    bool m_b_flag_have_odometry;


private:

    Eigen::VectorXd m_v_RightHandSide;

public:

    double m_f_time_symbolic_factorization;

public:

    /** @brief default constructor, initialize pool size to 0 */
    CECPGO_EdgeCycleVertexSystem()
            : m_n_edge_num(0), m_n_cycle_num(0), m_n_vertex_num(0),
              m_n_edge_element_num(0), m_n_cycle_element_num(0),
              m_b_flag_vertex_ready(false), /**<@brief default not calculating vertices */
              m_b_flag_have_odometry(false)
    {}


    ~CECPGO_EdgeCycleVertexSystem(){}

    /**	@brief calculates the size of System Pools in memory */
    size_t n_System_Allocation_Size() const
    {
        return( sizeof(CECPGO_EdgeCycleVertexSystem<EdgeType, CycleType, VertexType, n_pool_page_size>) +
               m_vertex_pool.n_Allocation_Size() - sizeof(m_vertex_pool) +
               m_edge_pool.n_Allocation_Size() - sizeof(m_edge_pool) +
               m_cycle_pool.n_Allocation_Size() - sizeof(m_cycle_pool) );
    }

    /**	@brief calculates the size of System Matrix in memory */
    size_t n_SystemMatrix_Allocation_Size() const
    {
        return ( m_m_SystemMatrix.n_Allocation_Size() - sizeof(m_m_SystemMatrix) );
    }

    /**	@brief calculates the size of Deduction Plan in memory */
    size_t n_DeductionPlan_Allocation_Size() const
    {
        return m_deduction_plan.n_Allocation_Size() - sizeof(m_deduction_plan);
    }

    /** @brief number of elements in edge pool */
    inline size_t n_Edge_Num() const
    {
        _ASSERTE(m_n_edge_num == m_edge_pool.n_Size());
        return m_n_edge_num;
    }

    /** @brief number of elements in cycle pool */
    inline size_t n_Cycle_Num() const
    {
        _ASSERTE(m_n_cycle_num == m_cycle_pool.n_Size());
        return m_n_cycle_num;
    }

    /** @brief number of elements in vertex pool */
    inline size_t n_Vertex_Num() const
    {
        _ASSERTE(m_n_vertex_num == m_vertex_pool.n_Size());
        return m_n_vertex_num;
    }
    //


    /** @brief get reference to edge pool */
    inline _TyEdgePool & r_Edge_Pool()
    {
        return m_edge_pool;
    }
    /** @brief get reference to edge pool. const  */
    inline const _TyEdgePool & r_Edge_Pool() const
    {
        return m_edge_pool;
    }
    /** @brief get reference to cycle pool */
    inline _TyCyclePool & r_Cycle_Pool()
    {
        return m_cycle_pool;
    }
    /** @brief get reference to cycle pool. const  */
    inline const _TyCyclePool & r_Cycle_Pool() const
    {
        return m_cycle_pool;
    }
    /** @brief get reference to vertex pool */
    inline _TyVertexPool & r_Vertex_Pool()
    {
        return m_vertex_pool;
    }
    /** @brief get reference to vertex pool. const  */
    inline const _TyVertexPool & r_Vertex_Pool() const
    {
        return m_vertex_pool;
    }
    //


    /** @brief encapsulated method to add an edge to the system. */
    inline _TyEdge & r_Add_Edge (const _TyEdge & t_edge) {
        _TyEdge &r_t_new_edge = m_edge_pool.r_Add_Element(t_edge);
        r_t_new_edge.Set_Order(m_n_edge_element_num);
        m_n_edge_element_num += n_dimension;
        ++m_n_edge_num;
        /**@brief create the vertices as well */
        while ((int)t_edge.n_Vertex_Id(0) > (int)m_n_vertex_num -1
               || (int)t_edge.n_Vertex_Id(1) > (int)m_n_vertex_num -1 )
        {
            _TyVertex v(m_n_vertex_num, _TyLieGroupMatrix::Identity());
            this->r_Add_Vertex(v);
        }
        return r_t_new_edge;
    }

    /** @brief encapsulated method to add a cycle to the system. */
    inline _TyCycle & r_Add_Cycle (const _TyCycle & t_cycle) {
        _TyCycle &r_t_new_cycle = m_cycle_pool.r_Add_Element(t_cycle);
        r_t_new_cycle.Set_Order(m_n_cycle_element_num);
        m_n_cycle_element_num += n_dimension;
        /// copy jacobian matrix pointers to spvec_jacobian_edge_index
        /// seems cannot work correctly in constructors, need a specific function call to sychronize pointers
        /// @note Be careful about copy constructors used in r_Add_Element. Invalid pointers of temporary objects are copied.
        r_t_new_cycle.Alloc_Jacobian();
        /// create index in edges
        const _TySparseVector & r_sp_vec = r_t_new_cycle.r_Get_Spvec_Jacobian_Edge_Index ();
        for (size_t i = 0, n = r_sp_vec.size(); i < n; ++i)
        {
            const _TyEntry & entry = r_sp_vec[i];
            /** edge index =MUST= edge_pool index. otherwise takes additional time to find edge. */
            m_edge_pool[entry.first].Append_Spvec_Jacobian_Cylce_Index(
                    std::make_pair(r_t_new_cycle.n_Order(), // use elementwise order in jacobian for matrix-vector multiplication.
                                   entry.second));
        }
        ++m_n_cycle_num;
        return r_t_new_cycle;
    }

    /** @brief encapsulated method to add a vertex to the system. */
    inline _TyVertex & r_Add_Vertex (const _TyVertex & t_vertex) {
        _TyVertex &r_t_new_vertex = m_vertex_pool.r_Add_Element(t_vertex);
        ++m_n_vertex_num;
        return r_t_new_vertex;
    }



    void Alloc_SystemMatrixBlocks()
    {

//        CTimer timer;
//        CTimerSampler tic(timer);
//
//        double time_unused = 0;
//        double time_deduce_systematrix_blocks = 0;
//        double time_allocate_system_blocks = 0;


//        tic.Accum_DiffSample(time_unused);

        m_edge_pool.For_Each(COps::CDeduce_SystemMatrixBlocks<_TyEdge, _TyDeductionPlan>(this->m_deduction_plan));

//        tic.Accum_DiffSample(time_deduce_systematrix_blocks);


        this->m_deduction_plan.Alloc_SystemMatrixBlocks(this->m_m_SystemMatrix);


//        tic.Accum_DiffSample(time_allocate_system_blocks);

        /** assert final system matrix to be square, and dimension equals to cycle dimension */
        _ASSERTE(m_m_SystemMatrix.n_Row_Num() == m_m_SystemMatrix.n_Column_Num() &&
                 m_m_SystemMatrix.n_Row_Num() == m_n_cycle_num * n_dimension &&
                 m_m_SystemMatrix.n_BlockRow_Num() == m_n_cycle_num);

        /** simply reisize right-hand-vector accoring to cycle dimension */
        m_v_RightHandSide.resize(m_n_cycle_num * n_dimension);
//
//        std::cout << "-------- Allocating System Matrix -----------" << std::endl;
//        std::cout << "time_deduce_systematrix_blocks: " << time_deduce_systematrix_blocks << std::endl;
//        std::cout << "time allocate system matrix: " << time_allocate_system_blocks << std::endl;
//        std::cout << std::endl;

    }


    /**
     * @brief calculate System Matrix.
     * A*J*Sigma*J^{T}*A^{T} = (AJL)*(AJL)^{T}
     * Given,  Sigma = L*L^{T},  JL = weighted-edge-jacobian
     */
    void Calculate_SystemMatrix()
    {
        /**@brief Linearize edges first */
        m_edge_pool.For_Each_Parallel(COps::CLinearize_Edge());

        /**@brief Calculate Cycle Jacobian and RightHandSide */
        m_cycle_pool.For_Each_Parallel(COps::CCalculate_CycleJacobian_And_Rhs(this->m_v_RightHandSide));

        /**@brief Calculate Blocks for System Matrix Accorindg to updated Jacobians */
        m_deduction_plan.Calculate_SystemMatrixBlocks();  ///< running parallel inside
    }


    /**@brief template function to solve v_update. */
    /**@param [in] solver is a linear solver */
    /**@param [in] v_result a vector to write the result in. */
    /**@return true if cholesky factorization success, otherwise false. */
    /**@brief Calculate: v_update = (A*J*L)^{T} * [ (A*J*L) * (A*J*L)^{T} ]^{-1} * [-A*J*eta + b] */
    template <class LinearSolverType>
    bool Solve_Cycle_System (LinearSolverType & solver)
    {
        bool b_cholesky_result = false;

        /** calculate variable ordering */
        // solver.SymbolicDecomposition_Blocky(m_m_SystemMatrix/*, true*/);
        /**< not necessary, automatically called in Solve_PosDef_Blocky() */

        /**@brief Calculate: h = [ (A*J*L) * (A*J*L)^{T} ]^{-1} * [-A*J*eta + b] */
        /**@brief enable reuse previous calculated symbolic factorization */

        // solver.Solve_PosDef(this->m_m_SystemMatrix, this->m_v_RightHandSide);
        // this function won't reuse variable ordering

        /** result in m_v_RightHandSide */
        b_cholesky_result = solver.Solve_PosDef_Blocky(this->m_m_SystemMatrix, this->m_v_RightHandSide);

        m_f_time_symbolic_factorization = solver.m_f_time_symbolic_factorization;


        if(!b_cholesky_result)
        {
        //    std::cerr << "Cholesky Solve Positive Definite Failed!" << std::endl;
            return b_cholesky_result;
        }
        else{
        //    std::cout << "Cholesky Solve Positive Definite Succeed!" << std::endl;
        }

        return b_cholesky_result;
    }


    /** @brief update estimates of edges with parallelism */
    bool Update_EdgeEstimate ()
    {
        m_edge_pool.For_Each_Parallel(COps::CUpdate_EdgeEstimate(m_v_RightHandSide));
        m_b_flag_vertex_ready = false;
        return true;
    }

    /** @brief compute objFunc edge by edge with parallelism */
    /** @ CANNOT do this with parallelism */
    double f_ObjFunc ()
    {
        return m_edge_pool.For_Each(COps::CSum_ObjFunc());
    }

    /**@return norm of cycle error vector */
    /** @ CANNOT do this with parallelism */
    inline double f_Get_CycleErrorNorm()
    {
        return std::sqrt( m_cycle_pool.For_Each(COps::CSum_CycleErrorSquaredNorm()) );
    };

    inline double f_Get_EdgePerturbationNorm()
    {
        double f_tmp = .0;
        for (int i =0; i < m_edge_pool.n_Size(); ++i)
            f_tmp += m_edge_pool[i].f_PerturbationSquaredNorm ();
        return std::sqrt(f_tmp);
    };

    /**@brief Print structure of System Matrix and Cycle Jacobian Matrix */
    void PrintSystemMatrix(const char * p_s_system_fig, int n_scalar_size = 5) const
    {
        m_m_SystemMatrix.Rasterize_Symmetric(p_s_system_fig, n_scalar_size);
    }


    inline const CUberBlockMatrix & r_Lambda () const
    {
        return m_m_SystemMatrix;
    }


    /** @brief a method to compute vertex estimates, from edge estimates */
    bool Calculate_VertexEstimate () {
        /**
         * @brief calcuate a spanning tree, which is a set of edges, here we just calculate odometry.
         * @brief use pointer to edge to stand for edges in MultiPool.
         */
        /**
         * @brief Currently, the code use odometry as spanning tree without loss of generality.
         * The theoretical difference of odometry and any arbitrary spanning tree is marginal.
         * However, using odometry can avoid explicit construction of a graph object.
         * @attention The results using odometry and any spanning for EC-PGO are exactly the same.
         * Thus we would suggest odometry over spanning trees whenever odometry is possible.
         * @note Odometry are a set of edges with consecutive node IDs, i.e., std::abs(id1 - id2) == 1.
         */

        if (m_b_flag_vertex_ready)
            return true;


        if (!m_b_flag_have_odometry) {
            for (size_t i = 0; i < m_n_edge_num; ++i) {
                _TyEdge &e = m_edge_pool[i];
                int id1 = e.n_Vertex_Id(0);
                int id2 = e.n_Vertex_Id(1);
                if (id2 - id1 == 1 || id1 - id2 == 1) {
                    m_v_ptr_odometry_edges.push_back(e.Ptr());
                }
            }
            _ASSERTE(!m_v_ptr_odometry_edges.empty());
            msc_func::SortOdometryEdges(m_v_ptr_odometry_edges);
            ///< sort edges in exact odometry sequence (by node IDs).
            m_b_flag_have_odometry = true;
        }

        /**@brief fix first vetex to the identity of Special Euclidean group */
        _TyLieGroupMatrix m = _TyLieGroupMatrix::Identity();
        m_vertex_pool[0].Set_VertexValue(m);


        size_t n_vertex_cnt = 0;

        for (size_t i = 0, n = m_v_ptr_odometry_edges.size(); i < n; ++i)
        {
            EdgeType* ptr_e = m_v_ptr_odometry_edges[i];
            size_t id1 = ptr_e->n_Vertex_Id(0);
            size_t id2 = ptr_e->n_Vertex_Id(1);
            if(n_vertex_cnt == id1){
                ++n_vertex_cnt;
                m *= ptr_e->r_Get_EdgeEstimate();
                m_vertex_pool[n_vertex_cnt].Set_VertexValue(m);
            }
            else if(n_vertex_cnt == id2)
            {
                ++n_vertex_cnt;
                m *= _TySE::Inv( ptr_e->r_Get_EdgeEstimate() );
                m_vertex_pool[n_vertex_cnt].Set_VertexValue(m);
            }
        }


        m_b_flag_vertex_ready = true; // set up flag indicating vertices are correctly calculated.

        return m_b_flag_vertex_ready;
    }



    double f_Calculate_VertexBased_ObjFunc ()
    {
        double f_vertex_obj_func = 0;

        if (!m_b_flag_vertex_ready)
            this->Calculate_VertexEstimate();

        if (m_b_flag_vertex_ready)
        {
            for (size_t i = 0, n = m_edge_pool.n_Size(); i < n; ++i)
            {
                const _TyEdge & e = m_edge_pool[i];

                _TyLieAlgebraVector tmp_rsd = _TySE::Log (
                        _TySE::Inv (e.r_Get_Measurement())
                        * _TySE::Inv (m_vertex_pool[e.n_Vertex_Id(0)].r_m_State())
                        * m_vertex_pool[e.n_Vertex_Id(1)].r_m_State()
                );

                f_vertex_obj_func += tmp_rsd.transpose()
                                     * ( e.r_Get_Information() * tmp_rsd );
            }
        }
        return f_vertex_obj_func;
    }




    /** @brief dump edge estimates. copy from SLAM++ library. */
    bool Dump_EdgeEstimate (const char * p_s_filename) {
        FILE *p_fw;
        if(!(p_fw = fopen(p_s_filename, "w"))) {
            fprintf(stderr, "error: failed to open \'%s\' for writing\n", p_s_filename);
            return false;
        }
        for (size_t i = 0, n = m_edge_pool.n_Size(); i < n; ++i){
            _TyEdge & e = m_edge_pool[i];
            fprintf(p_fw, "Edge %zd %zd ", e.n_Vertex_Id(0), e.n_Vertex_Id(1));
            Eigen::VectorXd v_state = e.v_State();
            for(size_t j = 0, m = v_state.rows(); j < m; ++ j) {
                double f = v_state(j);
                if(fabs(f) > 1)
                    fprintf(p_fw, (j)? ((j + 1 == m)? " %f\n" : " %f") : ((j + 1 == m)? "%f\n" : "%f"), f);
                else
                    fprintf(p_fw, (j)? ((j + 1 == m)? " %g\n" : " %g") : ((j + 1 == m)? "%g\n" : "%g"), f);
            }
        }
        fclose(p_fw);
        return true;
    }


    /** @brief dump vertex estimates. copy from SLAM++ library. */
    bool Dump_VertexEstimate (const char * p_s_filename) {
        FILE *p_fw;
        if(!(p_fw = fopen(p_s_filename, "w")) || !m_b_flag_vertex_ready ) {
            fprintf(stderr, "error: failed to open \'%s\' for writing\n", p_s_filename);
            return false;
        }
        for (size_t i = 0, n = m_vertex_pool.n_Size(); i < n; ++i){
            _TyVertex & v = m_vertex_pool[i];
            fprintf(p_fw, "Vertex %zd ", v.n_Vertex_Id());
            Eigen::VectorXd v_state = v.v_State();
            for(size_t j = 0, m = v_state.rows(); j < m; ++ j) {
                double f = v_state(j);
                if(fabs(f) > 1)
                    fprintf(p_fw, (j)? ((j + 1 == m)? " %f\n" : " %f") : ((j + 1 == m)? "%f\n" : "%f"), f);
                else
                    fprintf(p_fw, (j)? ((j + 1 == m)? " %g\n" : " %g") : ((j + 1 == m)? "%g\n" : "%g"), f);
            }
        }
        fclose(p_fw);
        return true;
    }

    /**@brief plot functions copied from SLAM++ library */
    #include "PlottingUtilities.h"

};

#endif //SLAM_PLUS_PLUS_ECPGO_EDGECYCLEVERTEXSYSTEM_H
