//
// This file is part of the "CB-PGO vs VB-PGO C++ Software Package"
//
// Copyright and license (c) 2018-2020 Fang Bai <fang dot bai at yahoo dot com> @UTS:CAS; 
// All rights reserved.
// Maintained by Fang Bai <fang.bai@yahoo.com; fang.bai@student.uts.edu.au>
// The software is published under GNU General Public License. See GNU_GENERAL_PUBLIC_LICENSE.txt
//
// This is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the software package. If not, see <http://www.gnu.org/licenses/>.
//
// For academic use, please cite our publication:
// "Sparse Pose Graph Optimization in Cycle Space"
// Fang Bai, Teresa A. Vidal-Calleja, Giorgio Grisetti
// IEEE Transactions on Robotics
//


#ifndef SLAM_PLUS_PLUS_TYPETRAIT_H
#define SLAM_PLUS_PLUS_TYPETRAIT_H

template <const bool ty2D = true>
class CTypeTraits{

public:

    typedef Eigen::Matrix<double, 3, 3, Eigen::ColMajor> _TyLieGroupMatrix; /**< @brief Lie Group matrix type */
    typedef Eigen::Matrix<double, 3, 3, Eigen::ColMajor> _TyJacobianMatrix; /**< @brief Jacobian matrix type */
    typedef Eigen::Matrix<double, 3, 1, Eigen::ColMajor> _TyLieAlgebraVector; /**< @brief Lie algebra vector space type */

    typedef typename LieGroup<true>::_TySE _TySE; /**<@brief lie group implementations */

    enum { n_dimension = 3 }; /**@brief dimension of DOF */
};


/**@brief specialization for 3D */
template <>
class CTypeTraits < false > {

public:
    typedef Eigen::Matrix<double, 4, 4, Eigen::ColMajor> _TyLieGroupMatrix; /**< @brief Lie Group matrix type */
    typedef Eigen::Matrix<double, 6, 6, Eigen::ColMajor> _TyJacobianMatrix; /**< @brief Jacobian matrix type */
    typedef Eigen::Matrix<double, 6, 1, Eigen::ColMajor> _TyLieAlgebraVector; /**< @brief Lie algebra vector space type */

    typedef typename LieGroup<false>::_TySE _TySE; /**<@brief lie group implementations */

    enum { n_dimension = 6 }; /**@brief dimension of DOF */
};


#endif //SLAM_PLUS_PLUS_TYPETRAIT_H
