//
// This file is part of the "CB-PGO vs VB-PGO C++ Software Package"
//
// Copyright and license (c) 2018-2020 Fang Bai <fang dot bai at yahoo dot com> @UTS:CAS; 
// All rights reserved.
// Maintained by Fang Bai <fang.bai@yahoo.com; fang.bai@student.uts.edu.au>
// The software is published under GNU General Public License. See GNU_GENERAL_PUBLIC_LICENSE.txt
//
// This is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the software package. If not, see <http://www.gnu.org/licenses/>.
//
// For academic use, please cite our publication:
// "Sparse Pose Graph Optimization in Cycle Space"
// Fang Bai, Teresa A. Vidal-Calleja, Giorgio Grisetti
// IEEE Transactions on Robotics
//


#ifndef SLAM_PLUS_PLUS_ECPGO_DEDUCTIONPLAN_H
#define SLAM_PLUS_PLUS_ECPGO_DEDUCTIONPLAN_H


#include <map>
#include <vector>


/**
 *	@brief calculates std::map memory allocation size estimate
 *
 *	@tparam K is map key type
 *	@tparam P is map payload type
 *
 *	@param[in] r_map is the map to estimate memory size of
 *
 *	@return Returns the approximate size of the given map, in bytes.
 */
template <class K, class P>
static size_t n_Map_Allocation_Size(const std::map<K, P> &r_map)
{
    return (sizeof(K) + sizeof(P) + sizeof(int) + 2 * sizeof(void*)) * // size of a node (key, payload, red/black, children)
           ((r_map.size() * 2) / 3) + sizeof(std::map<K, P>); // number of leafs + other nodes + size of the struct
}







template <class EdgeType>
class CECPGO_DeductionPlan
{

    typedef typename EdgeType::_TyJacobianMatrix _TyJacobianMatrix;

    enum {
        n_pool_page_size = 4096, /**< @brief deduction pool page size */
        n_pool_memory_align = 0,  /**< @brief deduction pool memory alignment */ // see no benefit in alignment right now, this stores the TDeduction elements, those do not need to be aligned
        n_dimension = EdgeType::n_dimension,
    };


    typedef std::pair<size_t, size_t> TKey;

    typedef std::pair<double*, double*> TProductEntry; ///< pointers pair-wise non-zero entries */


    struct TDeduction
    {
        double *p_dest; /**< pointer to system matrix */
        std::vector<TProductEntry> src_list; /**< @brief pointers to deduced blocks */
    };

    typedef forward_allocated_pool<TDeduction, n_pool_page_size, n_pool_memory_align> _TyPool;
    typedef std::map<TKey, TDeduction*> _TyDeductionMap;

    typedef Eigen::Map<_TyJacobianMatrix, CUberBlockMatrix::map_Alignment> _TyMapMatrix;

protected:
    _TyPool m_p_deduction_pool; /**< @brief list of deduction pools, one per each vertex dimension */ // actually need that for parallel processing
    _TyDeductionMap m_p_deduction_list; /**< @brief list of deductions, one per each vertex dimension */


public:

    size_t n_Allocation_Size() const
    {
        size_t n_size_deduction_pool = m_p_deduction_pool.size() * sizeof(double*);
        for (size_t i = 0, n = m_p_deduction_pool.size(); i < n; ++i)
        {
            n_size_deduction_pool += m_p_deduction_pool[i].src_list.size() * sizeof(double*) * 2;
        }

        return sizeof(CECPGO_DeductionPlan<EdgeType>)
            + n_size_deduction_pool - sizeof(m_p_deduction_pool)
            + n_Map_Allocation_Size(m_p_deduction_list) - sizeof(m_p_deduction_list);
    }

    /** push a product entry referenced by <id1, id2> into deduction pool */
    inline void AddProductEntry (TKey t_key, TProductEntry t_entry)  ///< use reference, only two ints...
    {
        typename _TyDeductionMap::iterator p_red_it = m_p_deduction_list.find(t_key);
        // the block referenced by key has not been built in the pool yet
        if(p_red_it == m_p_deduction_list.end())
        {
            m_p_deduction_pool.resize(m_p_deduction_pool.size() + 1);  ///< will this be faster than Add_Element?
            // add an element in pool to describe the new block
            TDeduction *p_red = &(*(m_p_deduction_pool.end() - 1));  // The address of last pool element
            // p_dest, and p_src are not set for the new block.
            // p_src will be incrementally built by calling this function
            // p_dest will be assigned after all blocks are added.
            p_red_it = m_p_deduction_list.insert(std::pair<const TKey, TDeduction*>(t_key, p_red)).first;
        }
        std::vector<TProductEntry> &r_list = (*p_red_it).second->src_list;
        r_list.push_back(t_entry);
    }



    //Todo Use CuberBlockMatrix Storage for Jacobian, rather than local Jacobians, would it be faster? Still need allocation time, I guess...
    // CUberBlockMatrix &r_storage
    // double *p_ptr = r_storage.p_Get_DenseStorage(n_size);



    /** Allocate blocks in system matrix using deduction plan */
    void Alloc_SystemMatrixBlocks (CUberBlockMatrix & r_lambda)
    {
        // allocate r_lambda according to elements order in map (sorted).
        typename _TyDeductionMap::iterator p_red_it;
        for ( p_red_it = m_p_deduction_list.begin(); p_red_it != m_p_deduction_list.end(); ++p_red_it)
        {
            const TKey & t_key = (*p_red_it).first;
            /** use p_GetBlock_Log(), or p_FindBlock() */
#ifdef __SLAM_PLUS_PLUS_ECPGO_COLUMN_MAJOR_ALLOCATE_SYSTEM_MATRIX

//            printf("allocate UberBlock at (%d, %d)\n", t_key.second, t_key.first);

            (*p_red_it).second->p_dest = r_lambda.p_GetBlock_Log(t_key.second/n_dimension,
                                                                 t_key.first/n_dimension,
                                                                 n_dimension, n_dimension,
                                                                 true, true);

#else
            // cannot use p_GetBlock_Log for row major allocation, report errors
            (*p_red_it).second->p_dest = r_lambda.p_FindBlock(t_key.first,
                                                              t_key.second,
                                                              n_dimension, n_dimension,
                                                              true, true);

#endif

        }
    }


    /** calculate system matrix values for all blocks referenced in the pool */
    inline void Calculate_SystemMatrixBlocks()
    {
        size_t _n = m_p_deduction_pool.size();
        _ASSERTE(_n <= INT_MAX);
        int n = int(_n);
        #pragma omp parallel for if(n > 50)  ///< can do in parallel
        for(int i = 0; i < n; ++ i) {
            const TDeduction &r_red = m_p_deduction_pool[i];
            _TyMapMatrix dest_map((double*)r_red.p_dest);
            dest_map.setZero();
            for(size_t j = 0, m = r_red.src_list.size(); j < m; ++ j)
            {

#ifdef __SLAM_PLUS_PLUS_ECPGO_COLUMN_MAJOR_ALLOCATE_SYSTEM_MATRIX

                dest_map += _TyMapMatrix(r_red.src_list[j].second).transpose()
                            * _TyMapMatrix(r_red.src_list[j].first);

#else

                dest_map += _TyMapMatrix(r_red.src_list[j].first).transpose()
                          * _TyMapMatrix(r_red.src_list[j].second);

#endif
            }
        }
    }


};


#endif //SLAM_PLUS_PLUS_ECPGO_DEDUCTIONPLAN_H
