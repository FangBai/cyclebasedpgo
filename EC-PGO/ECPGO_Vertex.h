//
// This file is part of the "CB-PGO vs VB-PGO C++ Software Package"
//
// Copyright and license (c) 2018-2020 Fang Bai <fang dot bai at yahoo dot com> @UTS:CAS; 
// All rights reserved.
// Maintained by Fang Bai <fang.bai@yahoo.com; fang.bai@student.uts.edu.au>
// The software is published under GNU General Public License. See GNU_GENERAL_PUBLIC_LICENSE.txt
//
// This is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the software package. If not, see <http://www.gnu.org/licenses/>.
//
// For academic use, please cite our publication:
// "Sparse Pose Graph Optimization in Cycle Space"
// Fang Bai, Teresa A. Vidal-Calleja, Giorgio Grisetti
// IEEE Transactions on Robotics
//


#ifndef SLAM_PLUS_PLUS_ECPGO_VERTEX_H
#define SLAM_PLUS_PLUS_ECPGO_VERTEX_H


template <const bool ty2D = true>
class CTypeTraits{

public:

    typedef Eigen::Matrix<double, 3, 3, Eigen::ColMajor> _TyLieGroupMatrix; /**< @brief Lie Group matrix type */
    typedef Eigen::Matrix<double, 3, 3, Eigen::ColMajor> _TyJacobianMatrix; /**< @brief Jacobian matrix type */
    typedef Eigen::Matrix<double, 3, 1, Eigen::ColMajor> _TyLieAlgebraVector; /**< @brief Lie algebra vector space type */

    typedef typename LieGroup<true>::_TySE _TySE; /**<@brief lie group implementations */

    enum { n_dimension = 3 }; /**@brief dimension of DOF */
};


/**@brief specialization for 3D */
template <>
class CTypeTraits < false > {

public:
    typedef Eigen::Matrix<double, 4, 4, Eigen::ColMajor> _TyLieGroupMatrix; /**< @brief Lie Group matrix type */
    typedef Eigen::Matrix<double, 6, 6, Eigen::ColMajor> _TyJacobianMatrix; /**< @brief Jacobian matrix type */
    typedef Eigen::Matrix<double, 6, 1, Eigen::ColMajor> _TyLieAlgebraVector; /**< @brief Lie algebra vector space type */

    typedef typename LieGroup<false>::_TySE _TySE; /**<@brief lie group implementations */

    enum { n_dimension = 6 }; /**@brief dimension of DOF */
};



template < class TypeTraits >
class CECPGO_Vertex {

public:

    typedef typename TypeTraits::_TyJacobianMatrix _TyJacobianMatrix;
    typedef typename TypeTraits::_TyLieAlgebraVector _TyLieAlgebraVector;
    typedef typename TypeTraits::_TyLieGroupMatrix _TyLieGroupMatrix;

    typedef typename TypeTraits::_TySE _TySE;

    enum { n_dimension = TypeTraits::n_dimension };

private:

    _TyLieGroupMatrix m_m_vertex; /**<@brief vertex value in matrix form */

    size_t m_n_vertex_id; /**<@brief unique vertex ID, same as storage index */

public:

    CECPGO_Vertex(){}

    /** @brief constructor: from vertex id, vertex value in matrix form */
    CECPGO_Vertex(size_t n_id, _TyLieGroupMatrix m_value)
            : m_n_vertex_id (n_id), m_m_vertex(m_value)
    {}

    /** @brief constructor: from vertex id */
    explicit CECPGO_Vertex(size_t n_id) : m_n_vertex_id (n_id) {}

    ~CECPGO_Vertex(){}

    /** @brief set vertex value */
    /** @arg lie group element in matrix form */
    inline void Set_VertexValue (const _TyLieGroupMatrix & m_vertex) {
        m_m_vertex = m_vertex;
    }

    /** @brief return vertex value in vector form. [ translation + axis-angle ] */
    inline _TyLieAlgebraVector v_State() const {
        return _TySE::M2V( m_m_vertex );
    }

    /** @brief return vertex value in matrix form. [ R, t ] */
    inline const _TyLieGroupMatrix & r_m_State() const {
        return m_m_vertex;
    }

    /** @brief return vertex value in matrix form. [ R, t ] */
    inline _TyLieGroupMatrix & r_m_State() {
        return m_m_vertex;
    }

    /** @brief return vertex ID */
    inline size_t n_Vertex_Id () {
        return m_n_vertex_id;
    }

    /** @brief return pointer to the vertex object, this pointer */
    inline CECPGO_Vertex * Ptr () {
        return this;
    }

};


#endif //SLAM_PLUS_PLUS_ECPGO_VERTEX_H
