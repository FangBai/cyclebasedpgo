//
// This file is part of the "CB-PGO vs VB-PGO C++ Software Package"
//
// Copyright and license (c) 2018-2020 Fang Bai <fang dot bai at yahoo dot com> @UTS:CAS; 
// All rights reserved.
// Maintained by Fang Bai <fang.bai@yahoo.com; fang.bai@student.uts.edu.au>
// The software is published under GNU General Public License. See GNU_GENERAL_PUBLIC_LICENSE.txt
//
// This is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the software package. If not, see <http://www.gnu.org/licenses/>.
//
// For academic use, please cite our publication:
// "Sparse Pose Graph Optimization in Cycle Space"
// Fang Bai, Teresa A. Vidal-Calleja, Giorgio Grisetti
// IEEE Transactions on Robotics
//



#ifndef SLAM_PLUS_PLUS_PLOTTINGUTILITIES_H
#define SLAM_PLUS_PLUS_PLOTTINGUTILITIES_H

/**
 * @brief plotting utilities copied from SLAM++ library
 */



/**
 *	@brief calculates bounding box of the vertices in the system
 *
 *	@tparam Derived is Eigen derived matrix type for the first matrix argument
 *
 *	@param[out] r_v_min is filled with the minimum coordinates of the transformed vertices
 *	@param[out] r_v_max is filled with the maximum coordinates of the transformed vertices
 *	@param[in] r_t_transform is vertex transformation matrix (only vertices of dimension
 *		greater or equal to the number of columns of this matrix are considered)
 *	@param[in] b_include_const_vertices is const vertex include flag
 *		(has no effect if there are no const vertices)
 *
 *	@return Returns true if there were any vertices to initialize the bounding box,
 *		otherwise returns false.
 */
template <class Derived0, class Derived1, class Derived2>
bool Vertex_BoundingBox(Eigen::MatrixBase<Derived1> &r_v_min,
                        Eigen::MatrixBase<Derived2> &r_v_max,
                        const Eigen::MatrixBase<Derived0> &r_t_transform,
                        bool b_include_const_vertices = true) const
{
    const size_t n_input_dim = r_t_transform.cols();
    const size_t n_output_dim = r_t_transform.rows();
    // this is probably already done in Eigen::Matrix::rows() and Eigen::Matrix::cols()

    _ASSERTE(r_v_min.rows() == n_output_dim);
    _ASSERTE(r_v_max.rows() == n_output_dim);
    // make sure that the dimensions are correct

    r_v_min.setZero();
    r_v_max.setZero();
    // handle the case of no vertices

    bool b_had_vertex = false;
    Eigen::Matrix<double, Derived0::RowsAtCompileTime, 1> v_projected;
    for(size_t i = 0, n = m_vertex_pool.n_Size(); i < n; ++ i) {
        _TyLieAlgebraVector v_state = m_vertex_pool[i].v_State();
        if(size_t(v_state.rows()) < n_input_dim)
            continue; // or could append zeros in a similar fashion OpenGL attribs work
        // get vertex state of a sufficient dimension (this might skip landmarks or some other parameters)

        v_projected = r_t_transform * v_state.head(n_input_dim); // need to use Eigen::MatrixBase, unable to optimize for special matrices (e.g. identity)

        if(b_had_vertex) {
            r_v_min = r_v_min.array().min(v_projected.array()).matrix();
            r_v_max = r_v_max.array().max(v_projected.array()).matrix();
        } else {
            r_v_min = r_v_max = v_projected;
            b_had_vertex = true;
        }
    }


    return b_had_vertex;
}



/**
 *	@brief plots the selected edges as lines
 *
 *	@tparam Derived is Eigen derived matrix type for the first matrix argument
 *
 *	@param[in,out] p_bitmap is pointer to the bitmap image to plot into
 *	@param[in] r_t_transform is transformation matrix from vertex to raster coordinates
 *	@param[in] r_v_offset is raster coordinates offset
 *	@param[in] n_color is edge color
 *	@param[in] f_line_width is line width
 *	@param[in] n_min_dimension is minimum measurement dimension (inclusive)
 *	@param[in] n_max_dimension is maximum measurement dimension (inclusive)
 *	@param[in] b_invert_dim_test is dimension test inversion flag (if set,
 *		only edges with dimension outside of the [n_min_dimension, n_max_dimension]
 *		interval are plotted; if cleared, only edges inside the interval are plotted)
 *	@param[in] n_first_edge is zero-based index of the first edge to draw
 *	@param[in] n_last_edge is zero-based index of one past the last edge to draw
 *		or -1 for the number of edges in the system
 *
 *	@note This function does not attempt to draw unary edges, and for hyperedges
 *		only the first two vertices are connected by a line.
 */
template <class Derived>
void Draw_Edges(TBmp *p_bitmap, const Eigen::MatrixBase<Derived> &r_t_transform,
                const Eigen::Vector2d &r_v_offset, uint32_t n_color, float f_line_width = 1,
                size_t n_min_dimension = 0, size_t n_max_dimension = SIZE_MAX,
                bool b_invert_dim_test = false, size_t n_first_edge = 0, size_t n_last_edge = size_t(-1)) const
{
    const size_t n_min_vertex_dim = r_t_transform.cols();
    const size_t n_optimized_vertex_num = m_vertex_pool.n_Size();

    // could use for-each and traits for deciding which edge types to draw and which to skip
    for(size_t i = n_first_edge, n = (n_last_edge == size_t(-1))?
                                     m_edge_pool.n_Size() : n_last_edge; i < n; ++ i) {

        const _TyEdge & r_edge = m_edge_pool[i];

        _TyLieAlgebraVector r_t_vert0 = m_vertex_pool[r_edge.n_Vertex_Id(0)].v_State();
        _TyLieAlgebraVector r_t_vert1 = m_vertex_pool[r_edge.n_Vertex_Id(1)].v_State();

        if(size_t(r_t_vert0.rows()) < n_min_vertex_dim || size_t(r_t_vert1.rows()) < n_min_vertex_dim)
            continue;

        Eigen::Vector2d v_vert0 = r_t_transform * r_t_vert0.head(n_min_vertex_dim) + r_v_offset;
        Eigen::Vector2d v_vert1 = r_t_transform * r_t_vert1.head(n_min_vertex_dim) + r_v_offset;
        // transform to raster coords

        float f_x0 = float(v_vert0(0)), f_y0 = float(v_vert0(1));
        float f_x1 = float(v_vert1(0)), f_y1 = float(v_vert1(1));
        // convert to float

        p_bitmap->DrawLine_AA(f_x0, f_y0, f_x1, f_y1, n_color, f_line_width);
    }
    // draw edges
}



/**
 *	@brief plots the selected (optimized) vertices
 *
 *	@param[in,out] p_bitmap is pointer to the bitmap image to plot into
 *	@param[in] r_t_transform is transformation matrix from vertex to raster coordinates
 *	@param[in] r_v_offset is raster coordinates offset
 *	@param[in] n_color is edge color
 *	@param[in] n_tick_type is tick type (0 for simple cross, 1 for antialiased cross,
 *		2 for tilted cross, 3 for circle, 4 for disc, 5 for square, 6 for filled square)
 *	@param[in] f_tick_size is tick size in pixels
 *	@param[in] f_line_width is tick line width
 *	@param[in] n_min_dimension is minimum vertex dimension (inclusive)
 *	@param[in] n_max_dimension is maximum vertex dimension (inclusive)
 *	@param[in] b_invert_dim_test is dimension test inversion flag (if set,
 *		only vertices with dimension outside of the [n_min_dimension, n_max_dimension]
 *		interval are plotted; if cleared, only vertices inside the interval are plotted)
 *	@param[in] n_first_vertex is zero-based index of the first vertex to draw
 *	@param[in] n_last_vertex is zero-based index of one past the last vertex to draw
 *		or -1 for the number of vertices in the system
 */
template <class Derived>
void Draw_Vertices(TBmp *p_bitmap, const Eigen::MatrixBase<Derived> &r_t_transform,
                   const Eigen::Vector2d &r_v_offset, uint32_t n_color, int n_tick_type = 0, float f_tick_size = 2,
                   float f_line_width = 1, size_t n_min_dimension = 0, size_t n_max_dimension = SIZE_MAX,
                   bool b_invert_dim_test = false, size_t n_first_vertex = 0, size_t n_last_vertex = size_t(-1)) const
{
    if(!f_tick_size && !f_line_width)
        return;

    const size_t n_min_vertex_dim = r_t_transform.cols();

    for(size_t i = n_first_vertex, n = (n_last_vertex == size_t(-1))?
                                       m_vertex_pool.n_Size() : n_last_vertex; i < n; ++ i) {

        _TyLieAlgebraVector v_state = m_vertex_pool[i].v_State();

        size_t n_dim = v_state.rows();
        if((!b_invert_dim_test && (n_dim < n_min_dimension || n_dim > n_max_dimension)) ||
           (b_invert_dim_test && !(n_dim < n_min_dimension || n_dim > n_max_dimension)) ||
           n_dim < n_min_vertex_dim)
            continue;

        Eigen::Vector2d v_vert = r_t_transform * v_state.head(n_min_vertex_dim) + r_v_offset;
        float f_x = float(v_vert(0)), f_y = float(v_vert(1));
        // transform to raster coords, convert to float

        plot::CPlotUtils::Draw_Tick(p_bitmap, f_x, f_y, n_color, n_tick_type, f_tick_size, f_line_width);
        // draw a tick
    }
}




/**
 *	@brief plots the system as a .tga (targa) image
 *
 *	@param[in] p_s_filename is the output file name
 *	@param[in] n_smaller_side_resoluion is the resolution of the shorter side of the image. in pixels
 *	@param[in] n_max_resolution is the maximal resolution limit for either dimension, in pixels
 *		(in case the aspect ratio is too high, the longer side is set to n_max_resolution
 *		and the shorter side is scaled appropriately)
 *	@param[in] f_tick_size is vertex tick size, in pixels (advanced settings)
 *	@param[in] f_tick_line_width is vertex tick line width, in pixels (advanced settings)
 *	@param[in] f_edge_line_width is edge line width, in pixels (advanced settings)
 *	@param[in] f_landmark_edge_line_width is pose-landmark edge line width, in pixels (advanced settings)
 *	@param[in] b_dark_landmark_edges is pose-landmark color selector; if set,
 *		it is black, otherwise it is light gray (advanced settings)
 *	@param[in] b_draw_frame is flag for drawing a frame around the image
 *		(the frame is fit tightly around the image)
 *	@param[in] n_padding is padding around the image (around the frame, if rendered)
 *	@param[in] b_landmark_ticks_only is vertex rendering flag (if set, only landmarks are rendered,
 *		otherwise all vertices are rendered)
 *
 *	@return Returns true on success, false on failure.
 */
bool Plot2D(const char *p_s_filename, int n_smaller_side_resoluion = 1024,
            int n_max_resolution = 8192, float f_tick_size = 2, float f_tick_line_width = 1,
            float f_edge_line_width = 1, float f_landmark_edge_line_width = 1,
            bool b_dark_landmark_edges = false, bool b_draw_frame = true, int n_padding = 32,
            bool b_landmark_ticks_only = false) const
{
    Eigen::Vector2d v_min(-1, -1), v_max(1, 1);
    Vertex_BoundingBox(v_min, v_max, Eigen::Matrix2d::Identity());
    // find minima / maxima

    int n_width, n_height;
    double f_scale;
    plot::CPlotUtils::Calc_Resolution(n_width, n_height, f_scale, v_min, v_max,
                                      n_smaller_side_resoluion, n_max_resolution);
    // calculate image size

    Eigen::Matrix2d t_transform;
    Eigen::Vector2d v_offset;
    plot::CPlotUtils::Calc_RasterTransform(t_transform, v_offset, n_width, n_height,
                                           n_padding, f_scale, v_min, Eigen::Matrix2d::Identity());
    // update spatial transform to raster transform

    TBmp *p_image;
    if(!(p_image = TBmp::p_Alloc(n_width + 2 * n_padding, n_height + 2 * n_padding)))
        return false;

    p_image->Clear(0xffffffU);
    // white background

    if(b_draw_frame) {
        int n_tick_offset = int(ceil(fabs(f_tick_size)));
        n_padding -= n_tick_offset;
        n_width += 2 * n_tick_offset;
        n_height += 2 * n_tick_offset;
        p_image->DrawRect(n_padding, n_padding, n_width + n_padding, n_height + n_padding, 0xff000000U);
        n_padding += n_tick_offset;
        n_width -= 2 * n_tick_offset;
        n_height -= 2 * n_tick_offset;
    }
    // black borders

    enum {
        n_landmark_edge_dim = 2,
        n_landmark_vertex_dim = 2,
        n_min_vertex_dim = 2
    };
    _ASSERTE(t_transform.cols() == n_min_vertex_dim);

    Draw_Edges(p_image, t_transform, v_offset, (b_dark_landmark_edges)?
                                               0xffaaaaaaU : 0xffeeeeeeU, f_landmark_edge_line_width,
               n_landmark_edge_dim, n_landmark_edge_dim);
    // draw edges to landmarks

    const int n_tick_type = (f_tick_size > 2.5f)? plot::tick_Cross :
                            plot::tick_Cross_Subpixel, n_ctick_type = plot::tick_TiltedCross;
    Draw_Vertices(p_image, t_transform, v_offset, 0xffff0000U, n_tick_type, f_tick_size,
                  f_tick_line_width, (b_landmark_ticks_only)? int(n_landmark_vertex_dim) : 0,
                  (b_landmark_ticks_only)? int(n_landmark_vertex_dim) : SIZE_MAX);
//    Draw_ConstVertices(p_image, t_transform, v_offset, 0xff8800ffU, n_ctick_type, f_tick_size,
//                       f_tick_line_width, (b_landmark_ticks_only)? int(n_landmark_vertex_dim) : 0,
//                       (b_landmark_ticks_only)? int(n_landmark_vertex_dim) : SIZE_MAX);
    // draw all the vertices

    Draw_Edges(p_image, t_transform, v_offset, 0xff0000ffU,
               f_edge_line_width, n_landmark_edge_dim, n_landmark_edge_dim, true);
    // draw all other edges

    bool b_result = CTgaCodec::Save_TGA(p_s_filename, *p_image, false);
    p_image->Delete();

    return b_result;
}



/**
 *	@brief plots the system as a .tga (targa) image
 *
 *	This version assumes a 3D system, it plots it as its projection to the XZ plane.
 *
 *	@param[in] p_s_filename is the output file name
 *	@param[in] n_smaller_side_resoluion is the resolution of the shorter side of the image. in pixels
 *	@param[in] n_max_resolution is the maximal resolution limit for either dimension, in pixels
 *		(in case the aspect ratio is too high, the longer side is set to n_max_resolution
 *		and the shorter side is scaled appropriately)
 *	@param[in] f_tick_size is vertex tick size, in pixels (advanced settings)
 *	@param[in] f_tick_line_width is vertex tick line width, in pixels (advanced settings)
 *	@param[in] f_edge_line_width is edge line width, in pixels (advanced settings)
 *	@param[in] f_landmark_edge_line_width is pose-landmark edge line width, in pixels (advanced settings)
 *	@param[in] b_dark_landmark_edges is pose-landmark color selector; if set,
 *		it is black, otherwise it is light gray (advanced settings)
 *	@param[in] b_draw_frame is flag for drawing a frame around the image
 *		(the frame is fit tightly around the image)
 *	@param[in] n_padding is padding around the image (around the frame, if rendered)
 *	@param[in] b_landmark_ticks_only is vertex rendering flag (if set, only landmarks are rendered,
 *		otherwise all vertices are rendered)
 *
 *	@return Returns true on success, false on failure.
 */
bool Plot3D(const char *p_s_filename, int n_smaller_side_resoluion = 1024,
            int n_max_resolution = 8192, float f_tick_size = 2, float f_tick_line_width = 1,
            float f_edge_line_width = 1, float f_landmark_edge_line_width = 1,
            bool b_dark_landmark_edges = false, bool b_draw_frame = true, int n_padding = 32,
            bool b_landmark_ticks_only = false) const
{
    Eigen::Vector2d v_min(-1, -1), v_max(1, 1);
    Eigen::Matrix<double, 2, 3> t_transform;
    t_transform << 1, 0, 0, 0, 1, 0; // extract x and y coordinates
//    t_transform << 1, 0, 0, 0, 0, 1; // extract x and z coordinates
    Vertex_BoundingBox(v_min, v_max, t_transform);
    // find minima / maxima

    int n_width, n_height;
    double f_scale;
    plot::CPlotUtils::Calc_Resolution(n_width, n_height, f_scale, v_min, v_max,
                                      n_smaller_side_resoluion, n_max_resolution);
    // calculate image size

    Eigen::Vector2d v_offset;
    plot::CPlotUtils::Calc_RasterTransform(t_transform, v_offset, n_width, n_height,
                                           n_padding, f_scale, v_min, t_transform);
    // update spatial transform to raster transform

    TBmp *p_image;
    if(!(p_image = TBmp::p_Alloc(n_width + 2 * n_padding, n_height + 2 * n_padding)))
        return false;

    p_image->Clear(0xffffffU);
    // white background

    if(b_draw_frame) {
        int n_tick_offset = int(ceil(fabs(f_tick_size)));
        n_padding -= n_tick_offset;
        n_width += 2 * n_tick_offset;
        n_height += 2 * n_tick_offset;
        p_image->DrawRect(n_padding, n_padding, n_width + n_padding, n_height + n_padding, 0xff000000U);
        n_padding += n_tick_offset;
        n_width -= 2 * n_tick_offset;
        n_height -= 2 * n_tick_offset;
    }
    // black borders

    enum {
        n_landmark_edge_dim = 3,
        n_landmark_vertex_dim = 3,
        n_min_vertex_dim = 3
    };
    _ASSERTE(t_transform.cols() == n_min_vertex_dim);

    Draw_Edges(p_image, t_transform, v_offset, (b_dark_landmark_edges)?
                                               0xffaaaaaaU : 0xffeeeeeeU, f_landmark_edge_line_width,
               n_landmark_edge_dim - 1, n_landmark_edge_dim); // BA landmark edges are 2D, SE(3) landmark edges are 3D
    // draw edges to landmarks

    const int n_tick_type = (f_tick_size > 2.5f)? plot::tick_Cross :
                            plot::tick_Cross_Subpixel, n_ctick_type = plot::tick_TiltedCross;
    Draw_Vertices(p_image, t_transform, v_offset, 0xffff0000U, n_tick_type, f_tick_size,
                  f_tick_line_width, (b_landmark_ticks_only)? int(n_landmark_vertex_dim) - 1 : 0,
                  (b_landmark_ticks_only)? int(n_landmark_vertex_dim) : SIZE_MAX);
//    Draw_ConstVertices(p_image, t_transform, v_offset, 0xff8800ffU, n_ctick_type, f_tick_size,
//                       f_tick_line_width, (b_landmark_ticks_only)? int(n_landmark_vertex_dim) - 1 : 0,
//                       (b_landmark_ticks_only)? int(n_landmark_vertex_dim) : SIZE_MAX);
    // draw all the vertices

    Draw_Edges(p_image, t_transform, v_offset, 0xff0000ffU,
               f_edge_line_width, n_landmark_edge_dim - 1, n_landmark_edge_dim, true); // BA landmark edges are 2D, SE(3) landmark edges are 3D
    // draw all other edges

    bool b_result = CTgaCodec::Save_TGA(p_s_filename, *p_image, false);
    p_image->Delete();

    return b_result;
}



/**
 *	@brief plots the system as a .tga (targa) image
 *
 *	@param[in] p_s_filename is the output file name
 *	@param[in] n_quality_profile is plot profile (one of plot_*)
 *
 *	@return Returns true on success, false on failure.
 */
bool Plot2D(const char *p_s_filename, plot_quality::EQualityProfile n_quality_profile) const
{
    switch(n_quality_profile) {
        case plot_quality::plot_Draft:
            return Plot2D(p_s_filename, 1024, 8192, 2, 1, 1, 1, false, true, 32, false);
        case plot_quality::plot_Printing:
            return Plot2D(p_s_filename, 2048, 2048, 10, 3, 7, 1, true, false, 10);
        case plot_quality::plot_Printing_LandmarkTicksOnly:
            return Plot2D(p_s_filename, 2048, 2048, 10, 3, 7, 1, true, false, 10, true);
        case plot_quality::plot_Printing_NoTicks:
            return Plot2D(p_s_filename, 2048, 2048, 0, 0, 7, 1, true, false, 4);
    };
    return false;
}



/**
 *	@brief plots the system as a .tga (targa) image
 *
 *	This version assumes a 3D system, it plots it as its projection to the XZ plane.
 *
 *	@param[in] p_s_filename is the output file name
 *	@param[in] n_quality_profile is plot profile (one of plot_*)
 *
 *	@return Returns true on success, false on failure.
 */
bool Plot3D(const char *p_s_filename, plot_quality::EQualityProfile n_quality_profile) const
{
    switch(n_quality_profile) {
        case plot_quality::plot_Draft:
            return Plot3D(p_s_filename, 1024, 8192, 2, 1, 1, 1, false, true, 32, false);
        case plot_quality::plot_Printing:
            return Plot3D(p_s_filename, 2048, 2048, 10, 3, 7, 1, true, false, 10);
        case plot_quality::plot_Printing_LandmarkTicksOnly:
            return Plot3D(p_s_filename, 2048, 2048, 10, 3, 7, 1, true, false, 10, true);
        case plot_quality::plot_Printing_NoTicks:
            return Plot3D(p_s_filename, 2048, 2048, 0, 0, 7, 1, true, false, 4);
    };
    return false;
}



#endif //SLAM_PLUS_PLUS_PLOTTINGUTILITIES_H
