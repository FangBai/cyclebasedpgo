// Simple interface for using the MCB library.
// fang.bai@student.uts.edu.au

#include <iostream>
#include <stdio.h>
#include <sys/resource.h>
#include <LEP/mcb/determinant.h>
#include <LEP/mcb/mcb.h>
#include <LEP/mcb/verify.h>

#ifdef LEDA_GE_V5
#include <LEDA/graph/graph_misc.h>
#include <LEDA/graph/gml_graph.h>
#include <LEDA/graph/edge_map.h>
#include <LEDA/core/h_array.h>
#else
#include <LEDA/graph_misc.h>
#include <LEDA/gml_graph.h>
#include <LEDA/edge_map.h>
#include <LEDA/h_array.h>
#endif

#include <fstream>
#include "GraphConstruction.h"
#include "myTimer.h"


#if defined(LEDA_NAMESPACE)
using namespace leda;
#endif

// length
edge_map<int> lenmap;


//
// print_mcb(G, mcb, enumb, edge2idxmap, fo)
template<class Container>
void print_mcb ( leda::graph& G, const leda::array<Container>& mcb, const mcb::edge_num& enumb,
                 const leda::edge_array<int>& edge2idxmap, std::ostream& fo );


// specialization for leda::array<mcb::spvecgf2>& mcb
template<>
void print_mcb<mcb::spvecgf2> ( leda::graph& G, const leda::array<mcb::spvecgf2>& mcb, const mcb::edge_num& enumb,
                 const leda::edge_array<int>& edge2idxmap, std::ostream& fo )
{
    leda::edge e;
    for( int i = 0; i < enumb.dim_cycle_space(); ++i ) {

        // hide all edges
        forall_edges(e, G)  G.hide_edge(e);

        // restore edges in mcb[i] to form a subgraph, i.e., a cycle
        list_item it = mcb[i].first();
        while (it != nil) {
            G.restore_edge( enumb(mcb[i].index(it)) );
            it = mcb[i].succ(it);
        }
        /* transfer cycle information into a topological subgraph  */
        //--then traverse edges on the subgraph, which a cycle

        leda::list<int> L;
        int sign_e = 1; int n_cnt = 0;

        // pick the first edge
        leda::edge ec = G.first_edge();
        L.append( sign_e * edge2idxmap[ec] ); /* print directly */
        // traverse the cycle along the direction of first edge

        leda::node v0 = G.source(ec);  /* beginning node  */
        leda::node vc = G.target(ec);  /* current working node */

        while(vc != v0) {
            list <edge> adj_lst = G.adj_edges(vc);
            if (ec == adj_lst.front())
                ec = adj_lst.back();
            else
                ec = adj_lst.front();
            if(vc== source(ec)){
                vc = target(ec); sign_e = 1;
            }else{
                vc = source(ec); sign_e = -1;
                ++n_cnt;
            }
            L.append(sign_e * edge2idxmap[ec]);  /* print directly */
        }
        // reverse and take negative if too many negative numbers
        if ( 2 * n_cnt > L.size() ){
            it = L.first();
            while( it != nil ) {
                L.assign(it, -L.contents(it));
                it = L.succ(it);
            }
            L.reverse_items(); // same results, L.reverse();
        }
        fo << L << std::endl;
    }
}


//// specialization for leda::array<leda::d_int_set>& mcb
//template <>
//void print_mcb <leda::d_int_set> ( leda::graph& G, const leda::array<leda::d_int_set>& mcb, const mcb::edge_num& enumb,
//                 const leda::edge_array<int>& edge2idxmap, std::ostream& fo )
//{
//    leda::edge e;
//    for( int i = 0; i < enumb.dim_cycle_space(); ++i ) {
//
//        // hide all edges
//        forall_edges(e, G)  G.hide_edge(e);
//
//        // restore edges in mcb[i] to form a subgraph, i.e., a cycle
//        leda::list<int> lmcb;
//        mcb[i].get_element_list(lmcb); // exact set to list
//        list_item it = lmcb.first();
//        while (it != nil) {
//            G.restore_edge( enumb( lmcb.contents(it) ) );
//            it = lmcb.succ(it);
//        }
//        lmcb.clear();
//        /* transfer cycle information into a topological subgraph  */
//        //--then traverse edges on the subgraph, which a cycle
//
//        leda::list<int> L;
//        int sign_e = 1; int n_cnt = 0;
//
//        // pick the first edge
//        leda::edge ec = G.first_edge();
//        L.append( sign_e * edge2idxmap[ec] ); /* print directly */
//        // traverse the cycle along the direction of first edge
//
//        leda::node v0 = G.source(ec);  /* beginning node  */
//        leda::node vc = G.target(ec);  /* current working node */
//
//        while(vc != v0) {
//            list <edge> adj_lst = G.adj_edges(vc);
//            if (ec == adj_lst.front())
//                ec = adj_lst.back();
//            else
//                ec = adj_lst.front();
//            if(vc== source(ec)){
//                vc = target(ec); sign_e = 1;
//            }else{
//                vc = source(ec); sign_e = -1;
//                ++n_cnt;
//            }
//            L.append(sign_e * edge2idxmap[ec]);  /* print directly */
//        }
//        // reverse and take negative if too many negative numbers
//        if ( 2 * n_cnt > L.size() ){
//            it = L.first();
//            while( it != nil ) {
//                L.assign(it, -L.contents(it));
//                it = L.succ(it);
//            }
//            L.reverse_items(); // same results, L.reverse();
//        }
//        fo << L << std::endl;
//    }
//}



void print_usage(const char * program)
{
    std::cout << "Usage: "<< program <<" [-d] [-i input] [-o output] [-a summary] [-s] [-c] [-l] [-k value] [-e value] [-f value] [-h]" << std::endl;
    std::cout <<"-d" << std::endl;
    std::cout <<"          Construct a directed graph. default undirected." << std::endl;
    std::cout <<"-i" << std::endl;
    std::cout <<"          Input file name. (default std::cin)" << std::endl;
    std::cout <<"-o" << std::endl;
    std::cout <<"          Output file name. (default std::cout)" << std::endl;
    std::cout <<"-a" << std::endl;
    std::cout <<"          Summary file name. (default std::cout)" << std::endl;
    std::cout <<"-s" << std::endl;
    std::cout <<"          Use the support vector approach (undirected graphs)." << std::endl;
    std::cout <<"-c" << std::endl;
    std::cout <<"          Use the hybrid approach (undirected graphs)." << std::endl;
    std::cout <<"-l" << std::endl;
    std::cout <<"          Use the hybrid approach with labelled trees (undirected graphs)." << std::endl;
    std::cout <<"-k value" << std::endl;
    std::cout <<"          If this option is given, we compute an approximate MCB." << std::endl;
    std::cout <<"          The approximation factor will be 2k-1." << std::endl;
    std::cout <<"-e value" << std::endl;
    std::cout <<"          Probability of error in the case of a directed graph. This is the" << std::endl;
    std::cout <<"          probability that the returned basis is not minimum." << std::endl;
    std::cout <<"          Default value is 0.375." << std::endl;
    std::cout <<"-f value" << std::endl;
    std::cout <<"          Try to compute a directed minimum cycle basis in F_p where p is the value " << std::endl;
    std::cout <<"          parameter. This is not necessarily a minimum cycle basis." << std::endl;
    std::cout <<"          The value parameter must be a prime number." << std::endl;
    std::cout <<"          If this option is given, the option -e is ignored." << std::endl;
    std::cout <<"-h" << std::endl;
    std::cout <<"          Display this message." << std::endl;
}

std::streambuf *pb_cin = std::cin.rdbuf(); // std::cin console buffer
//

int main(int argc, char* argv[]) {

    bool support = true;
    bool hybrid = false;
    bool treeshybrid = false;
    bool approx = false;
    bool directedfp = false;
    double errorp = 0.1;
    leda::integer prime;
    int k;
    int c;

    //use directed/oriented graph // -o
    bool use_directed_graph = false;
    char * input_file_name = NULL;
    char * output_file_name = NULL;
    char * summary_file_name = NULL;

    opterr = 0;  // in getopt.h which will avoid printing the the error to stderr if set to 0

    /*
    if(argc < 2){
        print_usage( argv[0] );
        return 0;
    }
    */

    while ((c = getopt (argc, argv, "di:o:a:sclk:e:f:h")) != -1)
        switch (c)
        {
            case 'd':
                use_directed_graph = true;
                break;
            case 'i':
                input_file_name = optarg;
                break;
            case 'o':
                output_file_name = optarg;
                break;
            case 'a':
                summary_file_name = optarg;
                break;
            case 's':
                support = true; 
                hybrid = false;
                treeshybrid = false;
                break;
            case 'c':
                support = false;
                hybrid = true;
                treeshybrid = false;
                break;
            case 'l':
                support = false;
                hybrid = false; 
                treeshybrid = true;
                break;
            case 'k':
                k = atoi( optarg );
                if ( k > 1 )
                    approx = true;
                else 
                    k = 1;
                break;
            case 'e':
                errorp = atof( optarg );
                if ( errorp <= 0.0 || errorp > 1.0 )
                    errorp = 0.1;
                break;
            case 'f': 
                prime = atoi( optarg );
                if ( prime < 3 || ! mcb::primes<mcb::ptype>::is_prime( prime ) ) {
                    std::cout << "-f parameter value should be a prime > 2, ignoring it.." << std::endl;   
                }
                else 
                    directedfp = true;
                break;
            case 'h':
            case '?':
            default:
                print_usage( argv[0] );
                return 0;
        }



    std::istream *pfi;
    std::ostream *pfo, *pfs;
    std::fstream fin, fout, fsum;



    if(input_file_name != NULL) {
        fin.open(input_file_name, std::ios::in);
        pfi = & fin;
    }
    else
        pfi = & std::cin;

    if(output_file_name != NULL) {
        fout.open(output_file_name, std::ios::out);
        pfo = & fout;
    }
    else
        pfo = & std::cout;

    if(summary_file_name != NULL) {
        fsum.open(summary_file_name, std::ios::out);
        pfs = & fsum;
    }
    else
        pfs = & std::cout;

    //
    const char * p_infile = (input_file_name !=NULL) ? input_file_name : "< std::cin";
    const char * p_outfile = (output_file_name !=NULL) ? output_file_name: "std::cout";
    *pfs << "input: " << p_infile << std::endl;
    *pfs << "output: " << p_outfile << std::endl;


    *pfs << "Comments: Edge index starts form 1, consecutively as adding order." << std::endl;

    std::streambuf * pbuf = pfi->rdbuf();
    // std::cout << "--- buffer size() = " << pbuf->in_avail() << std::endl;
    if (pbuf == pb_cin)
        std::cout << "Input Format: \'Edge node1 node2 ...\\n\'. Please use Ctrl+D to stop." << std::endl;


    // initialize Graph
    graph G;
    lenmap.init( G );

    if (!use_directed_graph)
        G.make_undirected(); // use undirected graph

    if( createPoseGraphByEdgeData(G, *pfi) )
        return 1;

    // G.print();  /* print graph */

    // copy from map to edge array
    edge e;
    edge_array<int> len( G, 1 );


    // execute
    float T;
    leda::used_time( T ); // start time


    float startTime = used_time ();


    Timer myTimer;
    myTimer.tic();


    mcb::edge_num enumb( G );

    int w;

    // map edge_pointer to its natural ordering (idx)
    leda::edge_array<int> edge2idxmap(G, 0);
    int id = 0;
    forall_edges (e, G)
        edge2idxmap[ e ] = ++id;
    //



    if ( G.is_undirected() )
    {
        if ( approx ) { 
            *pfs << "Approach: Computing undirected " << 2*k-1 << "-MCB with default approach." << std::endl;
            array< mcb::spvecgf2 > mcb;
            w = mcb::UMCB_APPROX( G, len, k, mcb, enumb );
            print_mcb(G, mcb, enumb, edge2idxmap, *pfo);
        }
        else if ( support ) { 
            *pfs << "Approach: Computing undirected MCB with Support Vector approach." << std::endl;
            array< mcb::spvecgf2 > mcb;
            array< mcb::spvecgf2 > proof;
            w = mcb::UMCB_SVA( G, len, mcb, proof, enumb );
            print_mcb(G, mcb, enumb, edge2idxmap, *pfo);
        }
//        else if ( hybrid ) {
//            *pfs << "Approach: Computing undirected MCB with Hybrid approach." << std::endl;
//            array< leda::d_int_set > mcb;
//            array< leda::d_int_set > proof;
//            w = mcb::UMCB_HYBRID( G, len, mcb, proof, enumb );
//            print_mcb(G, mcb, enumb, edge2idxmap, *pfo);
//        }
        else if ( treeshybrid ) { 
            *pfs << "Approach: Computing undirected MCB with Hybrid + Labelled Trees approach." << std::endl;
            array< mcb::spvecgf2 > mcb;
            array< mcb::spvecgf2 > proof;
            w = mcb::UMCB_FH( G, len, mcb, enumb );
            print_mcb(G, mcb, enumb, edge2idxmap, *pfo);
        }
        else { 
            *pfs << "Approach: Computing undirected MCB with default approach." << std::endl;
            array< mcb::spvecgf2 > mcb;
            array< mcb::spvecgf2 > proof;
            w = mcb::UMCB( G, len, mcb, enumb );
            print_mcb(G, mcb, enumb, edge2idxmap, *pfo);
        }
    }
    else 
    {
        if (approx)
            *pfs << "Approach: Computing directed " << 2 * k - 1 << "-MCB with default approach." << std::endl;
        else
            *pfs << "Approach: Computing directed MCB with default approach." << std::endl;
        if (directedfp)
            *pfs << "Approach: Computation is over F_" << prime << " instead of Q" << std::endl;

        array< mcb::spvecfp > mcb;
        array< mcb::spvecfp > proof;

        if ( approx ) { 
            if ( directedfp )
                w = mcb::DMCB_APPROX( G, len, k, mcb, enumb, prime );
            else
                w = mcb::DMCB_APPROX( G, len, k, mcb, enumb, errorp );
        }
//        else {
//            if ( directedfp )
//                w = mcb::DMCB<int>( G, len, mcb, proof, enumb, prime );
//            else
//                w = mcb::DMCB<int>( G, len, mcb, proof, enumb, errorp );
//        }

        //  print_mcb(G, mcb, enumb, edge2idxmap, fo)
        for( int i = 0; i < enumb.dim_cycle_space(); ++i ) {

            // hide all edges
            forall_edges(e, G)  G.hide_edge(e);

            // restore edges in mcb[i] to form a subgraph, i.e., a cycle
            list_item it = mcb[i].first();
            while (it != nil) {
                G.restore_edge( enumb(mcb[i].index(it)) );
                it = mcb[i].succ(it);
            }
            /* transfer cycle information into a topological subgraph  */
            //--then traverse edges on the subgraph, which a cycle

            leda::list<int> L;
            int sign_e = 1;   // traverse direction
            int n_cnt = 0;

            // pick the first edge
            leda::edge ec = G.first_edge();
            L.append(sign_e * edge2idxmap[ec]); /* print directly */
            // traverse the cycle along the direction of first edge

            leda::node v0 = G.source(ec);  /* beginning node  */
            leda::node vc = G.target(ec);  /* current working node */

            while(vc != v0) {

                list <edge> out_lst = G.out_edges(vc);
                list <edge> in_lst = G.in_edges(vc);

                if( sign_e == 1) { //sign_e = 1 // ec in forward direction
                    if (!out_lst.empty()) { // next edge in out_lst
                        ec = out_lst.front();
                        sign_e = 1;
                        vc = G.target(ec);
                    } else { // next_edge in in_lst
                        if (ec == in_lst.front())
                            ec = in_lst.back();
                        else
                            ec = in_lst.front();
                        sign_e = -1;
                        ++n_cnt;
                        vc = G.source(ec);
                    }
                } else {  // sign_e = -1  // ec in backward direction
                    if (!in_lst.empty()) { // next edge in in_lst
                        ec = in_lst.front();
                        sign_e = -1;
                        ++n_cnt;
                        vc = G.source(ec);
                    } else { // next edge in out_lst
                        if ( ec == out_lst.front())
                            ec = out_lst.back();
                        else
                            ec = out_lst.front();
                        sign_e = 1;
                        vc = G.target(ec);
                    }
                }
                L.append(sign_e * edge2idxmap[ec]);  /* print directly */
            }
            // reverse and take negative if too many negative numbers
            if ( 2 * n_cnt > L.size() ){
                it = L.first();
                while( it != nil ) {
                    L.assign(it, -L.contents(it));
                    it = L.succ(it);
                }
                L.reverse_items(); // same results, L.reverse();
            }
            *pfo << L << std::endl;
        }
    }

    T = used_time( T ); // finish time

    double timeUsed = used_time () - startTime;

    //if ( mcb::verify_cycle_basis( G, enumb, mcb ) == false ) 
      //  leda::error_handler(999,"MIN_CYCLE_BASIS: result is not a cycle basis");


    *pfs << "cycle_space_dim: " << enumb.dim_cycle_space() << std::endl;
    *pfs << "weight: " << w << std::endl;
    *pfs << "ledaTime: time_to_compute: " << std::setprecision(6) << timeUsed << std::endl;
    *pfs << "myTimer: time_to_compute: " << std::setprecision(6) << myTimer.toc() << std::endl;



    if(input_file_name != NULL)  fin.close();

    if(output_file_name != NULL)  fout.close();

    if(summary_file_name != NULL)  fsum.close();


    std::cout << "MCB Computation Finished." << std::endl;

    return 0;

}

/* ex: set ts=4 sw=4 sts=4 et: */


