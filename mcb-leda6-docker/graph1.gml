graph [
directed 0

node [ id 0 ]
node [ id 1 ]
node [ id 2 ]
node [ id 3 ]
node [ id 4 ]
node [ id 5 ]
node [ id 6 ]
node [ id 7 ]
node [ id 8 ]
node [ id 9 ]

edge [
	source 0
	target 1
	label 1
]
edge [
	source 0
	target 7
	label 1
]
edge [
	source 1
	target 8
	label 1
]
edge [
	source 1
	target 9
	label 1
]
edge [
	source 2
	target 4
	label 1
]
edge [
	source 2
	target 1
	label 1
]
edge [
	source 5
	target 0
	label 1
]
edge [
	source 5
	target 6
	label 1
]
edge [
	source 6
	target 0
	label 1
]
edge [
	source 7
	target 8
	label 1
]
edge [
	source 8
	target 2
	label 1
]
edge [
	source 8
	target 4
	label 1
]
edge [
	source 8
	target 6
	label 1
]

]
