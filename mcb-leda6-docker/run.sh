#!/bin/bash

# firstly run docker_run.sh to compile the image and exe
bash ./docker_run.sh

rm save -rf
mkdir save

bash ./mcb_run.sh ../data/intel.txt save/intel_o.txt save/intel_summary.txt

bash ./mcb_run.sh ../data/M3500.txt save/M3500_o.txt save/M3500_summary.txt

bash ./mcb_run.sh ../data/sphere2500.txt save/sphere2500_o.txt save/sphere2500_summary.txt

bash ./mcb_run.sh ../data/INTEL.txt save/INTEL_o.txt save/INTEL_summary.txt

bash ./mcb_run.sh ../data/MITb.txt save/MITb_o.txt save/MITb_summary.txt

bash ./mcb_run.sh ../data/city10k.txt save/city10k_o.txt save/city10k_summary.txt

bash ./mcb_run.sh ../data/torus10000.txt save/torus10000_o.txt save/torus10000_summary.txt


docker stop mcbcontainer

rm Dimitrios_mcb.txt


echo intel >> Dimitrios_mcb.txt
echo "---------------------" >> Dimitrios_mcb.txt
cat save/intel_summary.txt >> Dimitrios_mcb.txt
echo " " >> Dimitrios_mcb.txt


echo M3500 >> Dimitrios_mcb.txt
echo "---------------------" >> Dimitrios_mcb.txt
cat save/M3500_summary.txt >> Dimitrios_mcb.txt
echo " " >> Dimitrios_mcb.txt

echo sphere2500 >> Dimitrios_mcb.txt
echo "---------------------" >> Dimitrios_mcb.txt
cat save/sphere2500_summary.txt >> Dimitrios_mcb.txt
echo " " >> Dimitrios_mcb.txt

echo INTEL >> Dimitrios_mcb.txt
echo "---------------------" >> Dimitrios_mcb.txt
cat save/INTEL_summary.txt >> Dimitrios_mcb.txt
echo " " >> Dimitrios_mcb.txt

echo MITb >> Dimitrios_mcb.txt
echo "---------------------" >> Dimitrios_mcb.txt
cat save/MITb_summary.txt >> Dimitrios_mcb.txt
echo " " >> Dimitrios_mcb.txt

echo city10k >> Dimitrios_mcb.txt
echo "---------------------" >> Dimitrios_mcb.txt
cat save/city10k_summary.txt >> Dimitrios_mcb.txt
echo " " >> Dimitrios_mcb.txt

echo torus10000 >> Dimitrios_mcb.txt
echo "---------------------" >> Dimitrios_mcb.txt
cat save/torus10000_summary.txt >> Dimitrios_mcb.txt
echo " " >> Dimitrios_mcb.txt
